rem Android --------------------------------------------------

rem ZIPアーカイブシステムを使うとき
rem xcopy /Y Storage\assets.zip projects\Android\app\src\main\assets\
rem xcopy /Y Storage\disc.zip   projects\Android\app\src\main\assets\

xcopy /E /Y Storage\01_rom\*.* projects\Android\app\src\main\assets\


rem Windows10 uwp --------------------------------------------------
rem Icon類をAssetsフォルダにコピーする
xcopy /Y projects\CommonFiles\uwp\*.png projects\Windows10UWP\Assets\

rem asset リソースをZipアセットとしてコピーする
xcopy /Y Storage\assets.zip projects\Windows10UWP\Assets\
xcopy /Y Storage\disc.zip   projects\Windows10UWP\Assets\


rem Android cpp FileListMake--------------------------------------------------

set INPUT_FILE=temp.txt
set OUTPUT_FILE=projects\Android\app\src\main\cpp\temp.txt

del %INPUT_FILE%
del %OUTPUT_FILE%

dir game\*.cpp /s /b >%INPUT_FILE%

set BEFORE_STRING=%CD%
set AFTER_STRING=		../../../../../..

setlocal enabledelayedexpansion
for /f "delims=" %%a in (%INPUT_FILE%) do (
set line=%%a
echo !line:%BEFORE_STRING%=%AFTER_STRING%!>>%OUTPUT_FILE%
)


rem Android cpp FileListMake--------------------------------------------------

set INPUT_FILE2=%OUTPUT_FILE%
set OUTPUT_FILE2=projects\Android\app\src\main\cpp\fileList.txt

del %OUTPUT_FILE2%

set BEFORE_STRING2=\
set AFTER_STRING2=/


setlocal enabledelayedexpansion
for /f "delims=" %%a in (%INPUT_FILE2%) do (
set line=%%a
echo !line:%BEFORE_STRING2%=%AFTER_STRING2%!>>%OUTPUT_FILE2%
)
del %INPUT_FILE%
del %INPUT_FILE2%

rem --------- make assets.txt ---------------------------------

set INPUT_FILE3=assets.tmp
set OUTPUT_FILE3=projects\Android\app\src\main\assets\assets.txt

del %INPUT_FILE3%
del %OUTPUT_FILE3%

dir /b /S /a-d Storage\01_rom >%INPUT_FILE3%

set BEFORE_STRING=%CD%\Storage\01_rom\
set AFTER_STRING=

setlocal enabledelayedexpansion
for /f "delims=" %%a in (%INPUT_FILE3%) do (
set line=%%a
echo !line:%BEFORE_STRING%=%AFTER_STRING%!>>%OUTPUT_FILE3%
)
del %INPUT_FILE3%

