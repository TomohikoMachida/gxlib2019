//------------------------------------------
//
// ゲームアプリ側の実装
//
// gxLib 2019
//
// 2019.10.26 written by ragi.
//------------------------------------------
#include <gxLib.h>

void Visorizer( int bReset );

gxBool GameInit()
{

	return gxTrue;
}

gxBool GameMain()
{
	Visorizer(0);

    return gxTrue;
}


gxBool GamePause()
{

	return gxTrue;
}


gxBool GameSleep()
{
	//SLEEP時に１度だけ呼び出される

	return gxTrue;
}

gxBool GameResume()
{
	//SLEEPから回復したときに１度だけ呼び出される

	return gxTrue;
}

gxBool GameEnd()
{
	//アプリ終了時に１度だけ呼び出される

	return gxTrue;
}

gxBool GameReset()
{
	//リセット時に１度だけ呼び出される
    Visorizer( 1 );

	return gxTrue;
}

gxBool DragAndDrop( gxChar *pFileName )
{
	//ドラッグ＆ドロップされたときにドラッグされたファイル数分だけ呼び出される
	//Windowsのみ

	return gxTrue;
}


