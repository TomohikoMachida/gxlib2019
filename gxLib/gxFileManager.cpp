﻿#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
//#include <io.h>	//androidではない
#include <fcntl.h>
#include <time.h> 
#include <gxLib.h>
#include <gxLib/gx.h>

#include "gxFileManager.h"
#include "util/gxUIManager.h"
#include <string>

//#define _CRT_SECURE_NO_WARNINGS (1)

SINGLETON_DECLARE_INSTANCE( gxFileManager );

gxFileManager::gxFileManager()
{
/*
	for(Sint32 ii=0;ii<enRequestMax; ii++ )
	{
		m_FileInfo[ii].m_pDataBuf    = NULL;
		m_FileInfo[ii].m_uSizeBuf    = 0;
		m_FileInfo[ii].m_pNameBuf[0] = 0x00;
		m_FileInfo[ii].m_Location    = STORAGE_LOCATION_ROM;
		m_FileInfo[ii].m_AccessMode  = ACCESSMODE_READ;
	}
*/
	m_pDropFiles   = NULL;
	m_DropFilesNum = 0;
	m_DropFilesCnt = 0;

	m_bDragEnable = gxFalse;
	m_sReqCnt = 0;
}

gxFileManager::~gxFileManager()
{
	//for(Sint32 ii=0;ii<enRequestMax; ii++ )
	//{
	//	SAFE_DELETE( m_FileInfo[ii].m_pDataBuf );
	//}
	//渡されるべきデータなのでここでは処分しない

	SAFE_DELETES( m_pDropFiles );
}

void gxFileManager::Action()
{
//	for( Sint32 ii=0; ii<enRequestMax; ii++ )


	for( auto itr = m_FileInfo.begin() ; itr != m_FileInfo.end() ; itr ++)
	{
		//StFileInfo *p = &m_FileInfo[ii];
		StFileInfo *p;

		p = &itr->second;

		if( p->m_pNameBuf[0] == 0x00 ) continue;

		if( p->m_AccessMode == ACCESSMODE_READ )
		{
			//読み込み
			if( p->m_Location == STORAGE_LOCATION_ROM )
			{
				//ROMに探しに行く
				p->m_pDataBuf = loadFile( (const gxChar*)p->m_pNameBuf , &p->m_uSizeBuf , STORAGE_LOCATION_ROM );
			}
			else if( p->m_Location == STORAGE_LOCATION_INTERNAL)
			{
				//DVD領域からデータを探し出す
				p->m_pDataBuf = loadFile( (const gxChar*)p->m_pNameBuf , &p->m_uSizeBuf , STORAGE_LOCATION_INTERNAL);
			}
			else if (p->m_Location == STORAGE_LOCATION_EXTERNAL)
			{
				//HDD領域からデータを探し出す
				p->m_pDataBuf = loadFile((const gxChar*)p->m_pNameBuf, &p->m_uSizeBuf, STORAGE_LOCATION_EXTERNAL);
			}
			else if( p->m_Location == STORAGE_LOCATION_AUTO )
			{
				//メモリーカード領域から順番にデータを探し出す
				p->m_pDataBuf = loadFile( (const gxChar*)p->m_pNameBuf , &p->m_uSizeBuf , STORAGE_LOCATION_INTERNAL);
				if( p->m_pDataBuf == NULL )
				{
					//DVDになかったらROMに探しに行く
					p->m_pDataBuf = loadFile( (const gxChar*)p->m_pNameBuf , &p->m_uSizeBuf , STORAGE_LOCATION_ROM );
				}
			}
		}
		else if( p->m_AccessMode == ACCESSMODE_WRITE )
		{
			//書き込み
			if( p->m_Location == STORAGE_LOCATION_INTERNAL)
			{
				saveFile( (const gxChar*)p->m_pNameBuf , p->m_pDataBuf , p->m_uSizeBuf , STORAGE_LOCATION_INTERNAL);
			}
			else if (p->m_Location == STORAGE_LOCATION_EXTERNAL)
			{
				saveFile((const gxChar*)p->m_pNameBuf, p->m_pDataBuf, p->m_uSizeBuf, STORAGE_LOCATION_EXTERNAL);
			}
			else if( p->m_Location == STORAGE_LOCATION_AUTO )
			{
				saveFile( (const gxChar*)p->m_pNameBuf , p->m_pDataBuf , p->m_uSizeBuf , STORAGE_LOCATION_INTERNAL);
			}
			SAFE_DELETE( p->m_pDataBuf );	//Save時のデータはManagerが退避しているので、ここで削除する
		}

		p->m_pNameBuf[0] = 0x00;

		if( p->m_CallBack )
		{
			p->m_CallBack( p->id );
		}
	}

}


Sint32 gxFileManager::SaveReq( const gxChar *pFileName , Uint8* pData , Uint32 uSize , ESTORAGE_LOCATION _Location )
{
	int id = m_sReqCnt;

	//for( ;; )
	//{
	//	id = m_sReqCnt%enRequestMax;
	//	if( m_FileInfo[ id ].m_pNameBuf[0] )
	//	{
	//		m_sReqCnt ++;
	//		continue;
	//	}
	//	break;
	//}

	id = m_sReqCnt;

	m_FileInfo[id].id           = id;
	m_FileInfo[id].m_Location   = _Location;
	m_FileInfo[id].m_AccessMode = ACCESSMODE_WRITE;
	m_FileInfo[id].m_CallBack   = nullptr;

	m_FileInfo[id].m_pDataBuf = new Uint8[uSize];	//セーブの時はバッファにためておかないとスレッドで保存するときにメモリの中身はないかもしれない
	gxUtil::MemCpy( m_FileInfo[id].m_pDataBuf , pData , uSize );
	m_FileInfo[id].m_uSizeBuf = uSize;

	//ファイル名を最後に指定しないとスレッドが走り出す
#ifdef BUILD_COMPILER_VISUALSTUDIO
	sprintf_s(&m_FileInfo[id].m_pNameBuf[0], 128, "%s", pFileName);	//androidで無効
#else
	sprintf(&m_FileInfo[id].m_pNameBuf[0], "%s", pFileName);
#endif
	m_sReqCnt ++;

	return id;
}


Sint32 gxFileManager::LoadReq( const gxChar *pFileName , ESTORAGE_LOCATION _Location , std::function<void(Sint32 id)>func )
{
	int id = m_sReqCnt;

#ifdef BUILD_COMPILER_VISUALSTUDIO
	sprintf_s( &m_FileInfo[id].m_pNameBuf[0] ,128, "%s" , pFileName );	//androidで無効
#else
	sprintf( &m_FileInfo[id].m_pNameBuf[0] ,"%s" , pFileName );
#endif
	m_FileInfo[id].id           = id;
	m_FileInfo[id].m_Location   = _Location;
	m_FileInfo[id].m_AccessMode = ACCESSMODE_READ;
	m_FileInfo[id].m_CallBack   = func;

	m_sReqCnt ++;

	return id;
}

gxBool gxFileManager::IsLoadEnd( Sint32 id )
{
//	id = id%enRequestMax;

	if( m_FileInfo[id].m_pNameBuf[0] == 0x00 )
	{
		return gxTrue;
	}

	return gxFalse;
}

gxBool gxFileManager::IsSaveEnd( Sint32 id )
{
//	id = id%enRequestMax;

	if( m_FileInfo[id].m_pNameBuf[0] == 0x00 )
	{
		return gxTrue;
	}

	return gxFalse;
}


Uint8* gxFileManager::GetFileAddr(Sint32 id )
{
//	id = id%enRequestMax;

	if( m_FileInfo[id].m_pNameBuf[0] == 0x00 )
	{
		return m_FileInfo[id].m_pDataBuf;
	}

	return NULL;
}


Uint32 gxFileManager::GetFileSize(Sint32 id )
{
//	id = id%enRequestMax;

	return m_FileInfo[id].m_uSizeBuf;
}

void gxFileManager::Clear( Sint32 id )
{
	//if( m_FileInfo[id].m_pDataBuf )
	{
		m_FileInfo.erase(id);
	}
}

Uint8* gxFileManager::loadFile( const gxChar* pFileName , Uint32* pLength , int _location )
{
	//デバイスのファイル読み込みを開始

	Uint8 *pData = NULL;
	Uint32 uSize = 0;

	pData = CDeviceManager::GetInstance()->LoadFile(pFileName, &uSize, _location);
	*pLength = uSize;

	gxUIManager::GetInstance()->NowLoading();

	return pData;

}


gxBool gxFileManager::saveFile( const gxChar* pFileName  , Uint8 *pData , Uint32 uSize , int _location  )
{
	//デバイスのファイル書き込みを開始

	gxUIManager::GetInstance()->NowSaving();

	if (CDeviceManager::GetInstance()->SaveFile(pFileName, pData, uSize, _location))
	{
		return gxTrue;
	}

	return gxFalse;
}


void gxFileManager::SetDropFileNum( Sint32 num )
{
	//ドラッグ＆ドロップされたファイル数を記録する

	ClearDropFiles();

	m_DropFilesNum = num;

	m_pDropFiles = new gxChar*[ num ];
}


void gxFileManager::AddDropFile( gxChar* pFileName )
{
	//ドラッグ＆ドロップされたファイル名を追加する

	size_t length = strlen( pFileName )+1;

	m_pDropFiles[m_DropFilesCnt] = new gxChar[length];
#ifdef BUILD_COMPILER_VISUALSTUDIO
	sprintf_s( m_pDropFiles[m_DropFilesCnt], length, "%s", pFileName);
#else
	sprintf( m_pDropFiles[m_DropFilesCnt], "%s", pFileName);
#endif
	m_DropFilesCnt ++;
}


Sint32  gxFileManager::GetDropFileNum()
{
	return m_DropFilesNum;
}


gxChar* gxFileManager::GetDropFileName( Sint32 num )
{
	return m_pDropFiles[num];
}


void gxFileManager::ClearDropFiles()
{
	if( m_pDropFiles )
	{
		for( Sint32 ii=0; ii<m_DropFilesNum; ii++ )
		{
			SAFE_DELETES( m_pDropFiles[ii] );
		}

		SAFE_DELETES( m_pDropFiles );
	}

	m_DropFilesCnt = 0;
	m_DropFilesNum = 0;
}



