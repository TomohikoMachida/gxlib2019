﻿//--------------------------------------------------
//
// gxTexdManager.h
//
//--------------------------------------------------
#ifndef _TEXMANAGER_H_
#define _TEXMANAGER_H_

#include "util/CFileTarga.h"

class gxTexManager
{

public:

	enum {
		enMasterPageNum = MAX_MASTERTEX_NUM,//512,//16,
		enMasterWidth   = 2048,
		enMasterHeight  = 2048,
		enPageWidth     = 256,
		enPageHeight    = 256,
		enPageTexNum    = (enMasterWidth*enMasterHeight) / (enPageWidth*enPageHeight),
		enPageMax       = MAX_MASTERTEX_NUM*((enMasterWidth/enPageWidth)*(enMasterHeight/enPageHeight)),
	};

	gxTexManager(void);
	~gxTexManager(void);

	//ページからバンク番号を割り出す

	void SetUploadTextureRequest()
	{
		m_bUploadTextureRequest = gxTrue;
	}

	Sint32 getBankIndex( Sint32 pg )
	{
		if( pg < 0 ) return pg;
		return pg / m_sTexMax;
	}

	//テクスチャのロード
	gxBool LoadTexture( Sint32 tpg , gxChar* fileName , Uint32 colorKey=0xff00ff00 , Uint32 ox = 0, Uint32 oy = 0 , Sint32 *w=NULL , Sint32 *h=NULL );

	//バンクのイメージファイル
	Uint8* GetFileImage(Sint32 n)
	{
		return m_Targa[n].GetFileImage();
	}

	CFileTarga* GetAtlasTexture(Sint32 n)
	{
		return &m_Targa[n];
	}

	//バンクのファイルサイズを返す
	Uint32 GetFileSize( Sint32 n)
	{
		return m_Targa[n].GetFileSize();
	}

	//バンクの更新
	gxBool UploadTexture( gxBool bForce = gxFalse ,Sint32 sPage = -1);
	gxBool IsUploadTextureExist()
	{
		for (Sint32 ii = 0; ii < enMasterPageNum; ii++)
		{
			if (m_bUpdate[ii] && m_bUploadTextureRequest )
			{
				return gxTrue;
			}
		}

		return gxFalse;
	}

	// ------------------------------------------

	//テクスチャの追加
	gxBool addTexture(Sint32 tpg, CFileTarga* pTga, Uint32 colorKey, Uint32 ox = 0, Uint32 oy = 0);//, Sint32 *w = NULL, Sint32 *h = NULL );

	//テクスチャイメージの保存
	gxBool save( Sint32 tpg );

	void changeABGR2RGBA ( Sint32 x , Sint32 y , Uint8* pGraphicsBuffer );
	void changeTop2Bottom( Sint32 x , Sint32 y , Uint8* pGraphicsBuffer );

	void UpdateBankInfo( Sint32 sBank )
	{
		//バンクデータを外部から更新したことにする
		m_bUpdate[ sBank ] = gxTrue;
		m_bUsed[ sBank ]   = gxTrue;
	}

	void SetForceUpdate( Sint32 page , gxBool bUpdate = gxTrue )
	{
		//強制的にアップデートテクスチャ対象にする
		//(マスターテクスチャを直接書き換えた場合などに使用する)
		//ムービーが使用する

		m_bUpdate[ page ] = bUpdate;
	}

	gxChar* GetFileName( Sint32 tpg )
	{
		return &m_FileName[tpg][0];
	}

	SINGLETON_DECLARE( gxTexManager );

private:

	//バンクに入るページの数
	Sint32 m_sTexMax;

	//テクスチャの更新があればバンクに更新フラグを立てる
	gxBool m_bUpdate[enMasterPageNum];
	gxBool m_bUsed[enMasterPageNum];

	//テクスチャのイメージ格納用
	CFileTarga m_Targa[enMasterPageNum];

	gxChar m_FileName[enPageMax][FILENAMEBUF_LENGTH];

	// ------------------------------------------

//	Uint32* m_VRAM[enMasterPageNum];
	gxBool rectCopy( Uint32 tpg , Uint32 u , Uint32 v , Uint32 w , Uint32 h , Uint8* pBuf );
	gxBool m_bUploadTextureRequest = gxFalse;
};

#endif
