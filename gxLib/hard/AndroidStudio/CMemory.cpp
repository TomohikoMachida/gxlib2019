//-----------------------------------------------------------------------
//
// 
//
//-----------------------------------------------------------------------

#include <gxLib.h>
#include "CMemory.h"

static size_t m_UseMemory;
static size_t m_MaxUseMemory;
Uint8 m_AllocateLevel = 0;

Uint32 m_RamIndex = 0;

void* RAMArray[enMaxRam]={0};

gxBool m_bInitilizedMemoryUnit = gxFalse;

void SetAllocateLevel( Sint8 level = -1 )
{
	m_AllocateLevel = level;
}

void MemoryInit()
{
	m_bInitilizedMemoryUnit = gxTrue;
	gxUtil::MemSet( RAMArray , 0x00 , sizeof(RAMArray) );
	m_RamIndex = 0;
}

void MemoryDestroy( Sint8 level)
{
	for(Uint32 ii=0; ii<enMaxRam; ii++ )
	{
		if( RAMArray[ii] )
		{
			StRAMHead *p = (StRAMHead*)RAMArray[ii];

			if ( ( level == -1 ) || ( p->level == level ) ) 
			{
				//リークを発見
				StRAMHead *p = (StRAMHead*)RAMArray[ii];

				Uint8 *q = (Uint8*)(RAMArray[ii]);
				delete &q[ sizeof(StRAMHead) ];
			}
		}
	}
	m_bInitilizedMemoryUnit = gxFalse;
}


void UpdateMemoryStatus( Uint32* uNow , Uint32* uTotal , Uint32* uMax )
{
	//MBで返す

	*uNow   = m_UseMemory;
	*uTotal = GAME_HEAP_SIZE;
	*uMax   = m_MaxUseMemory;
}

int s_BlockNew = 0;
int s_BlockDel = 0;

void* operator new( size_t size )
{
    while(s_BlockNew>0)
    {
        gxLib::Sleep(1);
    }
    s_BlockNew = 1;

    Uint8 *p;

	while( RAMArray[m_RamIndex] )
	{
		m_RamIndex ++;
		m_RamIndex = m_RamIndex%enMaxRam;
	}

	p = (Uint8*)malloc( size + sizeof(StRAMHead) );

	StRAMHead *q = (StRAMHead*)p;
	q->index = m_RamIndex;
	q->level = m_AllocateLevel;
	q->size  = size;
	q->id[0] = 'G';
	q->id[1] = 'X';

	RAMArray[m_RamIndex] = p;

	m_UseMemory += size;

	if( m_UseMemory >= m_MaxUseMemory )
	{
		m_MaxUseMemory = m_UseMemory;
	}

	m_RamIndex ++;
	m_RamIndex = m_RamIndex%enMaxRam;

    s_BlockNew = 0;

    return (void*)&p[ sizeof(StRAMHead) ];
}


void* operator new[]( size_t size )
{
    while(s_BlockNew>0)
    {
        gxLib::Sleep(1);
    }
    s_BlockNew = 1;

    Uint8 *p;

	while( RAMArray[m_RamIndex] )
	{
		m_RamIndex ++;
		m_RamIndex = m_RamIndex%enMaxRam;
	}

	p = (Uint8*)malloc( size + sizeof(StRAMHead) );

	StRAMHead *q = (StRAMHead*)p;
	q->index = m_RamIndex;
	q->level = m_AllocateLevel;
	q->size  = size;
	q->id[0] = 'G';
	q->id[1] = 'X';

	RAMArray[m_RamIndex] = p;

	m_UseMemory += size;

	if( m_UseMemory >= m_MaxUseMemory )
	{
		m_MaxUseMemory = m_UseMemory;
	}

	m_RamIndex ++;
	m_RamIndex = m_RamIndex%enMaxRam;

    s_BlockNew = 0;

    return (void*)&p[ sizeof(StRAMHead) ];
}


void operator delete( void* ptr )
{
    while(s_BlockDel>0)
    {
        gxLib::Sleep(1);
    }
    s_BlockDel = 1;

    Uint8 *p = (Uint8 *)ptr;

#if defined(GX_RELEASE) || defined(GX_MASTER)
	if( ptr == NULL ) return;
#endif

	p -= sizeof(StRAMHead);

	if (m_bInitilizedMemoryUnit)
	{
        StRAMHead* q = (StRAMHead*)p;
        if (q->id[0] == 'G' && q->id[1] == 'X')
        {
            RAMArray[q->index] = NULL;
            m_UseMemory -= q->size;
        }
	}

	StRAMHead* q = (StRAMHead*)p;

	if (q->id[0] == 'G' && q->id[1] == 'X')
	{
		free(p);
	}
	else
	{
		ASSERT(1);
	}
    s_BlockDel = 0;

}

void operator delete[]( void* ptr )
{
    while(s_BlockDel>0)
    {
        gxLib::Sleep(1);
    }
    s_BlockDel = 1;

    Uint8 *p = (Uint8 *)ptr;

#if defined (GX_RELEASE) || defined(GX_MASTER)
	if( ptr == NULL ) return;
#endif

	p -= sizeof(StRAMHead);

	StRAMHead *q = (StRAMHead*)p;

	if (m_bInitilizedMemoryUnit)
	{
        if (q->id[0] == 'G' && q->id[1] == 'X')
        {
            RAMArray[q->index] = NULL;
            m_UseMemory -= q->size;
        }
	}

	if (q->id[0] == 'G' && q->id[1] == 'X')
	{
		free(p);
	}
	else
	{
		ASSERT(1);
	}
    s_BlockDel = 0;
}

void* REALLOC( void* p , unsigned int sz )
{
	size_t copySZ = sz;
	if (p)
	{
		StRAMHead* q = (StRAMHead*)(((Uint8*)p) - sizeof(StRAMHead));

		copySZ = sz;

		if (sz > q->size) copySZ = q->size;
	}

	Uint8* pRet = new Uint8[sz];

	if (p)
	{
		gxUtil::MemCpy(pRet, p, copySZ);
	}

	return pRet;
}


