﻿// --------------------------------------------------------------------
//Android専用
//
// --------------------------------------------------------------------
#define PLATFORM_ANDROID

//#define FILE_FROM_ZIP
#define _USE_OPENGL
#define _USE_OPENAL

















#define _CRT_SECURE_NO_WARNINGS

#if 0
	#define GX_BUILD_OPTIONx86
#else
	#define GX_BUILD_OPTIONx64
#endif


#define INDEXBUFFER_BIT (32)

#define JAVA_PROGRAM_PATH "jp/co/garuru/GameGirlActivity"
#define  LOG_TAG    "gxLib18"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

#include <jni.h>

#include <android/log.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <locale.h>
#include <wchar.h>

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <stdio.h>
#include <stdlib.h>

#include <stddef.h>
#include <stdint.h>	//size_t で必要

#include <assert.h>
#include <string.h>
#include <math.h>
#include <cctype>
#include <string>
#include <iostream>
#include <map>
#include <vector>
#include <functional>

//----------------------------------------------------
//----------------------------------------------------
void Movie();
Uint32 GetDebugTime();
Uint32 GetVsyncRate();
void ExecuteApp( char *appname );
gxBool IsFullScreen();
void ChangeWindowMode( gxBool bWindow );
void ScreenCapture();

//typedef struct StCustomVertex
//{
//	Float32 x,y,z,rhw;
//	Float32 r,g,b,a;
//	Float32 u,v;
//
//	Float32 sx,sy;
//	Float32 cx,cy;
//	Float32 rot;
//	Float32 fx,fy;			//flip
//	Float32 r2,g2,b2,a2;	//bllend
//
//} StCustomVertex;

//class CCommandList
//{
//public:
//	Uint32 eCommand;
//	Uint32 arg[4];
//	Sint32 x,y;
//	void*  pString;
//	Float32 opt;
//private:
//
//};
//
class CAndroid
{
public:
	enum {
		enSamplingNearest,
		enSamplingBiLenear,
	};

	CAndroid()
	{
		m_pJavaVM = NULL;
		m_PackageName[0] = 0x00;
		
		m_pOBBPath[0][0] = 0x00;
		m_pOBBPath[1][0] = 0x00;
        m_pSDPath[0] = 0x00;

		m_WebViewURLString[0] = 0x00;

		m_bCheckExtentionFile = gxFalse;
	}

	void SetPackageName( const gxChar *pPackageName )
	{
		sprintf( m_PackageName , "%s" , pPackageName );
	}

	gxChar* GetPackageName()
	{
		return m_PackageName;
	}

	void SetOBB_Path( Uint32 index , gxChar *pOBBPath )
	{
		//------------------------------------------------------------
		//------------------------------------------------------------

		m_bCheckExtentionFile = gxTrue;

		if( pOBBPath == NULL )
		{
			m_pOBBPath[index][0] = 0x00;
		}
		else
		{
			sprintf( m_pOBBPath[index] , "%s" , pOBBPath );
		}
	}

    gxChar* GetOBB_Path( Uint32 index = 0 )
    {
        //------------------------------------------------------------
        //OBB
        //------------------------------------------------------------
        if( m_pOBBPath[index][0] == 0x00 ) return NULL;

        return &m_pOBBPath[index][0];
    }

	void SetSD_Path( Uint32 index , gxChar *pSDPath )
	{
		//------------------------------------------------------------
		//------------------------------------------------------------

		if( pSDPath == NULL )
		{
			m_pSDPath[0] = 0x00;
		}
		else
		{
			sprintf( m_pSDPath , "%s" , pSDPath );
		}
	}
    gxChar* GetSD_Path( Uint32 index = 0 )
    {
        //------------------------------------------------------------
        //OBB
        //------------------------------------------------------------
        if( m_pSDPath[index] == 0x00 ) return NULL;

        return &m_pSDPath[0];
    }

	gxBool IsCheckOBB()
	{
		return m_bCheckExtentionFile;
	}


	void Vibration( Sint32 sec );

	void SetAssetManager( AAssetManager *am )
	{
		m_pAssetManager = am;
	}

	void HttpRequest( Sint32 index , gxChar* url )
	{
		sprintf( m_HTTPURLString , "%s",url );
		m_HTTPURLIndex = index;
	}

	Uint32 GetRenderingFilter()
	{
		return m_RenderFilter;
	}

	static gxChar* GetClipBoardText();
	static void  SetClipBoardText( gxChar *pString );


	JavaVM *m_pJavaVM;
    gxChar m_WebViewURLString[1024]={0};
    gxChar m_HTTPURLString[1024]={0};
	Sint32 m_HTTPURLIndex = 0;
	SINGLETON_DECLARE( CAndroid )

private:

	gxChar m_PackageName[FILENAMEBUF_LENGTH];
	gxChar m_pOBBPath[2][FILENAMEBUF_LENGTH];
    gxChar m_pSDPath[FILENAMEBUF_LENGTH];

	gxBool m_bCheckExtentionFile;

    AAssetManager*  m_pAssetManager;

	Uint32 m_RenderFilter = enSamplingNearest;
};

