//-----------------------------------------------------------------------
//
// 
//
//-----------------------------------------------------------------------

#include <gxLib.h>
#include "CMemory.h"

static size_t m_UseMemory;
static size_t m_MaxUseMemory;
Uint8 m_AllocateLevel = 0;

Uint32 m_RamIndex = 0;

//#define LEAK_CHECK


#ifdef LEAK_CHECK
	struct debugRAM
	{
		void* pData;
	};
	#define RAM_ARRAY_MAX (1024*1024)
	debugRAM RAMArray[RAM_ARRAY_MAX] = {0};
#endif

gxBool m_bInitilizedMemoryUnit = gxFalse;

void SetAllocateLevel( Sint8 level = -1 )
{
	m_AllocateLevel = level;
}

void MemoryInit()
{
	m_bInitilizedMemoryUnit = gxTrue;
	m_RamIndex = 0;

#ifdef LEAK_CHECK
	gxUtil::MemSet( RAMArray , 0x00 , sizeof(RAMArray) );
#endif
}


#ifdef LEAK_CHECK
void checkMemoryWarning()
{
	Sint32 nnn = 0;
	switch (m_RamIndex) {
	case 171:
		nnn++;
		break;
	}

}
#endif
void MemoryDestroy( Sint8 level)
{
#ifdef LEAK_CHECK
	for(Sint32 ii=0; ii< RAM_ARRAY_MAX; ii++ )
	{
		if(RAMArray[ii].pData )
		{
			StRAMHead *p = (StRAMHead*)&RAMArray[ii].pData;

			if ( ( level == -1 ) || ( p->level == level ) ) 
			{
				//リークを発見
				StRAMHead *p = (StRAMHead*)&RAMArray[ii].pData;

				delete p;
			}
		}
	}
#endif
	m_bInitilizedMemoryUnit = gxFalse;
}


void UpdateMemoryStatus( Uint32* uNow , Uint32* uTotal , Uint32* uMax )
{
	//MBで返す

	*uNow   = m_UseMemory;
	*uTotal = GAME_HEAP_SIZE;
	*uMax   = m_MaxUseMemory;
}

int s_BlockNew = 0;
int s_BlockDel = 0;

void* operator new( size_t size )
{
    while(s_BlockNew>0)
    {
        gxLib::Sleep(1);
    }
    s_BlockNew = 1;

    Uint8 *p;

	p = (Uint8*)malloc( size + sizeof(StRAMHead) );

	StRAMHead *q = (StRAMHead*)p;
	q->index = m_RamIndex;
	q->level = m_AllocateLevel;
	q->size  = size;
	q->id[0] = 'G';
	q->id[1] = 'X';


#ifdef LEAK_CHECK
	checkMemoryWarning();
	RAMArray[m_RamIndex].pData = p;
	m_RamIndex++;
#endif
	m_UseMemory += size;

	if( m_UseMemory >= m_MaxUseMemory )
	{
		m_MaxUseMemory = m_UseMemory;
	}

    s_BlockNew = 0;

    return (void*)&p[ sizeof(StRAMHead) ];
}


void* operator new[]( size_t size )
{
    while(s_BlockNew>0)
    {
        gxLib::Sleep(1);
    }
    s_BlockNew = 1;

    Uint8 *p;

	p = (Uint8*)malloc( size + sizeof(StRAMHead) );

	StRAMHead *q = (StRAMHead*)p;
	q->index = m_RamIndex;
	q->level = m_AllocateLevel;
	q->size  = size;
	q->id[0] = 'G';
	q->id[1] = 'X';


#ifdef LEAK_CHECK
	checkMemoryWarning();
	if (RAMArray[m_RamIndex].pData)
	{
		int n = 0;
		n++;
	}
	RAMArray[m_RamIndex].pData = p;
	m_RamIndex++;
#endif
	m_UseMemory += size;

	if( m_UseMemory >= m_MaxUseMemory )
	{
		m_MaxUseMemory = m_UseMemory;
	}

    s_BlockNew = 0;

    return (void*)&p[ sizeof(StRAMHead) ];
}


void operator delete( void* ptr )
{
    while(s_BlockDel>0)
    {
        gxLib::Sleep(1);
    }
    s_BlockDel = 1;

    Uint8 *p = (Uint8 *)ptr;

#if defined(GX_RELEASE) || defined(GX_MASTER)
	if( ptr == NULL ) return;
#endif

	p -= sizeof(StRAMHead);

	if (m_bInitilizedMemoryUnit)
	{
        StRAMHead* q = (StRAMHead*)p;
        if (q->id[0] == 'G' && q->id[1] == 'X')
        {
#ifdef LEAK_CHECK
			RAMArray[q->index].pData = NULL;
#endif
			m_UseMemory -= q->size;
        }
	}

	StRAMHead* q = (StRAMHead*)p;

	if (q->id[0] == 'G' && q->id[1] == 'X')
	{
		free(p);
	}
	else
	{
		ASSERT(1);
	}
    s_BlockDel = 0;

}

void operator delete[]( void* ptr )
{
    while(s_BlockDel>0)
    {
        gxLib::Sleep(1);
    }
    s_BlockDel = 1;

    Uint8 *p = (Uint8 *)ptr;

#if defined (GX_RELEASE) || defined(GX_MASTER)
	if( ptr == NULL ) return;
#endif

	p -= sizeof(StRAMHead);

	StRAMHead *q = (StRAMHead*)p;

	if (m_bInitilizedMemoryUnit)
	{
        if (q->id[0] == 'G' && q->id[1] == 'X')
        {
#ifdef LEAK_CHECK
			RAMArray[q->index].pData = NULL;
#endif
			m_UseMemory -= q->size;
        }
	}

	if (q->id[0] == 'G' && q->id[1] == 'X')
	{
		free(p);
	}
	else
	{
		ASSERT(1);
	}
    s_BlockDel = 0;
}

void* REALLOC( void* p , unsigned int sz )
{
	size_t copySZ = sz;
	if (p)
	{
		StRAMHead* q = (StRAMHead*)(((Uint8*)p) - sizeof(StRAMHead));

		copySZ = sz;

		if (sz > q->size) copySZ = q->size;
	}

	Uint8* pRet = new Uint8[sz];

	if (p)
	{
		gxUtil::MemCpy(pRet, p, copySZ);
	}

	return pRet;
}


