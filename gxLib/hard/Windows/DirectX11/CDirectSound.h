﻿#ifndef _DIRECTSOUND_H_
#define _DIRECTSOUND_H_

#ifdef _USE_DIRECTSOUND

//#include <dsound.h>

#include "../../../util/CFileWave.h"

class CDirectSound
{
public:
	enum{

		enSoundMax = MAX_SOUND_NUM,
	};

	CDirectSound();
	~CDirectSound();

	gxBool Init();

	void Action();


	void SetAudioEnable( gxBool bSoundOn );

	void SetVolume( Sint32 no , Float32 fVolume );

	void SetMasterVolume( Float32 fMasterVolume );

	SINGLETON_DECLARE( CDirectSound );

private:

	gxBool IsPlay( int sound );
	void Release();
	void PlaySoundEffect(int sound);
	void StopSoundEffect( int sound);
	void SetSoundFrequency( int sound , Float32 ratio );
	gxBool Clear();

	gxBool loadFile( Sint32 n , char* pFileName );
	gxBool readFile( Sint32 n , Uint8 *pData  , Uint32 uSize , StWAVEFORMATEX* pFormat );

	Float32 m_fMasterVolume;

};

#endif
#endif
