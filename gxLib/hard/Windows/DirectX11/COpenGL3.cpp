﻿
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxOrderManager.h>
#include <gxLib/gxRender.h>
#include <gxLib/gxTexManager.h>
#include <gxLib/gxDebug.h>
#include "gxLibResource.h"
#include "COpenGL3.h"

SINGLETON_DECLARE_INSTANCE( COpenGL3 );

#define FORMAT_TEXTURE_2D GL_RGB	//GL_RGBA
enum {
	enOptionNum = 4,
};

#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "lib/glew-2.1.0/lib/Release/x64/glew32.lib")

void _CheckError();
const GLchar  *vtxShader1[] = {
	//-1.0f ～ +1.0fの座標で頂点データを受け取る
	"#version 330\n",
	"in   vec4  a_position;\n",
	"in   vec4  a_color;\n",
	"in   vec2  a_texCoord;\n",
	"in   vec2  a_nmlCoord;\n",
	"in   vec2  a_scale;\n",
	"in   vec2  a_offset;\n",
	"in   float a_rotation;\n",
	"in   vec2  a_flip;\n",
	"in   vec4  a_blend;\n",
	"in   vec3  a_pointlight_pos;\n",
	"in   vec3  a_pointlight_rgb;\n",
	"in   float a_pointlight_length;\n",
    "in   vec4 a_options;\n",
	"uniform vec2 u_screen;\n",
	"out  vec4  v_vtxColor;\n",
	"out  vec2  v_texCoord;\n",
	"out  vec2  v_nmlCoord;\n",
	"out  vec4  v_position;\n",
	"out  vec4  v_blend;\n",
	"out   vec3  v_pointlight_pos;\n",
	"out   vec3  v_pointlight_rgb;\n",
	"out   float v_pointlight_length;\n",
    "out   vec4  v_options;\n",
	"void main(void)\n",
	"{\n",
	"v_vtxColor    = a_color;\n",
	"v_texCoord    = a_texCoord;\n",
	"v_nmlCoord    = a_nmlCoord;\n",
	"v_blend       = a_blend;\n",
	"v_pointlight_pos  = a_pointlight_pos;\n",
	"v_pointlight_rgb  = a_pointlight_rgb;\n",
	"v_pointlight_length  = a_pointlight_length;\n",
    "v_options  = a_options;\n",
	"vec2 pos = vec2(0.0 , 0.0);\n",
	"pos.x  = a_position.x * a_scale.x * u_screen.x;\n",	//ここで解像度をかけておかないと回転の時に比率が合わなくなる
	"pos.y  = a_position.y * a_scale.y * u_screen.y;\n",
	"vec2 pos2;\n",
	"pos2.x  = (pos.x * cos( a_rotation )      - pos.y * sin( a_rotation )*-1.0);\n",
	"pos2.y  = (pos.x * sin( a_rotation )*-1.0 + pos.y * cos( a_rotation ) );\n",
	"pos2.x  = pos2.x * a_flip.x;\n",
	"pos2.y  = pos2.y * a_flip.y;\n",
	"vec2 pos3;\n",
	"pos3.x  = pos2.x / u_screen.x + a_offset.x;\n",		//かけておいた解像度を元に戻す
	"pos3.y  = pos2.y / u_screen.y + a_offset.y;\n",
"v_position    = vec4( pos3.x , pos3.y , 0.0 , 0.0 );\n",
	"gl_Position = vec4( pos3.x , pos3.y , 0.0 , 1.0 );\n",
	"}\n",
};

//標準シェーダー
const GLchar  *pxlShader1[] = {
		"in vec2 v_texCoord;\n",
		"in vec4 v_vtxColor;\n",
		"in vec4 v_position;\n",
		"in vec4 v_blend;\n",
		"uniform sampler2D u_textureID1;\n",
		"void main(void)\n",
		"{\n",
			"vec4 color = texture2D( u_textureID1 , v_texCoord.xy);\n",
			"vec3 rgb = vec3( color.r , color.g , color.b)*v_vtxColor;\n",
			"vec4 blend = v_blend;\n",
			"rgb = rgb*(1.0 - blend.a) + (vec3(blend.r , blend.g , blend.b)*(blend.a) );\n",
			"gl_FragColor = vec4(rgb.r , rgb.g, rgb.b, color.a)*v_vtxColor.a;\n",
		"}\n",
};


//Bloomシェーダー
const GLchar  *pxlShader2[] = {
		//Bloomシェーダー
		"in vec2 v_texCoord;\n",
		"in vec4 v_vtxColor;\n",
		"in vec4 v_position;\n",
		"in vec4 v_blend;\n",
//		"in vec4 v_option;\n",
		"uniform float u_option[4];\n",
		"uniform sampler2D u_textureID1;\n",
		"void main(void)\n",
		"{\n",
			"vec4 col0 = texture2D( u_textureID1 , v_texCoord.xy );\n",
			"float fBrightness = max( col0.r , max(col0.g , col0.b ) );\n",

			"if( fBrightness >= u_option[0] ){ \n",
			"	gl_FragColor = col0;\n",
			"}else{\n",
			"	gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);\n",
			"};\n",
		"}\n",
};


const GLchar  *pxlShader3[] = {
	//GaussianDOFシェーダー
	"in vec2 v_texCoord;\n",
	"in vec4 v_vtxColor;\n",
	"in vec4 v_position;\n",
	"in vec4 v_blend;\n",
//	"in  vec2 v_screen;\n",
//	"in  vec4 v_option;\n",
	"uniform vec2 u_screen;\n",
	"uniform float u_option[4];\n",
	"uniform float u_weight[10];\n",
	"uniform sampler2D u_textureID;\n",
	"void main(void)\n",
	"{\n",
"   	vec2  fc;\n",
"   	vec3  destColor = vec3(0.0);\n",
"		if( u_option[0] < 0.5 )\n",
"		{\n",
"			fc = vec2(gl_FragCoord.x, gl_FragCoord.y);\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-9.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[9];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-8.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[8];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-7.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[7];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-6.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[6];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-5.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[5];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-4.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[4];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-3.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[3];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-2.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[2];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-1.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[1];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x-0.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[0];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+1.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[1];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+2.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[2];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+3.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[3];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+4.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[4];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+5.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[5];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+6.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[6];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+7.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[7];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+8.0)/u_screen[0] , (fc.y+0.0)/u_screen[1] )).rgb*u_weight[8];\n",
"			gl_FragColor = vec4(destColor, 1.0)*v_vtxColor.a;\n",
"        }else{\n",
"			fc = vec2(gl_FragCoord.x, gl_FragCoord.y);\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y-9.0)/u_screen[1] )).rgb*u_weight[9];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y-8.0)/u_screen[1] )).rgb*u_weight[8];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y-7.0)/u_screen[1] )).rgb*u_weight[7];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y-6.0)/u_screen[1] )).rgb*u_weight[6];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y-5.0)/u_screen[1] )).rgb*u_weight[5];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y-4.0)/u_screen[1] )).rgb*u_weight[4];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y-3.0)/u_screen[1] )).rgb*u_weight[3];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y-2.0)/u_screen[1] )).rgb*u_weight[2];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y-1.0)/u_screen[1] )).rgb*u_weight[1];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y-0.0)/u_screen[1] )).rgb*u_weight[0];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y+1.0)/u_screen[1] )).rgb*u_weight[1];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y+2.0)/u_screen[1] )).rgb*u_weight[2];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y+3.0)/u_screen[1] )).rgb*u_weight[3];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y+4.0)/u_screen[1] )).rgb*u_weight[4];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y+5.0)/u_screen[1] )).rgb*u_weight[5];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y+6.0)/u_screen[1] )).rgb*u_weight[6];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y+7.0)/u_screen[1] )).rgb*u_weight[7];\n",
"           destColor += texture2D( u_textureID, vec2( (fc.x+0.0)/u_screen[0] , (fc.y+8.0)/u_screen[1] )).rgb*u_weight[8];\n",
"			gl_FragColor = vec4(destColor, 1.0)*v_vtxColor.a;\n",
"        }\n",
	"}\n",

};


//Rasterシェーダー
const GLchar  *pxlShader4[] = {
		"in vec2 v_texCoord;\n",
		"in vec4 v_vtxColor;\n",
		"in vec4 v_position;\n",
		"in vec4 v_blend;\n",
		"uniform float u_option[4];\n",
		"uniform sampler2D u_textureID1;\n",
		"void main(void)\n",
		"{\n",
			"float num    = u_option[0];\n",
			"float range  = u_option[1];\n",
			"float time   = u_option[2];\n",
			"vec2 texCoord = v_texCoord.xy;\n",
			"texCoord.x += cos( (texCoord.y+time)*num)*range;\n",
			"vec4 color = texture2D( u_textureID1 , texCoord.xy);\n",
			"vec3 rgb = vec3( color.r , color.g , color.b)*v_vtxColor;\n",
			"vec4 blend = v_blend;\n",
			"rgb = rgb*(1.0 - blend.a) + (vec3(blend.r , blend.g , blend.b)*(blend.a) );\n",
			"gl_FragColor = vec4(rgb.r , rgb.g, rgb.b, color.a)*v_vtxColor.a;\n",
		"}\n",
};

//法線マップシェーダー

const GLchar* pxlShader5[] = {
		"in vec2 v_texCoord;\n",
		"in vec2 v_nmlCoord;\n",
		"in vec4 v_vtxColor;\n",
		"in vec4 v_position;\n",
		"in vec4 v_blend;\n",
		"in vec3  v_pointlight_pos;\n",
		"in vec3  v_pointlight_rgb;\n",
		"in float v_pointlight_length;\n",
		"uniform sampler2D u_textureID1;\n",
		"uniform sampler2D u_textureID2;\n",
		"uniform sampler2D u_textureID3;\n",
		"void main(void)\n",
		"{\n",
			"vec4 color1  = texture2D( u_textureID1  , v_texCoord.xy);\n",								//アルベド
			"vec4 color2  = texture2D( u_textureID2  , v_texCoord.xy+v_nmlCoord.xy );\n",				//法線
			"vec3 p1 = vec3( v_pointlight_pos[0] , v_pointlight_pos[1] , v_pointlight_pos[2]);\n",		//ライトのポジション
			"vec3 p2 = vec3( v_position[0]       , v_position[1]       , v_position[2]);\n",			//ピクセルのポジション
			"vec3 p3 = vec3( p1.x-p2.x , p1.y- p2.y, p1.z - p2.z);\n",									//点光源へのベクトル
			"vec3 p4 = vec3( (color2.r-0.5)*2.0 , (color2.g-0.5)*2.0, (color2.b-0.5)*2.0);\n",			//ピクセルの法線ベクトル
			"vec3 p5 = vec3( v_position[0], v_position[1], v_pointlight_pos[2]);\n",										//ピクセルと点光源の2次元の距離
			//ライトからの距離を算出して光の届く範囲を得る
			"float lightlength = distance(p1 , p5);\n",											//光の届く距離
			"lightlength = 1.0 - clamp(lightlength , 0.0 , v_pointlight_length ) / v_pointlight_length;\n",
			//光源と面の法線ベクトルの内積から傾きdを得る、dは0～1の範囲で暗さとなる
			"vec3 vec3_light   = normalize( p3 );\n",
			"vec3 vec3_surface = normalize( p4 );\n",
			"float d = dot( vec3_light , vec3_surface );\n",
			"d = clamp(d , 0.0 , 1.0 );\n",
			//マテリアルの計算
			"float metalic   = color2.a;\n",														//メタリック＝映り込み（最大0.5）
			"float roughness = 1.0-metalic;\n",															//ラフネス  = 光沢

			//光沢（明るいところはより明るく、暗いところはより暗くする）
			"vec3 specular = metalic*vec3( 1.0 , 1.0 , 1.0)*(d*pow(d,20.0*(1.0-roughness)));\n",				//スペキュラーの強さ（ラフネスから影響を受ける）

			//映り込みの場所を得る
			"vec2 texel;\n",
			"texel.x = (v_position.x+1.0)/2 + (1.0+vec3_surface.x)/2.0;\n",
			"texel.y = (v_position.y+1.0)/2 + (1.0+vec3_surface.y)/2.0;\n",
			"texel.x = texel.x/2.0;\n",
			"texel.y = texel.y/2.0;\n",
			"texel.x = (v_position[0]+1.0*(p4.x)/2.0);\n",	
			"texel.y = (v_position[1]+1.0*(p4.y)/2.0);\n",
			"texel.x = clamp(texel.x , -1.0 , 1.0);\n",
			"texel.y = clamp(texel.y , -1.0 , 1.0);\n",
			"texel.x = (texel.x+1.0)/2.0;\n",
			"texel.y = (texel.y+1.0)/2.0;\n",
			"vec4 env = texture2D( u_textureID3  , texel.xy );\n",										//環境テクスチャ
			//"env = vec4(v_pointlight_length,v_pointlight_length,v_pointlight_length,1.0);\n",										//環境テクスチャ

			//環境テクスチャ
			"float a1n = metalic*1.0;\n",
			"float a1r = 1.0-a1n;\n",
			"vec3 rgb = vec3( color1.r*a1r + env.r*a1n , color1.g*a1r+env.g*a1n , color1.b*a1r +env.b*a1n)*v_vtxColor.rgb;\n",		//色：（アルベド＋映り込み）＊頂点カラー（先に頂点カラーをかけないと色がついたスペキュラになる）
			"rgb = rgb*d*lightlength;\n",																							//影：法線方向による陰影と光の減衰率を掛け合わせる
			"rgb = rgb+specular;\n",																								//光：スペキュラ成分は最後に足す
			"gl_FragColor = vec4( rgb.x , rgb.y, rgb.z, color1.a)*v_vtxColor.a;\n",													//α：最後に半透明度合いを計算する
			//"gl_FragColor = vec4( (env.r+specular.r)*lightlength , (env.g+specular.g)*lightlength, (env.b+specular.b)*lightlength, color1.a)*v_vtxColor.a;\n",													//最後に半透明度合いを計算する
			//"vec3 rgb = vec4( env.r , env.g, env.b, color1.a)*v_vtxColor.a;\n",													//最後に半透明度合いを計算する
			//液体金属にしたい場合は法線マップを無効にする
			//"rgb = (rgb + specular)*d*lightlength;\n",																			//法線方向による影と光の減衰率を掛け合わせる
		"}\n",
};

//フォントシェーダー
const GLchar  *pxlShader6[] = {
    "in vec2 v_texCoord;\n",
    "in vec4 v_vtxColor;\n",
    "in vec4 v_blend;\n",
    "in vec4 v_options;\n",
    "uniform sampler2D u_textureID1;\n",
    "void main(void)\n",
    "{\n",
    "vec4 color1 = texture2D( u_textureID1 , v_texCoord.xy);\n",
    "vec4 color2 = vec4( v_blend.r , v_blend.g , v_blend.b , 1.0 );\n",
    "float mode = v_options[0];\n",
    "float alpha = color1.r;\n",
    "if(mode == 1.0) alpha = color1.g;\n",
    "if(mode == 2.0) alpha = color1.b;\n",
	"if(mode == 3.0) alpha = color1.a;\n",
//	"vec4 rgb1   = vec4( alpha * (1.0 - v_blend.a) , alpha * (1.0 - v_blend.a) , alpha * (1.0 - v_blend.a ) , alpha );\n",
//    "vec4 rgb2   = vec4( color2.r * (      v_blend.a) , color2.g * (      v_blend.a) , color2.b * (      v_blend.a ) , 1.0 );\n",
//    "gl_FragColor = vec4( rgb1.r + rgb2.r , rgb1.g + rgb2.g , rgb1.b + rgb2.b , alpha )*v_vtxColor;\n",
    "gl_FragColor = vec4( 1.0 , 1.0 , 1.0 , alpha )*v_vtxColor;\n",
	"}\n",
};

const GLchar* pxlShader0[] = {
		"in vec2 v_texCoord;\n",
		"in vec4 v_vtxColor;\n",
		"in vec4 v_position;\n",
		"in vec4 v_blend;\n",
		"uniform sampler2D u_textureID1;\n",
		"uniform sampler2D u_texNormal;\n",
		"uniform float u_PLight[10];\n",
		"void main(void)\n",
		"{\n",
			"vec4 color1  = texture2D( u_textureID1 , v_texCoord.xy);\n",
			"vec4 color2  = texture2D( u_texNormal  , v_texCoord.xy);\n",
			"vec3 p1 = vec3( u_PLight[0] , u_PLight[1] , u_PLight[2]);\n",
			"vec3 p2 = vec3( v_position[0] , v_position[1] , v_position[2]);\n",
			"vec3 p3 = vec3( p1.x-p2.x , p1.y- p2.y, p1.z - p2.z);\n",		//点光源へのベクトル
			"vec3 p4 = vec3( (color2.r-0.5)*2.0 , (color2.g-0.5)*2.0, (color2.b-0.5)*2.0);\n",			//面の法線ベクトル
			"vec3 vec3_light   = normalize( p3 );\n",
			"vec3 vec3_surface = normalize( p4 );\n",
			"float d = dot( vec3_light , vec3_surface );\n",
			"d = clamp(d , 0.0 , 1.0 );\n",
			"float l = 0.0;\n",////length( vec3(p2.x-p1.x , p2.y-p1.y , 0.0) );\n",
			"d = clamp(d , 0.0 , 1.0 )*(1.0-l);\n",
			"vec3 rgb = vec3( color1.r , color1.g , color1.b)*d;\n",
			"gl_FragColor = vec4( rgb.x , rgb.y, rgb.z, 1.0)*v_vtxColor.a;\n",
		"}\n",
};

COpenGL3::COpenGL3()
{
	m_bInitCompleted = gxFalse;

	m_bUpConvert = gxFalse;
//	m_bUpConvert = gxTrue;

	m_b3DView = gxFalse;

	m_GameW = WINDOW_W;
	m_GameH = WINDOW_H;

	m_pVertexBuffer = NULL;
	m_pIndexBuffer  = NULL;

	m_ShadingWindowW = m_GameW;
	m_ShadingWindowH = m_GameH;
}


COpenGL3::~COpenGL3()
{
	for( Sint32 ii=0; ii<enTexturePageMax; ii++ )
	{
		if( m_TextureData[ ii ].texture2D )
		{
		//	m_TextureData[ ii ].texture2D->Release();
		}
	}

	for( Sint32 ii=0; ii<enFrameBufferNum; ii++ )
	{
		if( m_OffScreenTexture[ ii ].texture2D )
		{
		//	m_TextureData[ ii ].texture2D->Release();
		}
	}


		//if( m_TextureData[ ii ].shaderResourceView )
		//{
		//	m_TextureData[ ii ].shaderResourceView->Release();
		//}

		//if( m_TextureData[ ii ].renderTargetView )
		//{
		//	m_TextureData[ ii ].renderTargetView->Release();
		//}

	SAFE_DELETES(m_pVertexBuffer);
	SAFE_DELETES(m_pIndexBuffer);
/*
	if( m_inputLayout )  m_inputLayout->Release();
	if( m_vertexBuffer ) m_vertexBuffer->Release();
	if( m_indexBuffer )  m_indexBuffer->Release();
	if( m_vertexShader ) m_vertexShader->Release();
	if( m_pixelShader )  m_pixelShader->Release();
	if( m_constantBuffer )  m_constantBuffer->Release();
*/
}


void COpenGL3::Init()
{
	//------------------------------------------------------
	// 頂点シェーダー ファイルを読み込んだ後、シェーダーと入力レイアウトを作成します。
	//------------------------------------------------------

	int pixelFormat;

	static PIXELFORMATDESCRIPTOR pfd = {
		sizeof(PIXELFORMATDESCRIPTOR),   // size of this pfd 
		1,                     // version number 
		PFD_DRAW_TO_WINDOW |   // support window 
		PFD_SUPPORT_OPENGL |   // support OpenGL 
		PFD_DOUBLEBUFFER,      // double buffered 
		PFD_TYPE_RGBA,         // RGBA type 
		24,                    // 24-bit color depth 
		0, 0, 0, 0, 0, 0,      // color bits ignored 
		0,                     // no alpha buffer 
		0,                     // shift bit ignored 
		0,                     // no accumulation buffer 
		0, 0, 0, 0,            // accum bits ignored 
		32,                    // 32-bit z-buffer 
		1,                     // no stencil buffer 
		1,                     // no auxiliary buffer 
		PFD_MAIN_PLANE,        // main layer 
		0,                     // reserved 
		0, 0, 0                // layer masks ignored 
	};

	pixelFormat = ChoosePixelFormat( CWindows::GetInstance()->m_WinDC, &pfd);
	SetPixelFormat(CWindows::GetInstance()->m_WinDC, pixelFormat, &pfd);

	m_hRC = wglCreateContext(CWindows::GetInstance()->m_WinDC );
	wglMakeCurrent( CWindows::GetInstance()->m_WinDC, m_hRC);

	GLenum err = glewInit();

	//	行列
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	//Reset
	glViewport(0, 0, m_GameW, m_GameH);

	BOOL(WINAPI *wglSwapIntervalEXT)(int) = NULL;

	wglSwapIntervalEXT = (BOOL(WINAPI*)(int))wglGetProcAddress("wglSwapIntervalEXT");

	if (wglSwapIntervalEXT)
	{
		wglSwapIntervalEXT(0);
	}

	int MaxUnitNum;
	/*
		glGetIntegerv(GL_MAX_TEXTURE_UNITS,&MaxUnitNum);
	GX_DEBUGLOG( "GL_MAX_TEXTURE_UNITS = %d", MaxUnitNum );			//OpenGL 3で非推奨

	glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS,&MaxUnitNum);
	GX_DEBUGLOG( "GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS = %d", MaxUnitNum );

	glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS,&MaxUnitNum);
	GX_DEBUGLOG( "GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS = %d", MaxUnitNum );

	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS,&MaxUnitNum);
	GX_DEBUGLOG( "GL_MAX_TEXTURE_IMAGE_UNITS = %d", MaxUnitNum );
*/

	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS_ARB,&MaxUnitNum);
	GX_DEBUGLOG( "GL_MAX_TEXTURE_IMAGE_UNITS_ARB = %d", MaxUnitNum );

	struct stGPU
	{
		Uint32 maxSpeed;
		Uint32 maxRam;
		Uint32 maxTextureUnit;
	};
	//gxDebug::GetInstance()->SetGPUPerformance();
/*
	err = glGetError();
	GLuint texture2D;

	Uint8 *pData = (Uint8*)malloc(1024*1024*4);

	glActiveTexture(GL_TEXTURE0 + 0);
	for(Sint32 ii=0; ii<1024*1024; ii++ )
	{
		glGenTextures( 1, &texture2D );
		glBindTexture( GL_TEXTURE_2D, texture2D );
		glPixelStorei( GL_UNPACK_ALIGNMENT, 1);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1024, 1024, 0, GL_RGBA, GL_UNSIGNED_BYTE, pData );
		err = glGetError();
		if( err )
		{
			err = err;
		}
	}

	free(pData);
*/

	glActiveTexture(GL_TEXTURE0 );

	initShader();

	initTexture();

	initVBO();

	Reset();

		glEnable ( GL_TEXTURE_2D );
/*毒*///		glEnable ( GL_NORMALIZE );
/*毒*///		glEnable ( GL_ALPHA_TEST );
		glEnable ( GL_BLEND );
		glDisable( GL_DEPTH_TEST );

	m_bInitCompleted = gxTrue;

}


// このメソッドは、CoreWindow オブジェクトが作成 (または再作成) されるときに呼び出されます。
void COpenGL3::Reset()
{
	//フルスクリーン切り替え

	DEVMODE devmode;

	EnumDisplaySettings(NULL, 0, &devmode);

	if( !CWindows::GetInstance()->IsBatchMode() )
	{
		if( CWindows::GetInstance()->IsFullScreen() )
		{
			RECT rect;
			HWND hDeskWnd = GetDesktopWindow(); //この関数でデスクトップのハンドルを取得
			GetWindowRect(hDeskWnd, &rect); //デスクトップのハンドルからその(画面の)大きさを取得

			GetWindowRect(CWindows::GetInstance()->m_hWindow, &CWindows::GetInstance()->m_WinRect);

			Sint32 w,h;
			w = rect.right - rect.left;
			h = rect.bottom - rect.top;

			SetMenu           ( CWindows::GetInstance()->m_hWindow , NULL );
			SetWindowLong     ( CWindows::GetInstance()->m_hWindow , GWL_STYLE, WS_POPUP|WS_VISIBLE|WS_EX_TOPMOST );
			MoveWindow        (	CWindows::GetInstance()->m_hWindow , 0,0, w , h , true	);

			{
				RECT rc;
				SetRect(&rc, 0, 0, w, h );
				CWindows::GetInstance()->m_WinRect;
				ClipCursor( &rc );
				UpdateWindow( CWindows::GetInstance()->m_hWindow );

				while( ShowCursor(FALSE)>0 );

			}

			return;
		}
		else
		{
			SetMenu( CWindows::GetInstance()->m_hWindow, LoadMenu(CWindows::GetInstance()->m_hInstance , MAKEINTRESOURCE( IDC_HELPWINDOW)) );

			//Windowの大きさを元に戻す
			SetWindowLong( CWindows::GetInstance()->m_hWindow , GWL_STYLE  , CWindows::GetInstance()->m_AppStyle );
			SetWindowPos ( CWindows::GetInstance()->m_hWindow,
			               HWND_NOTOPMOST,
			               CWindows::GetInstance()->m_WinRect.left   , CWindows::GetInstance()->m_WinRect.top,
						   CWindows::GetInstance()->m_WinRect.right  - CWindows::GetInstance()->m_WinRect.left,
						   CWindows::GetInstance()->m_WinRect.bottom - CWindows::GetInstance()->m_WinRect.top,  SWP_SHOWWINDOW );

			ChangeDisplaySettings( NULL , 0 );
			ClipCursor( NULL );
			UpdateWindow( CWindows::GetInstance()->m_hWindow );
			ShowCursor(TRUE);
		}
	}


	Sint32 gamew = m_GameW, gameh = m_GameH;
	Sint32 winw  = m_GameW, winh  = m_GameH;

	CGameGirl::GetInstance()->GetGameResolution(&gamew, &gameh);
	CGameGirl::GetInstance()->GetWindowsResolution(&winw, &winh);

	m_BackBufferSize.TopLeftX = 0;
	m_BackBufferSize.TopLeftY = 0;
	m_BackBufferSize.Width  = winw;
	m_BackBufferSize.Height = winh;

}


GLuint COpenGL3::compileShader( GLchar const* const* vtxShader , size_t sz_vtx , GLchar const* const* pxlShader ,size_t sz_pxl )
{
	GLint status;
	GLsizei bufSize;
	GLsizei length;
	GLchar *infoLog;

	GLuint  vshader = glCreateShader(GL_VERTEX_SHADER);;
	GLuint  pshader = glCreateShader( GL_FRAGMENT_SHADER );

	glShaderSource ( vshader, sz_vtx, vtxShader, NULL);
	glCompileShader( vshader );
	glGetShaderiv  ( vshader, GL_COMPILE_STATUS, &status );

	if (status == GL_FALSE)
	{
		glGetShaderiv( vshader, GL_INFO_LOG_LENGTH, &bufSize);
		infoLog = (GLchar*)malloc( bufSize );
		glGetShaderInfoLog( vshader, bufSize, &length, infoLog );
		gxLib::DebugLog("[vtx ShaderError] %s", infoLog );
		free(infoLog);
		return -1;
	}

	glShaderSource ( pshader, sz_pxl, pxlShader, NULL );
	glCompileShader( pshader );
	glGetShaderiv  ( pshader, GL_COMPILE_STATUS, &status );

	if (status == GL_FALSE)
	{
		glGetShaderiv( pshader, GL_INFO_LOG_LENGTH, &bufSize);
		infoLog = (GLchar*)malloc( bufSize );
		glGetShaderInfoLog( pshader, bufSize, &length, infoLog );
		gxLib::DebugLog("[pxl ShaderError] %s", infoLog );
		free(infoLog);
		return -1;
	}

	//attach prog program

	GLuint prog = glCreateProgram();
	glAttachShader( prog , vshader );
	glAttachShader( prog , pshader );

	//link shader program

	glLinkProgram ( prog );
	glGetProgramiv( prog, GL_LINK_STATUS, &status);

	if (status == GL_FALSE)
	{
		glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &bufSize );
		infoLog = (GLchar*)malloc( bufSize );
		glGetProgramInfoLog( prog, bufSize, &length, infoLog );
		gxLib::DebugLog("[Shader LinkError] %s", infoLog );
		free(infoLog);
		return -1;
	}

	glDeleteShader( vshader );
	glDeleteShader( pshader );

	return prog;
}


void COpenGL3::initShader()
{
	//----------------------------------------------------------------------
	// init shader
	//----------------------------------------------------------------------

	m_ShaderProg[ enShaderDefault ] = compileShader(
		(GLchar const* const*)vtxShader1,	sizeof(vtxShader1) / sizeof(vtxShader1[0]),
		(GLchar const* const*)pxlShader1,	sizeof(pxlShader1) / sizeof(pxlShader1[0]) );

	m_ShaderProg[ enShaderBloom  ] = compileShader(
		(GLchar const* const*)vtxShader1,	sizeof(vtxShader1) / sizeof(vtxShader1[0]),
		(GLchar const* const*)pxlShader2,	sizeof(pxlShader2) / sizeof(pxlShader2[0]) );

	m_ShaderProg[ enShaderBlur   ] = compileShader(
		(GLchar const* const*)vtxShader1,	sizeof(vtxShader1) / sizeof(vtxShader1[0]),
		(GLchar const* const*)pxlShader3,	sizeof(pxlShader3) / sizeof(pxlShader3[0]) );

	m_ShaderProg[ enShaderRaster ] = compileShader(
		(GLchar const* const*)vtxShader1,	sizeof(vtxShader1) / sizeof(vtxShader1[0]),
		(GLchar const* const*)pxlShader4,	sizeof(pxlShader4) / sizeof(pxlShader4[0]) );

	m_ShaderProg[enShaderNormal] = compileShader(
		(GLchar const* const*)vtxShader1, sizeof(vtxShader1) / sizeof(vtxShader1[0]),
		(GLchar const* const*)pxlShader5, sizeof(pxlShader5) / sizeof(pxlShader5[0]));

    m_ShaderProg[enShaderFont] = compileShader(
         (GLchar const* const*)vtxShader1, sizeof(vtxShader1) / sizeof(vtxShader1[0]),
         (GLchar const* const*)pxlShader6, sizeof(pxlShader6) / sizeof(pxlShader6[0]));

	m_ShaderProg[enShaderDev] = compileShader(
		(GLchar const* const*)vtxShader1, sizeof(vtxShader1) / sizeof(vtxShader1[0]),
		(GLchar const* const*)pxlShader0, sizeof(pxlShader0) / sizeof(pxlShader0[0]));
}


void COpenGL3::initTexture()
{
	//----------------------------------------------------------------------
	//テクスチャ生成
	//----------------------------------------------------------------------

	Sint32 w, h;

	//通常テクスチャを生成

	w = gxTexManager::enMasterWidth;
	h = gxTexManager::enMasterHeight;

	//for (Sint32 ii = 0; ii < enTexturePageMax; ii++)
	//{
	//	//必要なときにだけ作ることにする
	//	makeTexture(&m_TextureData[ii] , w , h ,32 );
	//}

	//ノンテクスチャポリゴン用テクスチャという矛盾したテクスチャ
	Uint8 *pBasicTex = new Uint8[32*32*4];

	gxUtil::MemSet( pBasicTex , 0xFF , 32*32*4 );

	makeTexture( &m_OffScreenTexture[ enBasicBuff ] , 32 , 32 ,32 , pBasicTex );

	SAFE_DELETES( pBasicTex );

	//壁紙用テクスチャを生成

	makeTexture(&m_OffScreenTexture[ enWallPaper ], 256, 256);

	//フレームバッファ用テクスチャを生成

	w = m_GameW;
	h = m_GameH;

	if (m_bUpConvert)
	{
		w = 2048;
		h = 2048;
	}

	makeTexture(&m_OffScreenTexture[enGameScreen0], w, h,24);
	makeTexture(&m_OffScreenTexture[enGameScreen1], w, h,24);
	makeTexture(&m_OffScreenTexture[enGameScreen2], w, h,24);
	makeTexture(&m_OffScreenTexture[enCaptureScreen], w, h,24);
	makeTexture(&m_OffScreenTexture[enGamePostProcess0], w, h,24);
	makeTexture(&m_OffScreenTexture[enGamePostProcess1], w, h,24);
	makeTexture(&m_OffScreenTexture[enGamePostProcess2], w, h,24);

	createFrameBuffer(&m_OffScreenTexture[enGameScreen0]);
	createFrameBuffer(&m_OffScreenTexture[enGameScreen1]);
	createFrameBuffer(&m_OffScreenTexture[enGameScreen2]);
	createFrameBuffer(&m_OffScreenTexture[enCaptureScreen]);
	createFrameBuffer(&m_OffScreenTexture[enGamePostProcess0]);
	createFrameBuffer(&m_OffScreenTexture[enGamePostProcess1]);
	createFrameBuffer(&m_OffScreenTexture[enGamePostProcess2]);

	//restore frame buffer state
	glBindFramebuffer( GL_FRAMEBUFFER, m_FrameBufferIndex );
	glBindTexture( GL_TEXTURE_2D, 0 );

}

void COpenGL3::initVBO()
{
	//頂点バッファを作成

	SAFE_DELETES( m_pVertexBuffer );
	SAFE_DELETES( m_pIndexBuffer );

	m_pVertexBuffer = new Uint8[ enVertexBufferSize ];
	m_pIndexBuffer  = new Uint8[ enIndexBufferSize ];

	glGenBuffers(1, &m_vertexBufferObject );
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, enVertexBufferSize, m_pVertexBuffer, GL_STREAM_DRAW );

	glGenBuffers(1, &m_indexBufferObject );
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, enIndexBufferSize, m_pIndexBuffer, GL_STREAM_DRAW );

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}



void COpenGL3::createFrameBuffer( TextureData *pTextureData )
{
	// フレームバッファオブジェクトを作成する

	if( pTextureData->frameBufferObject != -1 )
	{
		//既に作成されていたら削除する
		glDeleteFramebuffers( 1 , &pTextureData->frameBufferObject );
	}

	glGenFramebuffers(1, &pTextureData->frameBufferObject );
	glBindFramebuffer( GL_FRAMEBUFFER, pTextureData->frameBufferObject );

	// フレームバッファオブジェクトにカラーバッファとしてテクスチャを結合する
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pTextureData->texture2D, 0 );

	// フレームバッファオブジェクトを解除する
	glBindFramebuffer( GL_FRAMEBUFFER, m_FrameBufferIndex );
	glBindTexture( GL_TEXTURE_2D, 0 );
}

void COpenGL3::Update()
{
	makeWallPaper();

	m_b3DView = CGameGirl::GetInstance()->Is3DView();
}


void COpenGL3::Render()
{
    Sint32 gamew = m_GameW,gameh = m_GameH;
    Sint32 winw  = m_GameW,winh  = m_GameH;
    
    glDisable ( GL_SCISSOR_TEST );
    m_bScissorEnable = gxFalse;
    
    CGameGirl::GetInstance()->GetGameResolution(&gamew, &gameh);
    CGameGirl::GetInstance()->GetWindowsResolution(&winw, &winh);

    if(m_FrameBufferIndex == -1)
    {
        //glGetIntegerv(GL_FRAMEBUFFER_BINDING, &m_FrameBufferIndex);
		m_FrameBufferIndex = 0;
    }

	//背景色をクリア
	StViewPort viewport;

	m_ShadingWindowW = m_GameW;
	m_ShadingWindowH = m_GameH;

	Uint32 _argb = gxRender::GetInstance()->GetBgColor();
	Float32 rgba[] = {
		((_argb >> 16) & 0xff) / 255.0f,
		((_argb >> 8) & 0xff)  / 255.0f,
		((_argb >> 0) & 0xff)  / 255.0f,
		((_argb >> 24) & 0xff) / 255.0f,
	};


	//レンダーターゲットをゲーム画面用テクスチャにする
	glBindFramebuffer( GL_FRAMEBUFFER, m_OffScreenTexture[ enGamePostProcess0 + m_CurrentPage ].frameBufferObject );

	if( rgba[0] + rgba[1] + rgba[2] )
	{
		glClearColor( rgba[0], rgba[1], rgba[2], rgba[3] );
		glClear(GL_COLOR_BUFFER_BIT);
	}

	Float32 fLR = 3.0f;

	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	viewport.Width    = m_ShadingWindowW;
	viewport.Height   = m_ShadingWindowH;

/*
	if( m_bUpConvert )
	{
		viewport.Width    = 2048;
		viewport.Height   = 2048;
	}
*/

	glViewport( 0, 0, viewport.Width , viewport.Height );
	glDisable ( GL_SCISSOR_TEST );
	m_bScissorEnable = gxFalse;

	render();

	//-----------------------------------
	//system
	//-----------------------------------

	glDisable ( GL_SCISSOR_TEST );
	m_bScissorEnable = gxFalse;

	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	viewport.Width    = winw;//m_BackBufferSize.Width;
	viewport.Height   = winh;//m_BackBufferSize.Height;

	glViewport( 0, 0, viewport.Width , viewport.Height );

	m_ShadingWindowW = winw;
	m_ShadingWindowH = winh;

	renderSystem();

	SwapBuffers( CWindows::GetInstance()->m_WinDC );


}

void COpenGL3::render()
{
	if( !m_bInitCompleted ) return;

	gxDebug::GetInstance()->ResetDrawCallCnt();

	m_ShadingWindowW = m_GameW;
	m_ShadingWindowH = m_GameH;

	//GLの最終座標
	float glX1, glY1;
	float glX2, glY2;

	Sint32 gamew = m_GameW,gameh = m_GameH;
	Sint32 winw  = m_GameW,winh  = m_GameH;

	CGameGirl::GetInstance()->GetGameResolution   ( &gamew , &gameh );
	CGameGirl::GetInstance()->GetWindowsResolution( &winw  , &winh   );

	glX1 = 0.0f;
	glY1 = 0.0f;

	glX2 = 2.0f * gamew / winw;
	glY2 = 2.0f * gameh / winh;

	glX1 = -glX2 /2.0f;
	glY1 =  glY2 /2.0f;
	glX2 =  glX2 /2.0f;
	glY2 = -glY2 /2.0f;

	{
		Sint32 sCommandMax      = gxRender::GetInstance()->GetCommandNum();
		StCustomVertex *pVertex = gxRender::GetInstance()->GetVertex(0);

		Sint32 indexUnitSize = 4;

		Uint32 vtx_max = gxRender::GetInstance()->GetVertexNum();
		Uint32 idx_max = gxRender::GetInstance()->GetIndexNum();

		size_t vram_draw_max = enVertexBufferSize / sizeof(VertexPositionColorTexCoord);
		if (vtx_max >= vram_draw_max - 32)
		{
			vtx_max = vram_draw_max - 32;
			gxDebug::GetInstance()->SetError(gxDebug::ErrVertexBufferOver);
		}

		VertexPositionColorTexCoord* pVtxTex = (VertexPositionColorTexCoord*)pVertex;

		//頂点バッファ、インデックスバッファは小分けにしてMap,UnMapしてはいけない
		//やるなら位置をずらすこと

		Float32 w = 1.0f;
		Float32 offsetx = 0.0f;

		if( m_b3DView )
		{
			offsetx = 0.5f;
		}

		const VertexPositionColorTexCoord cubeVertices[] = 
		{
			//背景を描く:0
			{ StXMFLOAT4( -1.f*w,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 0.f , 1.0f-0.f ) ,StXMFLOAT2(0.f , 1.0f - 0.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() , },
			{ StXMFLOAT4(  1.f*w, -1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 1.f , 1.0f-1.f ) ,StXMFLOAT2(1.f , 1.0f - 1.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -1.f*w, -1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 0.f , 1.0f-1.f ) ,StXMFLOAT2(0.f , 1.0f - 1.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4(  1.f*w,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 1.f , 1.0f-0.f ) ,StXMFLOAT2(1.f , 1.0f - 0.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },

			//レンダーテクスチャを描く
			//FLIP1用:6
			{ StXMFLOAT4( glX1 - offsetx,  glY1 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(0.f , 1.0f-0.f), StXMFLOAT2(0.f , 1.0f - 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( glX2 - offsetx,  glY2 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(1.f , 1.0f-1.f), StXMFLOAT2(1.f , 1.0f - 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( glX1 - offsetx,  glY2 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(0.f , 1.0f-1.f), StXMFLOAT2(0.f , 1.0f - 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( glX2 - offsetx,  glY1 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(1.f , 1.0f-0.f), StXMFLOAT2(1.f , 1.0f - 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },

			//レンダーテクスチャを描く
			//FLIP2用:12
			{ StXMFLOAT4( glX1 + offsetx,  glY1 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(0.f , 1.0f-0.f), StXMFLOAT2(0.f , 1.0f - 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( glX2 + offsetx , glY2 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(1.f , 1.0f-1.f), StXMFLOAT2(1.f , 1.0f - 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( glX1 + offsetx,  glY2 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(0.f , 1.0f-1.f), StXMFLOAT2(0.f , 1.0f - 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( glX2 + offsetx,  glY1 ,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 1.0f) , StXMFLOAT2(1.f , 1.0f-0.f), StXMFLOAT2(1.f , 1.0f - 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },

			//ブラー用 1.0 -> 0.25 :18
			{ StXMFLOAT4( -1.f*w,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 0.f , 1.0f-0.f ), StXMFLOAT2(0.f , 1.0f - 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -0.5f*w, 0.5f*w, 0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 1.f , 1.0f-1.f ), StXMFLOAT2(1.f , 1.0f - 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -1.f*w,  0.5f*w, 0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 0.f , 1.0f-1.f ), StXMFLOAT2(0.f , 1.0f - 1.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -0.5f*w, 1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ) , StXMFLOAT2( 1.f , 1.0f-0.f ), StXMFLOAT2(1.f , 1.0f - 0.f) ,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },

			//ブラー用書き戻し 0.25 -> 1.0 : 24
			{ StXMFLOAT4( -1.f*w,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.f   , 1.0f  )	, StXMFLOAT2( 0.f   , 1.0f  )	,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4(  1.f*w, -1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.25f , 0.75f )	, StXMFLOAT2( 0.25f , 0.75f )	,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -1.f*w, -1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.f   , 0.75f )	, StXMFLOAT2( 0.f   , 0.75f )	,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4(  1.f*w,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.25f , 1.0f  )	, StXMFLOAT2( 0.25f , 1.0f  )	,StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },

			//ブラー中	0.25 -> 0.25 : 30
			{ StXMFLOAT4( -1.f*w ,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.f   , 1.0f)  , StXMFLOAT2( 0.f   , 1.0f)  ,	StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -0.5f*w,  0.5f*w, 0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.25f , 0.75f) , StXMFLOAT2( 0.25f , 0.75f) ,	StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -1.f*w,   0.5f*w, 0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.f   , 0.75f) , StXMFLOAT2( 0.f   , 0.75f) ,	StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4( -0.5f*w,  1.f*w,  0.f,1), StXMFLOAT4( 1.0f, 1.0f, 1.0f , 1.0f ), StXMFLOAT2( 0.25f , 1.0f)  , StXMFLOAT2( 0.25f , 1.0f)  ,	StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },

			//背景を描く:0
			{ StXMFLOAT4(-1.f * w,  1.f * w,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 0.9f), StXMFLOAT2(0.f , 1.0f - 0.f) , StXMFLOAT2(0.f , 1.0f - 0.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4(1.f * w, -1.f * w,  0.f,1) , StXMFLOAT4(1.0f, 1.0f, 1.0f , 0.9f), StXMFLOAT2(1.f , 1.0f - 1.f) , StXMFLOAT2(1.f , 1.0f - 1.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4(-1.f * w, -1.f * w,  0.f,1), StXMFLOAT4(1.0f, 1.0f, 1.0f , 0.9f), StXMFLOAT2(0.f , 1.0f - 1.f) , StXMFLOAT2(0.f , 1.0f - 1.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },
			{ StXMFLOAT4(1.f * w,  1.f * w,  0.f,1) , StXMFLOAT4(1.0f, 1.0f, 1.0f , 0.9f), StXMFLOAT2(1.f , 1.0f - 0.f) , StXMFLOAT2(1.f , 1.0f - 0.f),StXMFLOAT2(1.0f,1.0f)  ,StXMFLOAT2(0.0f,0.0f) ,0.f ,StXMFLOAT2(1.0f,1.0f) , StXMFLOAT4(0.0f, 0.0f, 0.0f , 0.0f) , StPointLightInfo() },

		};

		gxUtil::MemCpy( &pVtxTex[vtx_max] , (void*)cubeVertices , sizeof(cubeVertices) );

		Uint32 cubeIndices[] =
		{
			0+vtx_max, 1+vtx_max, 2+vtx_max,
			0+vtx_max, 3+vtx_max, 1+vtx_max,

			0+vtx_max+4, 1+vtx_max+4, 2+vtx_max+4,
			0+vtx_max+4, 3+vtx_max+4, 1+vtx_max+4,

			0+vtx_max+8, 1+vtx_max+8, 2+vtx_max+8,
			0+vtx_max+8, 3+vtx_max+8, 1+vtx_max+8,

			0+vtx_max+12, 1+vtx_max+12, 2+vtx_max+12,
			0+vtx_max+12, 3+vtx_max+12, 1+vtx_max+12,

			0+vtx_max+16, 1+vtx_max+16, 2+vtx_max+16,
			0+vtx_max+16, 3+vtx_max+16, 1+vtx_max+16,

			0+vtx_max+24, 1+vtx_max+24, 2+vtx_max+24,
			0+vtx_max+24, 3+vtx_max+24, 1+vtx_max+24,

			0 + vtx_max + 30, 1 + vtx_max + 30, 2 + vtx_max + 30,
			0 + vtx_max + 30, 3 + vtx_max + 30, 1 + vtx_max + 30,
		};

		gxUtil::MemCpy( &m_pIndexBuffer[0]        , gxRender::GetInstance()->GetIndexBuffer( 0 ) , idx_max*4 );
		gxUtil::MemCpy( &m_pIndexBuffer[idx_max*4], cubeIndices, sizeof(cubeIndices) );
	}

	m_CurrentPage = 0;

	if( !m_b3DView )
	{
/*
		if( rgba[0] + rgba[1] + rgba[2] )
		{
			//レンダーターゲットをゲーム画面用テクスチャにする
			glBindFramebuffer( GL_FRAMEBUFFER, m_OffScreenTexture[ enGamePostProcess0 + m_CurrentPage ].frameBufferObject );
			glClearColor( rgba[0], rgba[1], rgba[2], rgba[3] );
			glClear(GL_COLOR_BUFFER_BIT);
		}
*/

		m_ConstBuffer3dView.pos[0] = 0.0f;

		Sint32 start = 0, max = 0;

	//---------------
	max   = gxRender::GetInstance()->GetGameOrderMax();

	Uint32 vtx_max = gxRender::GetInstance()->GetVertexNum();
	Uint32 idx_max = gxRender::GetInstance()->GetIndexNum();

	size_t vram_draw_max = enVertexBufferSize / sizeof(VertexPositionColorTexCoord);

	if (vtx_max >= vram_draw_max - 32)
	{
		vtx_max = vram_draw_max - 32;
	}

	StCustomVertex* pVertex = gxRender::GetInstance()->GetVertex(0);
	size_t size = (vtx_max+32) * sizeof(VertexPositionColorTexCoord);
	glBindBuffer( GL_ARRAY_BUFFER, m_vertexBufferObject);
	glBufferSubData(GL_ARRAY_BUFFER , 0, size , pVertex );

	//---------------

		renderGameObject( start , max );

		//ToDo:Game1へコピー
		//glBindFramebuffer( GL_FRAMEBUFFER, m_TextureData[ enGameScreen1 ].frameBufferObject);
	}
	else
	{

	}

}


void COpenGL3::renderGameObject( Sint32 start , Sint32 max )
{
	//テクスチャにゲームオブジェクトを描く

	CCommandList* pCommand = NULL;
	Sint32 cmdMax = gxRender::GetInstance()->GetCommandNum();

	Uint32 idx_max = gxRender::GetInstance()->GetIndexNum();

	//---------------
	//max   = gxRender::GetInstance()->GetGameOrderMax();

/*
	Uint32 vtx_max = gxRender::GetInstance()->GetVertexNum();
	size_t vram_draw_max = enVertexBufferSize / sizeof(VertexPositionColorTexCoord);

	if (vtx_max >= vram_draw_max - 32)
	{
		vtx_max = vram_draw_max - 32;
	}

	StCustomVertex* pVertex = gxRender::GetInstance()->GetVertex(0);
	size_t size = (vtx_max+32) * sizeof(VertexPositionColorTexCoord);
	glBindBuffer( GL_ARRAY_BUFFER, m_vertexBufferObject);
	glBufferSubData(GL_ARRAY_BUFFER , 0, size , pVertex );
*/
	//---------------

	//start = 0;
	//max   = gxRender::GetInstance()->GetGameOrderMax();
	cmdMax = max;

	glActiveTexture( GL_TEXTURE0 + 0 );
	glBindTexture  ( GL_TEXTURE_2D, 0 );

	//通常のしぇーだーに戻す
	changeShader( enShaderDefault );

	//ゲーム画面の描画

	glBlendEquation( GL_FUNC_ADD );
	glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);

	for(Sint32 n=start; n<max; n++)
	{
		pCommand = gxRender::GetInstance()->GetCommandList( n );
		Sint32 v_start = pCommand->arg[0];
		Sint32 v_num   = pCommand->arg[1];
		Sint32 i_start = pCommand->arg[2];
		Sint32 i_num   = pCommand->arg[3];

		switch( pCommand->eCommand ){
		case eCmdBindNoneTexture:
			//テクスチャをはずす
			glActiveTexture( GL_TEXTURE0 + enAlbedoTextureMapSlot );
			glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enBasicBuff ].texture2D );
			break;

		case eCmdBindAlbedoTexture:
			{
				//テクスチャをつける
				glActiveTexture( GL_TEXTURE0 + enAlbedoTextureMapSlot );

				Sint32 page = pCommand->arg[0];

				if( page == enBackBuffer )
				{
					glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enGameScreen1 ].texture2D );
				}
				else if( page == enCapturePage )
				{
					glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enCaptureScreen ].texture2D );
				}
				else
				{
					glBindTexture( GL_TEXTURE_2D, m_TextureData[ page ].texture2D );
				}

			}
			break;

		case eCmdBindNormalTexture:
			{
				//法線マップ用テクスチャをつける
				glActiveTexture( GL_TEXTURE0 + enNormalTextureMapSlot );

				Sint32 page = pCommand->arg[0];

				if( page == enBackBuffer )
				{
					glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enGameScreen1 ].texture2D );
				}
				else if( page == enCapturePage )
				{
					glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enCaptureScreen ].texture2D );
				}
				else
				{
					glBindTexture( GL_TEXTURE_2D, m_TextureData[ page ].texture2D );
				}

			}
			break;

		case eCmdBindPalletTexture:
			{
				//パレットマップ用テクスチャをつける
				glActiveTexture( GL_TEXTURE0 + 2 );

				Sint32 page = pCommand->arg[0];

				if( page == enBackBuffer )
				{
					glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enGameScreen1 ].texture2D );
				}
				else if( page == enCapturePage )
				{
					glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enCaptureScreen ].texture2D );
				}
				else
				{
					glBindTexture( GL_TEXTURE_2D, m_TextureData[ page ].texture2D );
				}
			}
			break;

		case eCmdBindCaptureTexture:
			glActiveTexture(GL_TEXTURE0 + enCaptureTextureMapSlot);
			glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[enCaptureScreen].texture2D);
			break;

		case eCmdChgAttributeAlphaNml:
			//ブレンディング(標準)
			glBlendEquation( GL_FUNC_ADD );
			glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);
			break;

		case eCmdChgAttributeAlphaAdd:
			//ブレンディング(加算)
			glBlendEquation( GL_FUNC_ADD );
			glBlendFunc( GL_SRC_ALPHA , GL_ONE);
			break;

		case eCmdChgAttributeAlphaSub:
			//ブレンディング(減算)
//			glBlendFunc( GL_ZERO , GL_ONE_MINUS_SRC_ALPHA);
			glBlendEquation( GL_FUNC_REVERSE_SUBTRACT  );
			glBlendFunc( GL_SRC_ALPHA , GL_ONE);
			break;

		case eCmdChgAttributeAlphaCrs:
			//ブレンディング(乗算)
			glBlendFunc( GL_ZERO , GL_SRC_COLOR);
			break;

		case eCmdChgAttributeAlphaRvs:
			//ブレンディング(反転)
			glBlendFunc( GL_ONE_MINUS_DST_COLOR , GL_ZERO );
			break;

		case eCmdChgAttributeAlphaXor:
			//ブレンディング(XOR)
			glBlendFunc( GL_ONE_MINUS_DST_COLOR , GL_ONE_MINUS_SRC_COLOR );
			break;

		case eCmdChgAttributeAlphaScr:
			//ブレンディング(スクリーン乗算)
			glBlendFunc( GL_ONE_MINUS_DST_COLOR , GL_ONE );
			break;

		case eCmdRenderPoint:
			//点の描画
            glDrawElements( GL_POINTS , i_num , GL_UNSIGNED_INT  , &m_pIndexBuffer[i_start*4] );
			gxDebug::GetInstance()->AddDrawCallCnt();
			break;

		case eCmdRenderLineStrip:
			//連続線の描画
            glDrawElements( GL_LINES , i_num , GL_UNSIGNED_INT  , &m_pIndexBuffer[i_start*4] );
			gxDebug::GetInstance()->AddDrawCallCnt();
			break;

		case eCmdRenderLineNormal:
			//線の描画
            glDrawElements( GL_LINES , i_num , GL_UNSIGNED_INT  , &m_pIndexBuffer[i_start*4] );
			gxDebug::GetInstance()->AddDrawCallCnt();
			break;

		case eCmdRenderTriangle:
		case eCmdRenderSquare:
			//三角形の描画
            glDrawElements( GL_TRIANGLES , i_num , GL_UNSIGNED_INT  , &m_pIndexBuffer[i_start*4] );
			gxDebug::GetInstance()->AddDrawCallCnt();
			break;

		case eCmdRenderFont:
			//フォントの描画
			break;

		case eCmdScissor:
			//シザリング
			if( pCommand->arg[2] == 0 || pCommand->arg[3] == 0 )
			{
				glDisable ( GL_SCISSOR_TEST );
				m_bScissorEnable = gxFalse;
			}
			else
			{
				Sint32 x,y,w,h;
				w = pCommand->arg[2];
				h = pCommand->arg[3];
				x = pCommand->arg[0];
				y = m_GameH - pCommand->arg[1] - h;
				glEnable ( GL_SCISSOR_TEST );
				glScissor( x, y, w ,h );
				m_Scissor.Set(x,y,w,h);
				m_bScissorEnable = gxTrue;
			}
			break;
		case eCmdChangeShader:
			{
				switch( pCommand->arg[0] ){
				case gxShaderReset:
				case gxShaderDefault:
					changeShader(enShaderDefault );
					break;
				case gxShaderBloom:
					changeShader(enShaderBloom );
					break;
				case gxShaderBlur:
					changeShader(enShaderBlur );
					break;
				case gxShaderRaster:
					changeShader(enShaderRaster );
					break;
				case gxShaderNormal:
					changeShader(enShaderNormal );
					break;
				case gxShaderPallet:
					changeShader(enShaderNormal );
					break;
                case gxShaderFont:
                    changeShader(enShaderFont );
                    break;
				default:
					//changeShader(enShaderDev );
					changeShader(enShaderDefault );
					break;
				}
			}
			break;


		case eCmdChangeRenderTarget:
			break;

		case eCmdProcessingBloom:
			{
				Sint32 pageFrmBuf = 0;
				Sint32 pageTexBuf = 0;

				//enGameScreen2に現在の画面を退避する
				glActiveTexture( GL_TEXTURE0 + enAlbedoTextureMapSlot );
		changeShader(enShaderDefault );

				glBlendEquation( GL_FUNC_ADD );
				glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

				glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ enGameScreen0 ].frameBufferObject );
				glBindTexture(GL_TEXTURE_2D     , m_OffScreenTexture[ enGamePostProcess0+m_CurrentPage ].texture2D );
				glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[ (idx_max+0)*4 ] );
				gxDebug::GetInstance()->AddDrawCallCnt();

#if 1
				//ブルームシェーダーで輝度の高いものを抽出する
				changeShader( enShaderBloom );

		GLfloat option[enOptionNum] = { pCommand->arg[1]/100.0f , 0.0 };
		Sint32 optionID = glGetUniformLocation(m_ShaderProg[enShaderBlur], "u_option");
		glUniform1fv(optionID, 4, option);

				pageTexBuf = enGamePostProcess0 + m_CurrentPage;
				m_CurrentPage ++;
				m_CurrentPage = m_CurrentPage%3;
				pageFrmBuf = enGamePostProcess0 + m_CurrentPage;

				glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ pageFrmBuf ].frameBufferObject );
				glClearColor( 0.0f , 0.0f , 0.0f , 1.0f );
				glClear(GL_COLOR_BUFFER_BIT);

				glBindTexture(GL_TEXTURE_2D     , m_OffScreenTexture[ pageTexBuf ].texture2D );

				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

				//ブルーム対象の描画を行う
				glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[ (idx_max+0)*4 ] );
				gxDebug::GetInstance()->AddDrawCallCnt();
#endif

				//Bloom画像をぼかす
				Float32 fBlurPower[2] = { pCommand->arg[0]/10000.0f , 1.0f };

				changeShader( enShaderBlur , fBlurPower );

				for (Sint32 aa = 0; aa < 5; aa++ )//pCommand->arg[1]; aa++)
				{
					pageTexBuf = enGamePostProcess0 + m_CurrentPage;
					m_CurrentPage++;
					m_CurrentPage = m_CurrentPage % 3;

					pageFrmBuf = enGamePostProcess0 + m_CurrentPage;

					//まず横
					GLfloat option[enOptionNum] = { 0.0 , 0.0 };
					Sint32 optionID = glGetUniformLocation(m_ShaderProg[enShaderBlur], "u_option");
					glUniform1fv(optionID, enOptionNum, option);

					glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ pageFrmBuf ].frameBufferObject);
					glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT);
					glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[ pageTexBuf ].texture2D);

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

					glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &m_pIndexBuffer[(idx_max + 0) * 4]);
					gxDebug::GetInstance()->AddDrawCallCnt();

					//次に縦

					pageTexBuf = enGamePostProcess0 + m_CurrentPage;
					m_CurrentPage++;
					m_CurrentPage = m_CurrentPage % 3;
					pageFrmBuf = enGamePostProcess0 + m_CurrentPage;

					option[0] = 1.0;
					optionID = glGetUniformLocation(m_ShaderProg[enShaderBlur], "u_option");
					glUniform1fv(optionID, enOptionNum, option);

					glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ pageFrmBuf ].frameBufferObject);
					glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT);

					glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[ pageTexBuf ].texture2D);

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

					glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &m_pIndexBuffer[(idx_max + 0) * 4]);
					gxDebug::GetInstance()->AddDrawCallCnt();
				}

				//---------------

				//通常シェーダーに戻して退避していた画面を元に戻す
			changeShader();
				Sint32 bloomBuf = enGamePostProcess0 + m_CurrentPage;
				pageTexBuf = enGameScreen0;
				m_CurrentPage ++;
				m_CurrentPage = m_CurrentPage%3;
				pageFrmBuf = enGamePostProcess0 + m_CurrentPage;

				glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ pageFrmBuf ].frameBufferObject );
				glBindTexture(GL_TEXTURE_2D     , m_OffScreenTexture[ pageTexBuf ].texture2D );

				glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[(idx_max+0)*4] );
				gxDebug::GetInstance()->AddDrawCallCnt();

				//加算合成にする
				glBlendEquation(GL_FUNC_ADD);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE);

//				glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[(idx_max+30)*4] );

				pageTexBuf = bloomBuf;
				glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[ pageTexBuf ].texture2D);
				glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &m_pIndexBuffer[(idx_max + 30) * 4]);
				gxDebug::GetInstance()->AddDrawCallCnt();

				//シェーダーを元に戻す
			//changeShader();

				glBlendEquation( GL_FUNC_ADD );
				glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);
			}
			break;

		case eCmdProcessingBlur:
			{
				//ボケシェーダーに変更する
				glActiveTexture( GL_TEXTURE0 + enAlbedoTextureMapSlot );

				Float32 fBlurPower[2] = { pCommand->arg[0] / 10000.f , 0.0f };
				changeShader( enShaderBlur , fBlurPower );

				for (Sint32 aa = 0; aa < pCommand->arg[1]; aa++)
				{
					Sint32 pageTexBuf = enGamePostProcess0 + m_CurrentPage;
					m_CurrentPage++;
					m_CurrentPage = m_CurrentPage % 3;

					Sint32 pageFrmBuf = enGamePostProcess0 + m_CurrentPage;

					//画面を半分にして縮小する

			GLfloat options[enOptionNum] = { 0.0 , 1.0 };
			Sint32 optionsID = glGetUniformLocation( m_ShaderProg[ enShaderBlur ], "u_option");
			glUniform1fv( optionsID, 4, options );

					glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ pageFrmBuf ].frameBufferObject);
					glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT);
					glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[ pageTexBuf ].texture2D);

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

					glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &m_pIndexBuffer[(idx_max + 0) * 4]);
					gxDebug::GetInstance()->AddDrawCallCnt();


					//半分にしたバッファを元に戻す

					pageTexBuf = enGamePostProcess0 + m_CurrentPage;
					m_CurrentPage++;
					m_CurrentPage = m_CurrentPage % 3;
					pageFrmBuf = enGamePostProcess0 + m_CurrentPage;

		options[0] = 1.0f;
		optionsID = glGetUniformLocation( m_ShaderProg[ enShaderBlur ], "u_option");
		glUniform1fv( optionsID, 4, options );

		glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ pageFrmBuf ].frameBufferObject);
					glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT);

					glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[ pageTexBuf ].texture2D);

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

					glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &m_pIndexBuffer[(idx_max + 0) * 4]);
					gxDebug::GetInstance()->AddDrawCallCnt();

				}
				//シェーダーを元に戻す
			changeShader();
			}
			break;

		case eCmdCaptureScreen:
			{
				glActiveTexture( GL_TEXTURE0 + enAlbedoTextureMapSlot );
				changeShader( enShaderRaster );
				GLfloat option[enOptionNum] = { 10.0 , 0.1f , 0.f };
				option[0] = 10.0f * (pCommand->arg[0]/10000.0f);	//揺れの回数
				option[1] = pCommand->arg[1]/10000.0f;				//揺れの幅
				option[2] = pCommand->arg[2]/10000.0f;				//揺れ速度

				Sint32 optionID = glGetUniformLocation(m_ShaderProg[enShaderRaster], "u_option");
				glUniform1fv(optionID, 4, option );

				glDisable ( GL_SCISSOR_TEST );
				Sint32 pageTexBuf = enGamePostProcess0 + m_CurrentPage;
				Sint32 pageFrmBuf = enCaptureScreen;

				glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ pageFrmBuf ].frameBufferObject);
				glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
				glClear(GL_COLOR_BUFFER_BIT);
				glBindTexture(GL_TEXTURE_2D, m_OffScreenTexture[ pageTexBuf ].texture2D);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

				glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &m_pIndexBuffer[(idx_max + 0) * 4]);
				gxDebug::GetInstance()->AddDrawCallCnt();

				pageFrmBuf = enGamePostProcess0 + m_CurrentPage;
				glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ pageFrmBuf ].frameBufferObject);
				if( m_bScissorEnable )
				{
					glEnable ( GL_SCISSOR_TEST );
					glScissor( m_Scissor.x1, m_Scissor.y1, m_Scissor.x2-m_Scissor.x1 ,m_Scissor.y2 - m_Scissor.y1 );
				}

				changeShader( enShaderDefault );
			}
			break;

		case eCmdDevelop:
			{
				glActiveTexture( GL_TEXTURE0 + enAlbedoTextureMapSlot );

				changeShader(enShaderDev);
				glBlendEquation(GL_FUNC_ADD);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

				glActiveTexture(GL_TEXTURE0 + 1 );
				glBindTexture(GL_TEXTURE_2D, m_TextureData[1].texture2D);
				glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &m_pIndexBuffer[0 * 4]);
				gxDebug::GetInstance()->AddDrawCallCnt();

				glActiveTexture(GL_TEXTURE0);
				changeShader(enShaderDefault);
		}
		break;

		default:
			break;
		}
	}
}


void COpenGL3::renderSystem()
{
	//----------------------------------------------------------------------
	// 頂点シェーダーをアタッチします。
	//----------------------------------------------------------------------
	Uint32 idx_max = gxRender::GetInstance()->GetIndexNum();

	Float32 rgba[4] = { 0,0,0.5f,1 };	//rgba
	glBindFramebuffer( GL_FRAMEBUFFER, m_FrameBufferIndex );	//最終的な描画バッファに変更

	// バック バッファーと深度ステンシル ビューをクリアします。

	glClearColor( rgba[0], rgba[1], rgba[2], rgba[3] );
	glClear( GL_COLOR_BUFFER_BIT );

	// テクスチャが使用するサンプラー設定(Bilinear)
	switch( CWindows::GetInstance()->GetRenderingFilter() ){
	case CWindows::enSamplingNearest:
		//context->PSSetSamplers( 0, 1, &m_SamplerState[0] );
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		break;
	case CWindows::enSamplingBiLenear:
		//context->PSSetSamplers( 0, 1, &m_SamplerState[1] );
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		break;
	default:
		break;
	}

	glActiveTexture( GL_TEXTURE0 + enAlbedoTextureMapSlot );

	changeShader();

	glBlendEquation( GL_FUNC_ADD );
	glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);

	//--------------------------------------------------
	//壁紙画像をアタッチ
	//--------------------------------------------------
	glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enWallPaper ].texture2D );
	glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[idx_max*4] );

	//--------------------------------------------------
	//ゲーム画面をフリップ
	//--------------------------------------------------

	glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);
	glBlendEquation( GL_FUNC_ADD );
	glBlendFunc( GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);

	if( !m_b3DView )
	{
		//ピクセルシェーダーにリソース（テクスチャ）をアタッチする
		glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enGamePostProcess0+m_CurrentPage ].texture2D );

		//ゲーム画面を描画
		glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[(idx_max+6)*4] );
	}
	else
	{
//		//ピクセルシェーダーにリソース（テクスチャ）をアタッチする
//		glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enGameScreen1 ].texture2D );
//
//		//ゲーム画面を描画
//		glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[(idx_max+6)*4] );
//
//		//ピクセルシェーダーにリソース（テクスチャ）をアタッチする
//		glBindTexture( GL_TEXTURE_2D, m_OffScreenTexture[ enGameScreen2 ].texture2D );
//
//		//ゲーム画面を描画
//		glDrawElements( GL_TRIANGLES , 6 , GL_UNSIGNED_INT  , &m_pIndexBuffer[(idx_max+6+6)*4] );
	}

	//プライオリティ以上の描画は全画面で行う

	Sint32 start = gxRender::GetInstance()->GetGameOrderMax();
	Sint32 max   = gxRender::GetInstance()->GetCommandNum();
	renderGameObject( start , max );

}


void COpenGL3::Present() 
{
	//画面のフリップ

	if ( !m_bInitCompleted ) return;

	//	SwapBuffers( CWindows::GetInstance()->m_WinDC );

	gxChar* pFileName = CGameGirl::GetInstance()->GetScreenShotFileName();

	if (pFileName)
	{
		Uint32 length;
		GetFrameBufferImage(pFileName);
		CGameGirl::GetInstance()->SetScreenShot(gxFalse);
	}
}

void COpenGL3::ReadTexture( int texPage  )
{
	//外部からファイルを読み込む

	Uint32 col = 0x0000FF00;
	Uint32 w, h;
	
	w = gxTexManager::enMasterWidth;
	h = gxTexManager::enMasterHeight;

	if ( m_TextureData[texPage].texture2D == 0 )
	{
		//必要なときにだけ作ることにする
		makeTexture( &m_TextureData[texPage], w, h, 32);
	}

	//glActiveTexture(GL_TEXTURE0 );
	glBindTexture(GL_TEXTURE_2D, m_TextureData[texPage].texture2D );
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	//バイリニア
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	//ニアレストネイバー
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//----------------------------------------------------------------------

	Uint8 rgb[4];
	Uint8* pData = gxTexManager::GetInstance()->GetAtlasTexture(texPage)->GetTexelImage();
	size_t sz = 2048 * 2048 * 4;
	Uint8* pData2 = new Uint8[sz];

	Uint32 n = 0;
	for (Sint32 n = 0; n < sz; n += 4)
	{
		pData2[n + 3] = pData[n + 3];	//a
		pData2[n + 0] = pData[n + 2];	//r
		pData2[n + 1] = pData[n + 1];	//g
		pData2[n + 2] = pData[n + 0];	//b
	}

	GLenum err = glGetError();
	glTexImage2D(GL_TEXTURE_2D, 0, m_TextureData[texPage].format, w, h, 0, m_TextureData[texPage].format, GL_UNSIGNED_BYTE, pData2);// gxTexManager::GetInstance()->GetAtlasTexture(texPage)->GetTexelImage() );
	err = glGetError();

	SAFE_DELETES(pData2);
}


gxBool COpenGL3::makeTexture( TextureData *pTexture , int w , int h , int bitDepth , Uint8 *pData , Uint32 uSize )
{
	//----------------------------------------------------------------
	//書き込み可能なテクスチャの設定
	//----------------------------------------------------------------
	if (pTexture->texture2D != 0)
	{
		glDeleteTextures( 1, &pTexture->texture2D);
	}

	glGenTextures( 1, &pTexture->texture2D );

	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture( GL_TEXTURE_2D, pTexture->texture2D );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1);

	//----------------------------------------------------------------------
	//バイリニアフィルタリング
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	//ニアレストネイバー
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//----------------------------------------------------------------------

	GLenum err = glGetError();
	Uint32 textureFormat = GL_RGBA;
	if( bitDepth < 32 )
	{
		textureFormat = GL_RGB;
	}

	if( pData )
	{
		glTexImage2D(GL_TEXTURE_2D, 0, textureFormat, w, h, 0, textureFormat, GL_UNSIGNED_BYTE, pData );
		err = glGetError();
	}
	else
	{
		Uint8 *pData2 = new Uint8[w*h*(bitDepth/8)];
		gxUtil::MemSet( pData2 , 0x00 , w*h*(bitDepth/8) );
		
		if( bitDepth == 32 )
		{
			glTexImage2D(GL_TEXTURE_2D, 0, textureFormat, w, h, 0, textureFormat, GL_UNSIGNED_BYTE, pData2 );
		}
		else
		{
			glTexImage2D(GL_TEXTURE_2D, 0, textureFormat, w, h, 0, textureFormat, GL_UNSIGNED_BYTE, pData2 );
		}
		err = glGetError();
		SAFE_DELETES( pData2 );
	}

	pTexture->format = textureFormat;

	return gxTrue;
}


void COpenGL3::makeWallPaper()
{
	//ノイズテクスチャの作成

	Sint32 wh = 256;
	static Uint32 *p1 = NULL;
	Uint32 cnt = 0;

	if( p1 == NULL )
	{
		p1 = (Uint32*)malloc( wh*wh*4 );
	}

	for( int ii=0;ii<wh; ii++)
	{
		for (int jj = 0; jj < wh; jj++)
		{
			p1[cnt] = 0xFF000000|((gxLib::Rand()%256)<<16)|((gxLib::Rand()%256)<<8)|((gxLib::Rand()%256)<<0);
			cnt ++;
		}
	}

	makeTexture( &m_OffScreenTexture[enWallPaper] , wh , wh , 32 , (Uint8*)p1 , wh*wh*4 );

	//free( p1 );
}


void COpenGL3::configTextureSampling()
{
	//------------------------------------------------------
	//テクスチャのサンプリング方法の設定
	//------------------------------------------------------


}


void COpenGL3::configBlendState()
{
	//------------------------------------------------------
	// ブレンドステート（透過テクスチャ用）作成
	//------------------------------------------------------

}


struct ShaderAttributes
{
	gxChar* name;
	Uint32  num;
	Uint32  size;
	GLuint set( GLuint shader , GLuint offset )
	{
		GLuint id = glGetAttribLocation( shader, name );
		glEnableVertexAttribArray( id );
		glVertexAttribPointer( id, num, GL_FLOAT, GL_FALSE, sizeof(VertexPositionColorTexCoord), (GLvoid *)offset );
		offset += num * size;
		return offset;
	}
};

ShaderAttributes ShaderDefault[]={
	{ "a_position"	, 4,sizeof(Float32)},
	{ "a_color"		, 4,sizeof(Float32)},
	{ "a_texCoord"	, 2,sizeof(Float32)},
	{ "a_nmlCoord"	, 2,sizeof(Float32)},
	{ "a_scale"		, 2,sizeof(Float32)},
	{ "a_offset"	, 2,sizeof(Float32)},
	{ "a_rotation"	, 1,sizeof(Float32)},
	{ "a_flip"		, 2,sizeof(Float32)},
	{ "a_blend"		, 4,sizeof(Float32)},
	{ "a_pointlight_pos"   , 3,sizeof(Float32)},
	{ "a_pointlight_rgb"   , 3,sizeof(Float32)},
	{ "a_pointlight_length", 1,sizeof(Float32)},
    { "a_options"   , 4 ,sizeof(Float32)},
};

void COpenGL3::changeShader(ETypeShader sheaderIndex , Float32 *pFloatValue )
{
	//------------------------------------------------------
	// シェーダーを変更する
	//------------------------------------------------------

	glEnableVertexAttribArray( 0 );

	if(sheaderIndex == enShaderDefault )
	{
		//ノーマルシェーダー

		GLuint shader = m_ShaderProg[ enShaderDefault ];
		glUseProgram( shader );

		GLuint offset = 0;
		for( Sint32 ii=0; ii<ARRAY_LENGTH(ShaderDefault); ii++ )
		{
			ShaderAttributes *p = &ShaderDefault[ii];
			offset = p->set( shader , offset );
		}

		GLfloat screenSize[2] = { m_ShadingWindowW*1.0f , m_ShadingWindowH*1.0f};
		Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
		glUniform2fv(screenSizeID, 1, screenSize );

		int textureID1;
		textureID1 = glGetUniformLocation(shader, "u_textureID1" );
		glUniform1i( textureID1, 0 );

	}
	else if(sheaderIndex == enShaderBloom )
	{
		//Bloomシェーダー
		GLuint shader = m_ShaderProg[ enShaderBloom ];
		glUseProgram( shader );

		GLuint offset = 0;
		for( Sint32 ii=0; ii<ARRAY_LENGTH(ShaderDefault); ii++ )
		{
			ShaderAttributes *p = &ShaderDefault[ii];
			offset = p->set( shader , offset );
		}

		GLfloat screenSize[2] = { m_ShadingWindowW*1.0f , m_ShadingWindowH*1.0f};
		Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
		glUniform2fv(screenSizeID, 1, screenSize );

		GLfloat option[enOptionNum] = { 1.0 , 0.0 };
		Sint32 optionID = glGetUniformLocation(shader, "u_option");
		glUniform1fv(optionID, enOptionNum, option);

		int textureID1;
		textureID1 = glGetUniformLocation(shader, "u_textureID1" );
		glUniform1i(textureID1, 0 );
	}
	else if(sheaderIndex == enShaderBlur )
	{
		//Blurシェーダー

		GLuint shader = m_ShaderProg[ enShaderBlur ];
		glUseProgram( shader );

		GLuint offset = 0;
		for( Sint32 ii=0; ii<ARRAY_LENGTH(ShaderDefault); ii++ )
		{
			ShaderAttributes *p = &ShaderDefault[ii];
			offset = p->set( shader , offset );
		}

		GLfloat screenSize[2] = { m_ShadingWindowW*1.0f , m_ShadingWindowH*1.0f};
		Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
		glUniform2fv(screenSizeID, 1, screenSize );

		GLfloat options[enOptionNum] = { 1.0f , 1.0f };
		Sint32 optionsID = glGetUniformLocation(shader, "u_option");
		glUniform1fv( optionsID, enOptionNum, options );


		GLfloat weights[10] = {};

		if( pFloatValue[1] == 0.0f )
		{
			//通常のガウスブラー
			Float32 eRange_value = pFloatValue[0]*100;
			eRange_value = CLAMP(eRange_value,10.0f, 100.0f);

			Float32 t = 0.0f;
			Float32 d = eRange_value * eRange_value / 10.0f;
			for( Sint32 i = 0; i < 10; i++){
			    Float32 r = 1.0 + 2.0 * i;
			    Float32 w = exp(-0.5 * (r * r) / d);
			    weights[i] = w;
			    if(i > 0){w *= 2.0;}
			    t += w;
			}

			for(Sint32 i = 0; i < 10; i++){
			    weights[i] /= t;
			}
		}
		else
		{
			//Bloom用
			Float32 weight_temp[10]={1.0 , 0.9 , 0.8, 0.7, 0.6 , 0.5 , 0.4 , 0.3 , 0.2 , 0.1 };
			for (Sint32 i = 0; i < 10; i++) {
				weights[i] = (weight_temp[i]/9.0f)* pFloatValue[0];
			}
		}

		Sint32 weightID = glGetUniformLocation(shader, "u_weight");
		glUniform1fv( weightID, 10, weights );

		int textureID1;
		textureID1 = glGetUniformLocation(shader, "u_textureID");
		glUniform1i(textureID1, 0);

	}
	else if(sheaderIndex == enShaderRaster )
	{
		//Rasterシェーダー

		GLuint shader = m_ShaderProg[ enShaderRaster ];
		glUseProgram( shader );

		GLuint offset = 0;
		for( Sint32 ii=0; ii<ARRAY_LENGTH(ShaderDefault); ii++ )
		{
			ShaderAttributes *p = &ShaderDefault[ii];
			offset = p->set( shader , offset );
		}

		GLfloat screenSize[2] = { m_ShadingWindowW*1.0f , m_ShadingWindowH*1.0f};
		Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
		glUniform2fv(screenSizeID, 1, screenSize );

		GLfloat option[enOptionNum] = { 1.0 , 0.0 };
		Sint32 optionID = glGetUniformLocation(shader, "u_option");
		glUniform1fv(optionID, enOptionNum, option);

		int textureID1;
		textureID1 = glGetUniformLocation(shader, "u_textureID1" );
		glUniform1i(textureID1, 0 );

	}
	else if (sheaderIndex == enShaderNormal )
	{
		//法線マップシェーダー
		GLuint shader = m_ShaderProg[enShaderNormal];
		glUseProgram(shader);

		GLuint offset = 0;
		for (Sint32 ii = 0; ii < ARRAY_LENGTH(ShaderDefault); ii++)
		{
			ShaderAttributes* p = &ShaderDefault[ii];
			offset = p->set(shader, offset);
		}

		GLfloat screenSize[2] = { m_ShadingWindowW * 1.0f , m_ShadingWindowH * 1.0f };
		Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
		glUniform2fv(screenSizeID, 1, screenSize);

		int textureID1;
		textureID1 = glGetUniformLocation(shader, "u_textureID1");
		glUniform1i(textureID1, enAlbedoTextureMapSlot);

		int textureID2;
		textureID2 = glGetUniformLocation(shader, "u_textureID2");
		glUniform1i(textureID2, enNormalTextureMapSlot);

		int textureID3;
		textureID3 = glGetUniformLocation(shader, "u_textureID3");
		glUniform1i(textureID3, enCaptureTextureMapSlot);

		//int textureID2;
		//textureID2 = glGetUniformLocation(shader, "u_texNormal");
		//glUniform1i(textureID2, 1);

		//法線マップテクスチャスロット

		//ライト位置
//		Float32 fRot = (gxLib::GetGameCounter()*2)%360;
//		Float32 mx = 2*gxUtil::Touch()->Position()->x/m_GameW - 1.0f;
//		Float32 my = 2 * gxUtil::Touch()->Position()->y/m_GameH - 1.0f;
//		my *= -1.0f;
//		Float32 pointlight_param[10] = { mx , my , 1.0 , 1.0, 1.0 ,    /*argb*/1.0, 1.0 , 1.0 , 1.0 };
//		int pointLightID1;
//		pointLightID1 = glGetUniformLocation(shader, "u_PLight");
//		glUniform1fv(pointLightID1, 10 , pointlight_param );

	}
    else if(sheaderIndex == enShaderFont )
    {
        //フォントシェーダー
        
        GLuint shader = m_ShaderProg[ enShaderFont ];
        glUseProgram( shader );
        
        GLuint offset = 0;
        for( Sint32 ii=0; ii<ARRAY_LENGTH(ShaderDefault); ii++ )
        {
            ShaderAttributes *p = &ShaderDefault[ii];
            offset = p->set( shader , offset );
        }
        
        GLfloat screenSize[2] = { m_ShadingWindowW*1.0f , m_ShadingWindowH*1.0f};
        Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
        glUniform2fv(screenSizeID, 1, screenSize );
        
        int textureID1;
        textureID1 = glGetUniformLocation(shader, "u_textureID1" );
        glUniform1i( textureID1, 0 );
        
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}
	else if (sheaderIndex == enShaderDev )
	{
		GLuint shader = m_ShaderProg[enShaderDev];
		glUseProgram(shader);

		GLuint offset = 0;
		for (Sint32 ii = 0; ii < ARRAY_LENGTH(ShaderDefault); ii++)
		{
			ShaderAttributes* p = &ShaderDefault[ii];
			offset = p->set(shader, offset);
		}

		GLfloat screenSize[2] = { m_ShadingWindowW * 1.0f , m_ShadingWindowH * 1.0f };
		Sint32 screenSizeID = glGetUniformLocation(shader, "u_screen");
		glUniform2fv(screenSizeID, 1, screenSize);

		int textureID1;
		textureID1 = glGetUniformLocation(shader, "u_textureID1");
		glUniform1i(textureID1, 0);

		int textureID2;
		textureID2 = glGetUniformLocation(shader, "u_textureID2");
		glUniform1i(textureID2, 1);

		Float32 fRot = (gxLib::GetGameCounter()*2)%360;

		Float32 mx = 2*gxUtil::Touch()->Position()->x/m_GameW - 1.0f;
		Float32 my = 2 * gxUtil::Touch()->Position()->y/m_GameH - 1.0f;
		my *= -1.0f;
		Float32 pointlight_param[10] = { mx , my , 1.0 , 1.0, 1.0 ,    /*argb*/1.0, 1.0 , 1.0 , 1.0 };
		int pointLightID1;
		pointLightID1 = glGetUniformLocation(shader, "u_PLight");
		glUniform1fv(pointLightID1, 10 , pointlight_param );

	}

}


void COpenGL3::GetFrameBufferImage(gxChar* pFileName)
{
	// フレームバッファの内容をPBOに転送

	// PBO作成とバインド
	Uint32 length = m_GameW * m_GameH * 3;

#if 0
	GLuint pbo;


	glGenBuffers(1, &pbo);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pbo);

	glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, *pLength, 0, GL_DYNAMIC_DRAW);    // バッファの作成と初期化

	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

	glBindBuffer(GL_PIXEL_PACK_BUFFER_ARB, pbo);    // PBOをアクティブにする

	glReadPixels(0, 0, m_GameW, m_GameH, GL_BGRA, GL_UNSIGNED_BYTE, 0);     // 描画をPBOにロード

	// PBOの内容を編集
	GLubyte* ptr = (GLubyte*)glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_WRITE);
	glUnmapBufferARB(GL_PIXEL_PACK_BUFFER_ARB);
	glBindBuffer(GL_PIXEL_PACK_BUFFER_ARB, 0);
#endif

	GLubyte* ptr = (GLubyte*)new GLubyte[length];
	glBindFramebuffer(GL_FRAMEBUFFER, m_OffScreenTexture[ enGamePostProcess0 + m_CurrentPage ].frameBufferObject);

	glReadBuffer(GL_FRONT);

	// OpenGLで画面に描画されている内容をバッファに格納
	glReadPixels(
		0,                 //読み取る領域の左下隅のx座標
		0,                 //読み取る領域の左下隅のy座標 //0 or getCurrentWidth() - 1
		m_GameW,          //読み取る領域の幅
		m_GameH,          //読み取る領域の高さ
		GL_BGR, //it means GL_BGR,           //取得したい色情報の形式
		GL_UNSIGNED_BYTE,  //読み取ったデータを保存する配列の型
		ptr      //ビットマップのピクセルデータ（実際にはバイト配列）へのポインタ
	);

	if (ptr)
	{
		Uint32 argb;
		CFileTarga tga;
		tga.Create(m_GameW, m_GameH, 32);
		Uint32 n = 0;
		for (Uint32 yy = 0; yy < m_GameH; yy++)
		{
			for (Uint32 xx = 0; xx < m_GameW; xx++)
			{
				argb = ARGB( 0xff , ptr[3*n+2], ptr[3*n+1], ptr[3*n+0] );
				tga.SetARGB(xx, m_GameH - yy -1 , argb);
				n++;
			}
		}
		tga.SaveFile(pFileName);
	}

	SAFE_DELETES(ptr);
}


void _CheckError()
{

	GLenum err = glGetError();

	if( err )
	{
		GX_DEBUGLOG("err[%d]", err);// , gluGetString(err) );
	}
}
