﻿//---------------------------------------------------------------
//
// Windows Desktop向け::メイン
//
//
//
//---------------------------------------------------------------
//ren
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxPadManager.h>
#include <gxLib/gxFileManager.h>
#include <gxLib/gxBlueTooth.h>
#include <gxLib/util/gxUIManager.h>

#ifdef _USE_OPENGL
	#include "COpenGL3.h"
#else
	#include "CDx11Desktop.h"
#endif

#ifdef _USE_OPENAL
	#include "../COpenAL.h"
#else
	#include "../CXAudio2.h"
#endif

#include "gxLibResource.h"
#include "CGamePad.h"

#include <shellapi.h>	//<- VC2017ではこっち


SINGLETON_DECLARE_INSTANCE( CWindows );

#define NAME_APRICATION APPLICATION_NAME
#define NAME_APRICLASS  "gxLib2018"
#include "../CMemory.h"

void makeAccelKey();

enum {
	enID_ChangeFullScreenMode = 11001,	//フルスクリーン切り替え
	enID_AppExit              = 11002,	//アプリ終了
	enID_GamePause			  = 11003,	//ゲームのポーズ
	enID_GameStep			  = 11004,	//ゲームのステップ
	enID_PadEnableSwitch	  = 11005,	//コントローラー設定
	enID_DebugMode			  = 11006,	//デバッグモードのON/OFF
	enID_Reset				  = 11007,	//リセット
	enID_ScreenShot           = 11008,	//スクリーンショット
	enID_FullSpeed			  = 11009,	//フルスピード
	enID_SoundSwitch		  = 11010,	//サウンドOn /Off
	enID_SamplingFilter		  = 11011,	//サンプリングフィルター

	enID_MasterVolumeAdd	  = 11012,	//Volume+
	enID_MasterVolumeSub	  = 11013,	//Volume+
	enID_SwitchVirtualPad	  = 11014,	//VirtualPad On/ Off
	enID_DispManual			  = 11015,	//マニュアル表示
	enID_DispEULA			  = 11016,	//EULA表示

	enID_SoftPause       	  = 11300,	//Game Pause
	enAccelMax			  	  = 32,

};


LRESULT CALLBACK    gxLibWindowProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

void makeWindow( );
void SystemUpdate();
void AddNotificationIcon( HWND hWnd );

#define ID_TRAYICON (1)
#define WM_TASKTRAY     (WM_APP + 1) 
NOTIFYICONDATA nid = { sizeof(NOTIFYICONDATA) };

void BlueToothThreadMake();

int APIENTRY WinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

	struct Args
	{
		gxChar* ope;
		gxChar* code;
	};

	std::vector<Args> DragAndDropArgs;

	{
		//引数分解
		int NumArgs = 0;
		wchar_t* pWCharStr = CDeviceManager::UTF8toUTF16(lpCmdLine);
		LPWSTR *q = CommandLineToArgvW(pWCharStr, &NumArgs);
		for (Sint32 ii = 0; ii < NumArgs; ii++)
		{
			if (wcscmp( q[ii], L"-batchmode") == 0)
			{
				SetConsoleOutputCP(CP_UTF8);
				CWindows::GetInstance()->SetBatchMode(gxTrue);
				if (!::AttachConsole(ATTACH_PARENT_PROCESS))
				{
					::AllocConsole();
				}
				freopen("CON", "r", stdin);     // 標準入力の割り当て
				freopen("CON", "w", stdout);    // 標準出力の割り当て
				printf("%s:batchmodeStart\n", NAME_APRICATION);
			}
			else if (wcscmp(q[ii], L"-dd") == 0)
			{
				//gxFileManager::GetInstance()->SetDropFileNum(sNum);
				if (ii + 1 < NumArgs)
				{
					Args arg;
					arg.ope  = CDeviceManager::UTF16toUTF8(q[ii + 0]);
					arg.code = CDeviceManager::UTF16toUTF8(q[ii + 1]);
					DragAndDropArgs.push_back(arg);
					ii++;
				}
			}
		}
	}

/*
	TCHAR crDir[MAX_PATH + 1];
	TCHAR crDir2[MAX_PATH + 1];
	GetCurrentDirectory(MAX_PATH + 1, crDir);
	sprintf(crDir2, "%s/../../", crDir);
	SetCurrentDirectory(crDir2);
*/

	CWindows::GetInstance()->m_hInstance = hInstance;

	makeWindow();
	makeAccelKey();

	CDeviceManager::GetInstance()->AppInit();

	BlueToothThreadMake();


	while( gxTrue )
	{
		CGameGirl::GetInstance()->Action();

		if (DragAndDropArgs.size() > 0)
		{
			gxFileManager::GetInstance()->SetDragEnable(gxFalse);
			gxFileManager::GetInstance()->SetDropFileNum(DragAndDropArgs.size());
			while (DragAndDropArgs.size() > 0)
			{
				gxFileManager::GetInstance()->AddDropFile(DragAndDropArgs[0].code);
				SAFE_DELETES(DragAndDropArgs[0].ope);
				SAFE_DELETES(DragAndDropArgs[0].code);
				DragAndDropArgs.erase(DragAndDropArgs.begin());
			}
			gxFileManager::GetInstance()->SetDragEnable(gxTrue);
		}

		if (CGameGirl::GetInstance()->IsAppFinish())
		{
			break;
		}

		SystemUpdate();
	}

	//CDeviceManager::GetInstance()->SaveConfig();

	CBlueToothManager::GetInstance()->DestroyBlueToothThread();

	GX_DEBUGLOG("system close");
		
	CGameGirl::GetInstance()->End();

	//メインスレッドの終了まちを追加
	extern gxBool m_GameMainThreadExist;
	while (m_GameMainThreadExist)
	{
		gxLib::Sleep(10);
	}

	CGameGirl::DeleteInstance();

	CGraphics::DeleteInstance();

	CAudio::DeleteInstance();

    CGamePad::DeleteInstance();

	DestroyAcceleratorTable( CWindows::GetInstance()->m_hAccel);

	DestroyWindow( CWindows::GetInstance()->m_hWindow );

	CWindows::DeleteInstance();

	CDeviceManager::DeleteInstance();

	CBlueToothManager::DeleteInstance();

	gxLib::Destroy();

	//Uint32 uNow, uMax, uTotal;
	//uNow = uNow / 1024 / 1024;
	//GetInstance()->UpdateMemoryStatus(&uNow, &uTotal, &uMax);

	MemoryDestroy();

	printf("done.\n");

	return 0;
}


void makeWindow( )
{
    WNDCLASSEX windowConfig;

    windowConfig.cbSize = sizeof(WNDCLASSEX);

    windowConfig.style          = CS_HREDRAW | CS_VREDRAW;
    windowConfig.lpfnWndProc    = gxLibWindowProc;
    windowConfig.cbClsExtra     = 0;
    windowConfig.cbWndExtra     = 0;
    windowConfig.hInstance      = CWindows::GetInstance()->m_hInstance;
    windowConfig.hIcon          = LoadIcon(CWindows::GetInstance()->m_hInstance, MAKEINTRESOURCE(IDI_GXLIB_ICON));
    windowConfig.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    windowConfig.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    windowConfig.lpszMenuName   = MAKEINTRESOURCE(IDC_HELPWINDOW);
    windowConfig.lpszClassName  = TEXT( NAME_APRICLASS );
    windowConfig.hIconSm        = LoadIcon(windowConfig.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	RegisterClassEx( &windowConfig );


	//画面の中央にセット
	RECT desktop;

	GetWindowRect( GetDesktopWindow(), (LPRECT)&desktop);

	// フレームなどのクライアント領域以外のサイズを考慮

	Sint32 w,h;

	RECT rect = { 0, 0, WINDOW_W, WINDOW_H };

	AdjustWindowRect( &rect, WS_OVERLAPPEDWINDOW|WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_VISIBLE, FALSE );

	w = rect.right - rect.left;
	h = rect.bottom - rect.top;
	Sint32 winw = desktop.right - desktop.left;
	Sint32 winh = desktop.bottom - desktop.top;

	if(  w >= winw ||  h > winh )
	{
		w = w/2;
		h = h/2;
	}

	Sint32 ax,ay;

	ax = ( desktop.right  - desktop.left)/2 - w/2;
	ay = ( desktop.bottom - desktop.top )/2  - h/2;

	if( ax < 0) ax = 0;
	if( ay < 0) ay = 0;

	Sint32 frmHeight = GetSystemMetrics(SM_CYFIXEDFRAME);

	Sint32 hh = GetSystemMetrics(SM_CYMENU);// +frmHeight - 1;
	Sint32 ww = 0;//GetSystemMetrics(SM_CYFIXEDFRAME) * 2;

	CWindows::GetInstance()->m_hWindow = CreateWindow(
				TEXT( NAME_APRICLASS ),
				TEXT( NAME_APRICATION ),
	            WS_OVERLAPPEDWINDOW|WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,// | WS_VISIBLE,
				ax,
				ay,
				w + ww,
				h + hh,
				NULL,
				NULL,
				CWindows::GetInstance()->m_hInstance,
			    NULL);

//	ShowWindow( CWindows::GetInstance()->m_hWindow, gxTrue );
	ShowWindow( CWindows::GetInstance()->m_hWindow, !CWindows::GetInstance()->IsBatchMode() );
	UpdateWindow( CWindows::GetInstance()->m_hWindow );

	CWindows::GetInstance()->m_WinDC    = GetDC(CWindows::GetInstance()->m_hWindow );
	CWindows::GetInstance()->m_AppStyle = GetWindowLong(CWindows::GetInstance()->m_hWindow , GWL_STYLE);

	GetWindowRect( CWindows::GetInstance()->m_hWindow , &CWindows::GetInstance()->m_WinRect );

	/*-- 念のためウインドウサイズ補正 --*/
#if 0
	RECT client;
	GetClientRect( CWindows::GetInstance()->m_hWindow, &client );

	int diffx = (client.right - client.left) - WINDOW_W;
	int diffy = (client.bottom - client.top) - WINDOW_H;

	if ( diffx != 0 || diffy != 0 )
	{
		rect.right  -= diffx;
		rect.bottom -= diffy;

		MoveWindow(
			CWindows::GetInstance()->m_hWindow,
			CWindows::GetInstance()->m_WinRect.left,
			CWindows::GetInstance()->m_WinRect.top,
			rect.right - rect.left,
			rect.bottom - rect.top,
			TRUE
		);
	}
#endif
	// ハードがマルチタッチをサポートしているかどうか
	int  value= ~GetSystemMetrics( SM_DIGITIZER );

	if( !(value & 0xc0) )
	{
		RegisterTouchWindow(CWindows::GetInstance()->m_hWindow, 0 );
	}

	QueryPerformanceFrequency( (LARGE_INTEGER*)& CWindows::GetInstance()->Vsyncrate );		//秒間のカウント


}


void SystemUpdate()
{
	// --------------------------------------------------------------------
    // メイン メッセージ ループ:
    // ※これがないとWindowsからのメッセージを受けられなくなるので注意
	// --------------------------------------------------------------------

    MSG msg;

	while(PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
    {
		if(!GetMessage(&msg, NULL, 0, 0))
		{
			CWindows::GetInstance()->m_wParam = msg.wParam;
		}

        if ( !TranslateAccelerator( msg.hwnd, CWindows::GetInstance()->m_hAccel, &msg) )
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

	//スクリーンセーバーカウンターをリセット
	::SetThreadExecutionState(ES_DISPLAY_REQUIRED);
	//マウスを動かして、スクリーンセーバー抑止
	INPUT input[1];
	::ZeroMemory(input, sizeof(INPUT));
	input[0].type = INPUT_MOUSE;
	input[0].mi.dwFlags = MOUSEEVENTF_MOVE;//相対座標指定でマウスを動かす。
	input[0].mi.dx = 0; //でも０で初期化しているので相対座標０で動かさない。
	input[0].mi.dy = 0;
	::SendInput(1, input, sizeof(INPUT)); //移動

}

LRESULT CALLBACK gxLibWindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	// --------------------------------------------------------------------
	//基本的なウインドウコールバック受付
	// --------------------------------------------------------------------
	static HWND hStatus;

    switch (message){
    case WM_CREATE:
	    DragAcceptFiles(hWnd, gxTrue);
	    AddNotificationIcon( hWnd );
		break;

	case WM_DROPFILES:
		{
			HDROP hDrop = (HDROP) wParam;
			Sint32 sNum = DragQueryFile(hDrop , 0xffffffff , NULL , NULL );

			gxFileManager::GetInstance()->SetDragEnable( gxFalse );
			gxFileManager::GetInstance()->SetDropFileNum( sNum );

			gxChar str[512];

			for(Sint32 ii=0;ii<sNum;ii++)
			{
#if 0
				wchar_t wStr[512];
				DragQueryFile(hDrop , ii , (LPWSTR)wStr , sizeof(wStr) );
				Uint32 uLen = wcslen(wStr);
				WideCharToMultiByte( CP_ACP , 0 ,(LPCWSTR)wStr , uLen+1 , str, uLen*2 ,NULL,NULL);
#else
				DragQueryFile(hDrop , ii , str , sizeof(str) );
#endif
				gxFileManager::GetInstance()->AddDropFile( str );
			}
			gxFileManager::GetInstance()->SetDragEnable( gxTrue );
		}
		break;


	case WM_SYSCOMMAND:
		//スクリーンセーバー抑制
		if ( wParam == SC_SCREENSAVE )
	    {
	        return 1;
	    }
		if ( wParam == SC_MONITORPOWER )
	    {
	        return 1;
	    }
	    return (DefWindowProc(hWnd, message, wParam, lParam));

	case WM_TASKTRAY:
		if ( wParam == ID_TRAYICON )
		{
			switch ( lParam ){
			case WM_RBUTTONDOWN:
				{
					POINT po;
					HMENU hMenu , tmp;
					SetForegroundWindow( hWnd );
					GetCursorPos(&po);
					//.x = GET_X_LPARAM(lParam);
					//po.y = GET_Y_LPARAM(lParam);
					//ClientToScreen(hWnd , &po );
					tmp = GetMenu(hWnd);//LoadMenu( CWindows::GetInstance()->m_hInstance , TEXT(NAME_APRICLASS));
					hMenu = GetSubMenu(tmp , 0);
					TrackPopupMenu( hMenu , TPM_LEFTALIGN | TPM_BOTTOMALIGN ,	po.x , po.y , 0 , hWnd , NULL );
					GX_DEBUGLOG("WM_TASKTRAY");
				}
				break;
			}
		}
		break;

    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // 選択されたメニューの解析:
            switch (wmId)
            {
			case IDM_WINDOW_ORIGINAL:
				CGameGirl::GetInstance()->SetScreenMode(CGameGirl::enScreenModeOriginal );
				CGameGirl::GetInstance()->AdjustScreenResolution();
				break;

			case IDM_WINDOW_ASPECT:
				CGameGirl::GetInstance()->SetScreenMode(CGameGirl:: enScreenModeAspect );
				CGameGirl::GetInstance()->AdjustScreenResolution();
				break;

			case IDM_WINDOW_STRETCH:
				CGameGirl::GetInstance()->SetScreenMode(CGameGirl::enScreenModeFull );
				CGameGirl::GetInstance()->AdjustScreenResolution();
				break;

			case IDM_DRAW_3D:
				CGameGirl::GetInstance()->Set3DView( !CGameGirl::GetInstance()->Is3DView() );
				break;

			case enID_SoftPause:
				CGameGirl::GetInstance()->SetSoftPause( !CGameGirl::GetInstance()->IsSoftPause() );
				break;

			case enID_GamePause			 : 	//ゲームのポーズ
				CGameGirl::GetInstance()->SetPause( !CGameGirl::GetInstance()->IsPause() );
				break;

			case enID_GameStep			 : 	//ゲームのステップ
				if( CGameGirl::GetInstance()->IsPause() )
				{
					CGameGirl::GetInstance()->PauseStep();
				}
				else
				{
					CGameGirl::GetInstance()->SetPause(gxTrue);
				}
				break;

			case enID_PadEnableSwitch	 : 	//コントローラー切り替え
				gxPadManager::GetInstance()->SetEnableController( !gxPadManager::GetInstance()->IsEnableController() );
				break;

			case enID_DebugMode			 : 	//デバイスモードの切り替え
				CGameGirl::GetInstance()->ToggleDeviceMode();
				break;

			case enID_Reset:
			 	//リセット
			 	CGameGirl::GetInstance()->SetReset();
				break;

			case enID_DispManual:
				//マニュアル表示
				gxLib::OpenAppManual();
				break;

			case enID_DispEULA:
				//EULA表示
				gxLib::OpenEULADocument();
				break;

			case enID_ScreenShot         : 	//スクリーンショット
				CGameGirl::GetInstance()->SetScreenShot(gxTrue);
				break;

			case enID_SamplingFilter:
				//サンプリングフィルター切り替え
				if( CWindows::GetInstance()->GetRenderingFilter() == CWindows::enSamplingNearest )
				{
					CWindows::GetInstance()->SetRenderingFilter( CWindows::enSamplingBiLenear );
				}
				else
				{
					CWindows::GetInstance()->SetRenderingFilter( CWindows::enSamplingNearest );
				}
				break;

			case enID_FullSpeed			 : 	//フルスピード
				CGameGirl::GetInstance()->WaitVSync( !CGameGirl::GetInstance()->IsWaitVSync() );
				break;

			case enID_SoundSwitch		 : 	//サウンドON/OFF
				if( CWindows::GetInstance()->IsSoundEnable() )
				{
					CWindows::GetInstance()->SetSoundEnable(gxFalse);
				}
				else
				{
					CWindows::GetInstance()->SetSoundEnable(gxTrue);
				}
				break;

			case enID_ChangeFullScreenMode:	//フルスクリーン切り替え
			case IDM_FULLSCREEN_SWITCH:
				CWindows::GetInstance()->SetFullScreen( !CWindows::GetInstance()->IsFullScreen() );
				CGraphics::GetInstance()->Reset();
				break;

			case IDM_SOUND_ON:
				CWindows::GetInstance()->SetSoundEnable( gxTrue );
				break;
			case IDM_SOUND_OFF:
				CWindows::GetInstance()->SetSoundEnable( gxFalse );
				break;

            case IDM_ABOUT:
                DialogBox( CWindows::GetInstance()->m_hInstance, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
			case IDM_LICENSE:
                DialogBox( CWindows::GetInstance()->m_hInstance, MAKEINTRESOURCE(IDD_DIALOG_LICENSE), hWnd, About);
				break;

			case enID_MasterVolumeAdd:
				//Volume ++
				{
					Float32 fVol = gxLib::GetAudioMasterVolume();
					gxLib::SetAudioMasterVolume(fVol += 0.1f);
				}
				break;

			case enID_MasterVolumeSub:
				//Volume --
				{
					Float32 fVol = gxLib::GetAudioMasterVolume();
					gxLib::SetAudioMasterVolume( fVol -= 0.1f );
				}
				break;

			case IDM_CONTROLLER:
			case enID_SwitchVirtualPad:
				{
					CWindows::GetInstance()->m_bVirtualPad = !CWindows::GetInstance()->m_bVirtualPad;
					gxLib::SetVirtualPad(CWindows::GetInstance()->m_bVirtualPad );
				}
				break;

			case enID_AppExit             :	//アプリ終了
            case IDM_EXIT:
				if (CWindows::GetInstance()->IsFullScreen())
				{
					CWindows::GetInstance()->SetFullScreen(gxFalse);
					CGraphics::GetInstance()->Reset();
				}
				else
				{
					CGameGirl::GetInstance()->Exit();
				}
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;

	case WM_SIZE:
		{
			Sint32 w, h;
			w = LOWORD(lParam);
			h = HIWORD(lParam);
			//w -= 6;
			//h -= 2;
			CGameGirl::GetInstance()->SetWindowSize(w,h);
			CGameGirl::GetInstance()->AdjustScreenResolution();
			CDeviceManager::GetInstance()->Resume();

		}
		break;

    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: HDC を使用する描画コードをここに追加してください...
            EndPaint(hWnd, &ps);
        }
        break;

	case WM_QUERYENDSESSION:
		ShutdownBlockReasonCreate(hWnd,L"gxLib running" );
		return false;

 	case WM_ENDSESSION:
		CGameGirl::GetInstance()->Exit();
		ShutdownBlockReasonDestroy(hWnd);
		break;

	case WM_CLOSE:
		DragAcceptFiles(hWnd, gxFalse);
		CGameGirl::GetInstance()->Exit();
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        Shell_NotifyIcon( NIM_DELETE, &nid );
        break;

    default:
		//デバイスの入力状態をチェック
		if (!CGameGirl::IsEndApp())
		{
			CGamePad::GetInstance()->InputKeyCheck(message, wParam, lParam);
		}

        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}


INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	// --------------------------------------------------------------------
	// アバウトダイアログ
	// --------------------------------------------------------------------

    UNREFERENCED_PARAMETER(lParam);

    switch (message){
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


void makeAccelKey()
{
	// --------------------------------------------------------------------
	// アクセラレーターキーを登録する
	// --------------------------------------------------------------------

	ACCEL wAccel[ enAccelMax ];

	struct AccelInfo {
		Uint32 id;
		Uint32 vKey1;
		Uint32 vKey2;
	};

	AccelInfo AccelTbl[]={

#ifdef GX_DEBUG
		{ enID_AppExit 				,VK_ESCAPE	,0 },
		{ enID_SoftPause	     	,'P'		,FALT|FVIRTKEY },
		{ enID_ChangeFullScreenMode	,VK_RETURN	,FALT },
		{ enID_GamePause			,VK_F1		,FVIRTKEY },
		{ enID_SoundSwitch			,VK_F2		,FVIRTKEY },
		{ enID_GameStep				,VK_F3		,FVIRTKEY },
		{ enID_SwitchVirtualPad		,VK_F4		,FVIRTKEY },

		{ enID_DispManual			,VK_F5		,FVIRTKEY },
		{ enID_DispEULA				,VK_F6		,FVIRTKEY },
		{ enID_Reset				,VK_F8		,FVIRTKEY },

		{ enID_DebugMode			,VK_F9		,FVIRTKEY },
		{ enID_PadEnableSwitch		,VK_F10		,FVIRTKEY },
		{ enID_ScreenShot			,VK_F11		,FVIRTKEY },
		{ enID_FullSpeed			,VK_F12		,FVIRTKEY },

		{ enID_MasterVolumeAdd		,VK_ADD			/*VK_PRIOR*/,FVIRTKEY },
		{ enID_MasterVolumeSub		,VK_SUBTRACT	/*VK_NEXT*/	,FVIRTKEY },

//		{ enID_ChangeFullScreenMode	,VK_F5		,FVIRTKEY },
//		{ enID_SamplingFilter		,VK_F6		,FVIRTKEY },
#else
		{ enID_AppExit 				,VK_ESCAPE	,0 },
		{ enID_ChangeFullScreenMode	,VK_RETURN	,FALT },
		{ enID_SoftPause			,VK_F1		,FVIRTKEY },
		{ enID_SwitchVirtualPad		,VK_F4		,FVIRTKEY },
		{ enID_DispManual			,VK_F5		,FVIRTKEY },
		{ enID_DispEULA				,VK_F6		,FVIRTKEY },
		{ enID_Reset				,VK_F8		,FVIRTKEY },
		{ enID_PadEnableSwitch		,VK_F10		,FVIRTKEY },
		{ enID_MasterVolumeAdd		,VK_ADD			/*VK_PRIOR*/,FVIRTKEY },
		{ enID_MasterVolumeSub		,VK_SUBTRACT	/*VK_NEXT*/	,FVIRTKEY },
		{ enID_AppExit 				,VK_ESCAPE	,0 },
#endif
	};

	Sint32 max = ARRAY_LENGTH(AccelTbl);

	for( Sint32 ii=0; ii<max; ii++ )
	{
		wAccel[ii].cmd   = AccelTbl[ii].id;
		wAccel[ii].key   = AccelTbl[ii].vKey1;
		wAccel[ii].fVirt = AccelTbl[ii].vKey2;
	}

	CWindows::GetInstance()->m_hAccel = CreateAcceleratorTable( wAccel, max );

}


void AddNotificationIcon( HWND hWnd )
{

	//nid.cbSize = ;
	nid.hWnd = hWnd;
	nid.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE | NIF_SHOWTIP | NIF_GUID;
	nid.hIcon = LoadIcon(CWindows::GetInstance()->m_hInstance, MAKEINTRESOURCE(IDI_SMALL));;
	nid.uID   = ID_TRAYICON;
	//nid.uCallbackMessage = WMAPP_NOTIFYCALLBACK;
	nid.uCallbackMessage = WM_TASKTRAY;      // 通知メッセージの定数
	lstrcpy(nid.szTip, TEXT(NAME_APRICATION));

	if( !Shell_NotifyIcon(NIM_ADD, &nid) )
	{
		DWORD lasterror;
		lasterror = GetLastError();
		// GetLastError == ERROR_SUCCESS or ERROR_FILE_NOT_FOUND // taskbar was not found.
		while( lasterror == ERROR_TIMEOUT )
		{
			Sleep(1000);
			if( Shell_NotifyIcon(NIM_MODIFY, &nid) )
			{
				break;// NotifyIcon alredy exists.
			}
			if( Shell_NotifyIcon(NIM_ADD, &nid) )
			{
				break;// NotifyIcon adding is successful.
			}
			lasterror = GetLastError();
		}
	}
	// NOTIFYICON_VERSION_4 is prefered
	nid.uVersion = NOTIFYICON_VERSION_4;

	//return Shell_NotifyIcon(NIM_SETVERSION, &nid);
}


void CWindows::ExecuteApp( char *Appname )
{
	//アプリケーションを実行する

	Uint32 len = strlen( Appname );

#if 0
	wchar_t wStr[0xff];
	len = MultiByteToWideChar( CP_ACP , 0 ,(char*)Appname , strlen((char*)Appname) , wStr , 0xff );
	wStr[len] = 0x0000;
	ShellExecute( CWindows::GetInstance()->m_hWindow, _T("open"),  wStr, NULL, NULL, SW_SHOWNORMAL);
#else
	ShellExecute(CWindows::GetInstance()->m_hWindow, _T("open"),  Appname, NULL, NULL, SW_SHOWNORMAL);
#endif

}

gxChar* CWindows::GetClipBoard( Uint32 type )
{
	//クリップボードのテキストを参照する

	HGLOBAL hMem;       // 取得用のメモリ変数
	LPTSTR  lpBuff;     // 参照用のポインタ
	gxChar* p = NULL;

	if ( OpenClipboard( CWindows::GetInstance()->m_hWindow ) )
	{
	    if ( (hMem = GetClipboardData(CF_TEXT)) != NULL )
	    {
	        if ( (lpBuff = (LPTSTR)GlobalLock(hMem)) != NULL )
	        {
				Uint32 len = strlen( lpBuff );
				p= new gxChar[len];
				
				sprintf( p , "%s" , lpBuff );

	            GlobalUnlock( hMem );
	        }
	    }
	    CloseClipboard();
	}

	return p;
}


gxBool CWindows::SetClipBoard( gxChar *pText )
{
	//テキストをクリップボードに保存する

	HGLOBAL     hMem;       // 設定用のメモリ変数
	LPTSTR      lpBuff;     // 複写用のポインタ
	LPTSTR      lpText;     // 複写元の文字列
	DWORD       dwSize;     // 複写元の長さ

	dwSize = ((lstrlen(pText) + 1) * sizeof(TCHAR));   // '\0' 文字分を加算

	//make
	if ( (hMem = GlobalAlloc((GHND|GMEM_SHARE),dwSize)) != NULL )
	{
	    if ( (lpBuff = (LPTSTR)GlobalLock(hMem)) != NULL )
	    {
	        lstrcpy( lpBuff, pText );                  // lpBuff にコピー
	        GlobalUnlock( hMem );

	        if ( OpenClipboard( CWindows::GetInstance()->m_hWindow ) )
	        {
	            EmptyClipboard();
	            SetClipboardData( CF_TEXT, hMem );      // データを設定
	            CloseClipboard();
			    GlobalFree( hMem );                             // クリップボードが開けないとき解放
				return gxTrue;
	        }
	    }
	    GlobalFree( hMem );                             // クリップボードが開けないとき解放
	}

	return gxFalse;
}


