﻿//------------------------------------------------------------
//
// machine.cpp
// マシン固有のデバイスへのアクセスを行うものはここに書く
// ハードウェアに依存する部分
//
//------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxDebug.h>
#include <gxLib/gxFileManager.h>

#ifdef _USE_OPENGL
#include "COpenGL3.h"
#else
#include "CDx11Desktop.h"
#endif

#ifdef _USE_OPENAL
	#include "../COpenAL.h"
#else
	#include "../CXAudio2.h"
#endif

#include "CGamePad.h"
#include "../CMemory.h"
#include "CHttpClient.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>
#include <fcntl.h>

//Float32 getFrameTime();
SINGLETON_DECLARE_INSTANCE( CDeviceManager );

/*
typedef struct gxTimeval {
  long tv_sec;
  long tv_usec;
} gxTimeval;
*/


CDeviceManager::CDeviceManager()
{

}

CDeviceManager::~CDeviceManager()
{
	CHttpClient::DeleteInstance();

}

void CDeviceManager::AppInit()
{
	static int nnn = 0;
	if( nnn == 0 )
	{
		CGameGirl::GetInstance()->Init();
		CGraphics::GetInstance()->Init();
		CAudio::GetInstance()->Init();
	    CGamePad::GetInstance()->Init();

		gxLib::SetVirtualPad( CWindows::GetInstance()->m_bVirtualPad );

		MemoryInit();
	}
	else
	{
        CGameGirl::GetInstance()->SetResume();
	}

	CGameGirl::GetInstance()->AdjustScreenResolution();

	nnn ++;
}


void CDeviceManager::GameInit()
{
	//バックで受け取った入力機器の情報をgxLib側へ更新する

	CGamePad::GetInstance()->Action();
}


void CDeviceManager::Render()
{
	//描画処理
	
	if( CGameGirl::GetInstance()->IsResume() )
	{
		return;
	}

	CGraphics::GetInstance()->Update();
	CGraphics::GetInstance()->Render();
}


void CDeviceManager::vSync()
{
	//1/60秒の同期待ち
	static Float32 _TimeOld = gxLib::GetTime();

	Float32 _TimeNow;
	do
	{
		_TimeNow = gxLib::GetTime();
		if (_TimeNow < _TimeOld - 100.0f)
		{
			int nnn = 0;
			nnn++;
		}
	}
	while( _TimeNow < ( _TimeOld + (1.0f/ FRAME_PER_SECOND) ) );

	_TimeOld = _TimeNow;

}


#if 0
gxBool CDeviceManager::LoadConfig()
{
	//コンフィグファイルをロードする
/*
	char buf[1024];
	sprintf( buf , "%s", FILENAME_CONFIG );

	//-------------------------------------------------------------------

	Uint8 *pData = NULL;
	Uint32 uSize = 0;

	pData = LoadFile( buf , &uSize , STORAGE_LOCATION_INTERNAL);

	if( pData )
	{
		gxUtil::MemCpy( &gxLib::SaveData , pData , sizeof(gxLib::StSaveData) );
	}
	else
	{
		return gxFalse;
	}

	SAFE_DELETES( pData );
*/
	return gxTrue;
}


gxBool CDeviceManager::SaveConfig()
{
/*
	char buf[1024];
	sprintf( buf , "%s", FILENAME_CONFIG );

	SaveFile( buf , (Uint8*)&gxLib::SaveData , sizeof(gxLib::StSaveData) , STORAGE_LOCATION_INTERNAL);
*/
	return gxTrue;
}
#endif

void CDeviceManager::Movie()
{
	
}

void CDeviceManager::Resume()
{

}


void CDeviceManager::UploadTexture(Sint32 sBank)
{
	CGraphics::GetInstance()->ReadTexture( sBank );
}


void CDeviceManager::Flip()
{
	//バックバッファに用意した画像を転送する

	CGraphics::GetInstance()->Present();
}


void CDeviceManager::Play()
{
	CAudio::GetInstance()->Action();
}


gxBool CDeviceManager::NetWork()
{
	CHttpClient::GetInstance()->Action();

	return gxTrue;
}


void CDeviceManager::MakeThread( void* (*pFunc)(void*) , void * pArg )
{
	// スレッドの作成
	//void func( void *arg )型の関数ポインタを渡すこと！ 

	DWORD threadId; 
	HANDLE hThread;

	hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)pFunc, (LPVOID)pArg, CREATE_SUSPENDED, &threadId); 

	SetThreadPriority( hThread , THREAD_PRIORITY_HIGHEST );

	// スレッドの起動 
	ResumeThread(hThread); 
}


void CDeviceManager::Sleep( Uint32 msec )
{
	::Sleep( msec );
}

gxBool CDeviceManager::PadConfig( Sint32 padNo , Uint32 button )
{
	return gxTrue;
}

void CDeviceManager::Clock( gxLib::Clock *pClock )
{
	//現在の時刻をミリ秒で取得する
	std::chrono::system_clock::time_point now;
	now = std::chrono::system_clock::now();
	time_t tt = std::chrono::system_clock::to_time_t(now);
	tm local_tm = *localtime(&tt);

	std::chrono::system_clock::duration tp = now.time_since_epoch();
	std::chrono::microseconds us = std::chrono::duration_cast<std::chrono::microseconds>(tp);

	pClock->Year  = local_tm.tm_year + 1900;	// years since 1900
	pClock->Month = local_tm.tm_mon + 1;		// months since January - [0, 11]
	pClock->Day   = local_tm.tm_mday;   		// day of the month - [1, 31]
	pClock->DOW   = local_tm.tm_wday; 			// days since Sunday - [0, 6]
	pClock->Hour  = local_tm.tm_hour; 			// hours since midnight - [0, 23]
	pClock->Min   = local_tm.tm_min; 			// minutes after the hour - [0, 59]
	pClock->Sec   = local_tm.tm_sec; 			// seconds after the minute - [0, 60] including leap second
	pClock->MSec = (us.count()/1000) % (1000);
	pClock->USec = us.count() % (1000);

}

void CDeviceManager::LogDisp(char* pString)
{
	//デバッグログの表示

	if( CWindows::GetInstance()->IsBatchMode() )
	{
		printf(pString);
		printf("\n");
		return;
		wchar_t* p = UTF8toUTF16(pString);
		//wprintf(L"%s" , p);
		//wprintf(L"%s", "\n");
		std::wcout.imbue(std::locale("", std::locale::ctype));
		std::wstring str = p;
		// 標準出力へ出力する
		std::wcout << str << std::endl;
		SAFE_DELETES(p);
		return;
	}

	wchar_t* p = UTF8toUTF16(pString);
	OutputDebugStringW(p);// pString);
	OutputDebugStringW( L"\n" );
	SAFE_DELETES(p);
	//wchar_t* pWCharStr = UTF8toUTF16(pString);
	//OutputDebugStringW(pWCharStr);
	//OutputDebugStringW( L"\n" );
}

#include <gxLib/util/CFileZip.h>

Uint8    *g_pZipBuffer = NULL;
CFileZip *g_pAssetZip  = NULL;
gxChar m_ZipPath[FILENAMEBUF_LENGTH]={0};	//TODO::あとで開放すること

Uint8* getZipFileData( gxChar* pZipName , gxChar* pFileName , Uint32 *pLength )
{
	Uint32 len = strlen( pFileName );

	gxChar *fileName0 = new gxChar[32 + len];

	len = sprintf_s(fileName0, len + 32, "%s", pFileName);

	//zip時は / でパスを区切る

	for (int ii = 0; ii<len; ii++)
	{
		if (fileName0[ii] == '\\')
		{
			fileName0[ii] = '/';
		}
	}

	if( pLength )
	{
		*pLength = 0;
	}


	//------------------------------------
	int fh;

	Uint8* pBuffer = NULL;
	struct stat filestat;
	long sz, readsz;
	unsigned long pos = 0;
	int ret = 1;

	//-----------------------------------------

	Uint8 *pReturnData = NULL;
	gxBool bMakeNewZip = gxFalse;

	if( g_pAssetZip )
	{
		if( strcmp( m_ZipPath , pZipName ) != 0 )
		{
			bMakeNewZip = gxTrue;
			SAFE_DELETE( g_pAssetZip );
			SAFE_DELETES( g_pZipBuffer );

		}
	}
	else
	{
		bMakeNewZip = gxTrue;
	}

	if( bMakeNewZip )
	{
		fh = open( (char*)pZipName, O_RDONLY | O_BINARY );

		if ( fh < 0 )
		{
			SAFE_DELETES(fileName0);
			return NULL;
		}

		fstat(fh, &filestat);
		readsz = sz = filestat.st_size;

		pBuffer = new Uint8[ readsz ];

		if (pBuffer == NULL) return NULL;

		while (ret > 0)
		{
			if (readsz > 1024) readsz = 1024;

			ret = read( fh, &pBuffer[pos], readsz );
			pos += ret;
			sz -= ret;
			readsz = sz;
		}

		close(fh);

		g_pZipBuffer = pBuffer;

		g_pAssetZip = new CFileZip();

		if ( !g_pAssetZip->Read( pBuffer , filestat.st_size ) )
		{
			//そもそもzipがなかった
			SAFE_DELETES( pBuffer );
			SAFE_DELETES( fileName0 );
			return NULL;
		}

		//最後に読んだZIP名を覚えておく
		sprintf( m_ZipPath ,"%s" , pZipName );
	}

	//Zipは読めている

	Uint32 uSize = 0;

	pReturnData = g_pAssetZip->Decode( fileName0, &uSize );

	*pLength = uSize;

	//SAFE_DELETE( pZip );		//使い回しがあるのでここでは消さない
	//SAFE_DELETES( pBuffer );	//使い回しがあるのでここでは消さない

	SAFE_DELETES( fileName0 );


	return pReturnData;
}

std::string cnvertPlatformFileName( const gxChar* pFileName0 )
{
	// 「/」を「￥」に変える

	gxChar fileName[1024];
	sprintf( fileName , "%s" , pFileName0 );

	Uint32 len = strlen(fileName);

	for( int ii=0; ii<len; ii++ )
	{
		if( fileName[ii] == '/' )
		{
			fileName[ii] = '\\';
		}
	}

	std::string ret = fileName;

	return ret;

}

Uint8* loadFile2( const gxChar* pFileNameU8 , Uint32* pLength )
{
	//------------------------------------

	int fh;

	Uint8* pBuffer = NULL;
	struct stat filestat;
	long sz, readsz;
	unsigned long pos = 0;
	int ret = 1;

	//まずはSJISでチャレンジ
	char buf[1024];
	GetCurrentDirectory(1024,buf);
	gxChar *pFileNameSJIS = CDeviceManager::UTF8toSJIS((gxChar*)pFileNameU8);
	fh = open( (char*)pFileNameSJIS, O_RDONLY | O_BINARY );
	SAFE_DELETES(pFileNameSJIS);

	if ( fh < 0 )
	{
		//生データ名でチャレンジ
		fh = open((char*)pFileNameU8, O_RDONLY | O_BINARY);

		if (fh < 0)
		{
			return NULL;
		}
	}

	fstat(fh, &filestat);
	readsz = sz = filestat.st_size;

	*pLength = filestat.st_size;
	pBuffer = new Uint8[readsz];

	if (pBuffer == NULL) return NULL;

	while (ret > 0)
	{
		if (readsz > 1024) readsz = 1024;

		ret = read(fh, &pBuffer[pos], readsz);
		pos += ret;
		sz -= ret;
		readsz = sz;
	}

	close(fh);

	return pBuffer;

}


Uint8* CDeviceManager::LoadFile( const gxChar* pFileNameU8 , Uint32* pLength , Uint32 location )
{

	std::string url = pFileNameU8;
	std::replace(url.begin(), url.end(), '/', '\\');
	std::string fileName = "";

	if (std::string::npos != url.find(':'))
	{
		location = STORAGE_LOCATION_EXTERNAL;
	}

#ifdef FILE_FROM_ZIP

	if( !bDirect )
	{
		
		Uint8 *pRetData = NULL;

		switch( location ){
		case gxFileManager::STORAGE_LOCATION_ROM:
			sprintf( fileName, "Storage\\rom.zip");
			pRetData = getZipFileData( fileName , fileNameU8, pLength );
			if( pRetData )
			{
				return pRetData;
			}

			//ファイルが見つからなかったので実データを探しに行く

			break;

		case gxFileManager::STORAGE_LOCATION_DISC:
			sprintf( fileName, "Storage\\disc.zip");
			pRetData = getZipFileData( fileName , fileNameU8, pLength );
			if( pRetData )
			{
				return pRetData;
			}

			//ファイルが見つからなかったので実データを探しに行く

			break;

		case gxFileManager::STORAGE_LOCATION_MCARD:
		case gxFileManager::STORAGE_LOCATION_SD:
			//こいつらはファイル読み込みのみ対応
		default:
			break;
		}
	}

#endif


	Uint8 *pData = nullptr;

	switch( location ){
	case STORAGE_LOCATION_ROM:
		//LoadFile(2)

		//disc
		fileName = "Storage\\02_disc\\" + url;
		pData = loadFile2(fileName.c_str(), pLength);
		if( pData == nullptr )
		{
			//rom
			fileName = "Storage\\01_rom\\" + url;
			pData = loadFile2(fileName.c_str(), pLength);
			if( pData == nullptr )
			{
				return nullptr;
			}
		}
		break;

	case STORAGE_LOCATION_INTERNAL:
		//LoadStorageFile / SaveStorage

		//disk
		fileName = "Storage\\03_disk\\" + url;
		pData = loadFile2(fileName.c_str(), pLength);
		if( pData == nullptr )
		{
			//disc
			fileName = "Storage\\02_disc\\" + url;
			pData = loadFile2(fileName.c_str(), pLength);
			if( pData == nullptr )
			{
				//rom
				fileName = "Storage\\01_rom\\" + url;
				pData = loadFile2(fileName.c_str(), pLength);
				if( pData == nullptr )
				{
					return nullptr;
				}
			}
		}
		break;

	case STORAGE_LOCATION_EXTERNAL:
	default:
		//LoadFile(1) / SaveFile
		fileName = url;
		break;
	}

	return pData;
}


bool CreateDirectories(char* pURL )
{
	//フォルダを作成する
	//とりあえずわたってきた名前をそのまま使う
	//Write時には２回（sjis,u8の２回処理が来ることに留意）

	gxBool bSuccess = gxTrue;

	//size_t len = strlen(pURL);
	//char *pBuf = new char[len+1];
	//char *pSeparate[256];
	//int  cnt = 0;
	std::vector<char*> separates;
	std::string url = pURL;;
	std::replace(url.begin(), url.end(), '\\', '/' );

	char *pBuf = new char[ url.size()+1];
	sprintf( pBuf, "%s", url.c_str());

	Sint32 cnt = 0;
	for ( Sint32 ii= url.size(); ii>=0; ii-- )
	{
		if ( pBuf[ii] == ':')
		{
			separates.push_back(&pBuf[0]);
			cnt++;
			break;
		}
		else if ( ii == 0 )
		{
			separates.push_back(&pBuf[0]);
			cnt++;
			break;
		}
		else if ( pBuf[ii] == '/')
		{
			pBuf[ii] = 0x00;
			if( cnt > 0 ) separates.push_back(&pBuf[ii + 1]);
			cnt++;
		}
	}

	size_t max = separates.size();
	std::string path = "";

	for (Sint32 ii = 0; ii < max; ii++)
	{
		path += separates[max - 1 - ii];

			if (!CreateDirectory( path.c_str(), NULL))
		{
			bSuccess = gxFalse;
		}

		path += "/";
	}

	delete[] pBuf;

	return bSuccess;
}


gxBool CDeviceManager::SaveFile( const gxChar* pFileNameU8 , Uint8* pReadBuf , Uint32 uSize, Uint32 location )
{
	//ファイルの書き込み

	Uint32 len = strlen(pFileNameU8);

	std::string url = pFileNameU8;
	std::replace(url.begin(), url.end(), '/', '\\');
	std::string fileName = "";

	if ( std::string::npos !=  url.find(':') )
	{
		location = STORAGE_LOCATION_EXTERNAL;
	}

	switch( location ){
	case STORAGE_LOCATION_ROM:
		return gxFalse;

	case STORAGE_LOCATION_INTERNAL:
		fileName = "Storage\\03_disk\\" + url;
		break;

	case STORAGE_LOCATION_EXTERNAL:
	default:
		fileName = url;
		break;
	}

	// ---------------------------------------------------------------------------------
	//U8をSJISに変換
	//※Zipからの展開だとZipに内包されたファイル名がSJISでわたってくる可能性がある
	gxChar *pFileNameSJIS = UTF8toSJIS( (gxChar*)fileName.c_str());
	// ---------------------------------------------------------------------------------

	int fh;
	Uint8* pBuffer = NULL;
	unsigned long pos=0;
	int ret=1;

	fh = open( (gxChar*)pFileNameSJIS,O_WRONLY|O_BINARY|O_TRUNC|O_CREAT,S_IREAD|S_IWRITE);

	if(fh < 0)
	{
		//書き込みミス
		CreateDirectories(pFileNameSJIS );

		//フォルダを作って再度チャレンジ

		fh = open( (gxChar*)pFileNameSJIS,O_WRONLY|O_BINARY|O_TRUNC|O_CREAT,S_IREAD|S_IWRITE);

		if( fh < 0 )
		{
			SAFE_DELETES(pFileNameSJIS);

			//生ファイル名で再度チャレンジ

			fh = open((gxChar*)fileName.c_str(), O_WRONLY | O_BINARY | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);

			if (fh < 0)
			{
				CreateDirectories((gxChar*)fileName.c_str());

				fh = open((gxChar*)fileName.c_str(), O_WRONLY | O_BINARY | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);

				if (fh < 0)
				{
					return gxFalse;
				}
			}
		}
	}

	write(fh,pReadBuf,uSize);

	close(fh);

	SAFE_DELETES(pFileNameSJIS);

	return gxTrue;
}

/*
gxBool CDeviceManager::SaveFile(const gxChar* pFileName, Uint8* pReadBuf, Uint32 uSize , Uint32 location )
{
	Uint32 len = strlen(pFileName);

	gxChar *fileName0 = new gxChar[len+32];

	len = sprintf_s(fileName0, len+32, "%s", pFileName);

	gxBool bDirect = gxFalse;
	for( int ii=0; ii<len; ii++ )
	{
		if( fileName0[ii] == '/' )
		{
			fileName0[ii] = '\\';
		}
		if( fileName0[ii] == ':' )
		{
			bDirect = gxTrue;
		}
	}


	char fileName[512];
	if( bDirect )
	{
		sprintf(fileName, "%s", fileName0 );
	}
	else
	{
		sprintf(fileName, "Storage\\memcard\\%s", fileName0);
	}

	int fh;

	fh = open((char*)fileName, O_WRONLY | O_BINARY | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);

	//updateMemoryStatus();

	if (fh<0)
	{
		//書き込みミス
		return gxFalse;
	}
	else
	{
		write(fh, pReadBuf, uSize);
	}

	close(fh);

	return gxTrue;
}
*/


gxBool CDeviceManager::GamePause()
{
	return gxTrue;
	//return ::GamePause();
}

gxBool CDeviceManager::GameUpdate()
{
/*
	static gxBool m_bGameInit = gxFalse;
	gxBool bExist = gxTrue;

	if( !m_bGameInit )
	{
		if( ::GameInit() )
		{
			m_bGameInit = gxTrue;
		}
		else
		{
			return gxTrue;
		}
	}

	if( bExist )
	{
		//ゲームメインへ
		bExist = ::GameMain();
	}

	if( !bExist ) return gxFalse;
*/

	return gxTrue;
}


void CDeviceManager::UpdateMemoryStatus( Uint32* uNow , Uint32* uTotal , Uint32* uMax )
{
	//KBで帰ってきた状態をMBで返す

	Uint32 uNowByte;
	Uint32 uTotalByte;
	Uint32 uMaxByte;

	::UpdateMemoryStatus(&uNowByte, &uTotalByte, &uMaxByte);

	*uNow = (uNowByte >> 10) >> 10;
	*uTotal = (uTotalByte >> 10) >> 10;
	*uMax = (uMaxByte >> 10) >> 10;
}


static wchar_t WCharBuf[1024];
static wchar_t *pWChar = WCharBuf;// NULL;

wchar_t *CDeviceManager::SJIStoUTF16( gxChar* pString , size_t *pSize )
{
	//char から wcharに変換する

	size_t u8Len = strlen( pString );
	size_t maxSize = u8Len*4;

	wchar_t *pW1 = new wchar_t[maxSize];

	//UTF8 -> WIDECHAR(UTF16)
	//size_t sz = MultiByteToWideChar( CP_UTF8, 0, (char*)pString, u8Len, pW1, maxSize);

	//SJIS -> WIDECHAR(UTF16)
	size_t sz = MultiByteToWideChar( CP_ACP, 0, (char*)pString, u8Len, pW1, maxSize);

	//ここのSZはWIDECHARの文字数なのでバイト数に変換する
	sz *= 2;

	Uint8 *pW2 = new Uint8[sz + 2];

	gxUtil::MemCpy( pW2 , pW1 , sz );
	pW2[sz+0] = 0x00;
	pW2[sz+1] = 0x00;

	SAFE_DELETES(pW1);

	if( pSize ) *pSize = sz;

	return (wchar_t*)pW2;
}


wchar_t* CDeviceManager::UTF8toUTF16(gxChar* pString, size_t* pSize)
{
	//char から wcharに変換する

	size_t u8Len = strlen(pString);
	size_t maxSize = u8Len * 4;

	wchar_t* pW1 = new wchar_t[maxSize];


	//UTF8 -> WIDECHAR(UTF16)
	size_t sz = MultiByteToWideChar( CP_UTF8, 0, (char*)pString, u8Len, pW1, maxSize);

	//SJIS -> WIDECHAR(UTF16)
	//size_t sz = MultiByteToWideChar(CP_ACP, 0, (char*)pString, u8Len, pW1, maxSize);

	//ここのSZはWIDECHARの文字数なのでバイト数に変換する
	sz *= 2;

	Uint8* pW2 = new Uint8[sz + 2];

	gxUtil::MemCpy(pW2, pW1, sz);
	pW2[sz + 0] = 0x00;
	pW2[sz + 1] = 0x00;

	SAFE_DELETES(pW1);

	if (pSize)* pSize = sz;

	return (wchar_t*)pW2;
}

gxChar* CDeviceManager::UTF16toUTF8(wchar_t *pUTF16buf, size_t* pSize)
{
	Uint8* u8 = (Uint8*)pUTF16buf;

	if (u8[0] == 0xff && u8[1] == 0xfe)
	{
		//BOM付きだったのでオフセットする
		pUTF16buf++;
	}

	size_t u16len = wcslen(pUTF16buf);

	gxChar* pBuf = new gxChar[u16len*4];

	//size_t sz;
	//wchar_t* pw = CDeviceManager::MultiByteToWChar(m_pBuf, &sz);
	//gxUtil::MemCpy( pBuf, m_pBuf , m_uStringSize );

	//setlocale(LC_ALL, "ja_JP.UTF-8");
	//size_t length = wcstombs( pBuf, pUTF16buf, u16len * 4);
	size_t length = WideCharToMultiByte( CP_UTF8, 0, pUTF16buf, -1, &pBuf[0], u16len * 4, NULL, NULL);

	if (length > u16len * 4)
	{
		*pSize = 0;
		pBuf[0] = 0x00;
		return pBuf;
	}

	gxChar* pBuf2 = new gxChar[length + 1];
	gxUtil::MemCpy(pBuf2, pBuf, length);
	pBuf2[length] = 0x00;

	SAFE_DELETES( pBuf );

	if( pSize ) *pSize = length;

	return (gxChar*)pBuf2;
}


gxChar* CDeviceManager::UTF16toSJIS(wchar_t* pUTF16buf, size_t* pSize)
{
	Uint8* u8 = (Uint8*)pUTF16buf;
	if (u8[0] == 0xff && u8[1] == 0xfe)
	{
		//BOM付きだったのでオフセットする
		pUTF16buf++;
	}

	size_t u16len = wcslen(pUTF16buf);

	gxChar* pBuf = new gxChar[u16len * 4];

	//size_t sz;
	//wchar_t* pw = CDeviceManager::MultiByteToWChar(m_pBuf, &sz);
	//gxUtil::MemCpy( pBuf, m_pBuf , m_uStringSize );

	//setlocale(LC_ALL, "ja_JP.Shift_JIS");
	//size_t length = wcstombs(pBuf, pUTF16buf, u16len * 4);
	size_t length = WideCharToMultiByte(CP_ACP, 0, pUTF16buf, -1, pBuf, u16len*4, NULL, NULL);

	gxChar* pBuf2 = new gxChar[length + 1];
	gxUtil::MemCpy(pBuf2, pBuf, length);
	pBuf2[length] = 0x00;

	SAFE_DELETES(pBuf);

	if (pSize)* pSize = length;

	return (gxChar*)pBuf2;
}

gxChar* CDeviceManager::UTF8toSJIS( gxChar* pUTF8buf, size_t* pSize)
{
	wchar_t *p16  = UTF8toUTF16(pUTF8buf, pSize);
	gxChar* pSJIS = UTF16toSJIS(p16, pSize);
	SAFE_DELETES(p16);

	return pSJIS;
}

gxChar* CDeviceManager::SJIStoUTF8( gxChar* pSJISbuf, size_t* pSize)
{
	wchar_t* p16  = SJIStoUTF16(pSJISbuf, pSize);
	gxChar* pUTF8 = UTF16toUTF8(p16, pSize);
	SAFE_DELETES(p16);

	return pUTF8;

}


#include <shellapi.h>
#include "gxLibResource.h"

void CDeviceManager::ToastDisp( gxChar* pMessage )
{
	NOTIFYICONDATA	tn = { NOTIFYICONDATA_V2_SIZE };
	tn.hWnd = CWindows::GetInstance()->m_hWindow;
	tn.uID = 100;
	tn.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
	//tn.uCallbackMessage = WM_FASTCOPY_NOTIFY;
	tn.hIcon = LoadIcon(CWindows::GetInstance()->m_hInstance, (LPCSTR)(LONG_PTR)IDI_GXLIB_ICON);	//hIcon;
	sprintf(tn.szTip, "testtest");
	tn.uFlags |= NIF_INFO;

	strncpy(tn.szInfoTitle, APPLICATION_NAME, sizeof(tn.szInfoTitle));
	strncpy(tn.szInfo, pMessage, sizeof(tn.szInfo));

	tn.uTimeout		= 3 * 1000;
	tn.dwInfoFlags	= NIIF_INFO | NIIF_NOSOUND;
	tn.dwState = NIS_SHAREDICON;

	
	HRESULT ret;
	ret = ::Shell_NotifyIcon(NIM_DELETE, &tn);

	ret = ::Shell_NotifyIcon(NIM_ADD, &tn);

}

gxBool CDeviceManager::GetAchievement( Uint32 achieveindex )
{
	return gxTrue;
}

gxBool CDeviceManager::SetAchievement( Uint32 achieveindex )
{
	gxLib::SetToast("Achievement (%d)" , achieveindex );

	return gxTrue;
}


void CDeviceManager::OpenWebClient( gxChar* pString )
{
    //WebViewを表示するリクエストを発行する

    //sprintf( CAndroid::GetInstance()->m_WebViewURLString,"%s" , pString );
	CWindows::GetInstance()->ExecuteApp( pString );

}

