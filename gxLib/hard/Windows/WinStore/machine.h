﻿// --------------------------------------------------------------------
//
// つじつまあわせ用共通ヘッダ
//
// --------------------------------------------------------------------

#define FILE_FROM_ZIP
#define _USE_OPENAL









#define PLATFORM_WINSTORE
#define TARGET_OS_WINDOWS




#ifdef WIN32
	#define GX_BUILD_OPTIONx86
#else
	#define GX_BUILD_OPTIONx64
#endif

#ifdef _USE_OPENAL
	#define CAudio COpenAL
#else
	#define CAudio CXAudio
#endif

#define BUILD_COMPILER_VISUALSTUDIO

#define WIN32_LEAN_AND_MEAN             // Windows ヘッダーから使用されていない部分を除外します。
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <windowsx.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <locale.h>

#include <ppltasks.h>

#include <dxgi1_4.h>
#include <d3d11_3.h>
#include <DirectXMath.h>

#include <string>
#include <iostream>
#include <map>
#include <vector>
#include <functional>
#include <cstdlib>
#include <chrono>

using namespace DirectX;
using namespace Microsoft::WRL;

using namespace concurrency;
using namespace Windows::ApplicationModel;
using namespace Windows::ApplicationModel::Core;
using namespace Windows::ApplicationModel::Activation;
using namespace Windows::UI::Core;
using namespace Windows::UI::Input;
using namespace Windows::System;
using namespace Windows::Foundation;
using namespace Windows::Graphics::Display;
using namespace Windows::Storage;
using namespace Windows::Storage::Streams;
using namespace Platform;

//----------------------------------------------------
//プラットフォーム専用関数
//----------------------------------------------------
void Movie();
Uint32 GetDebugTime();
Uint32 GetVsyncRate();
void ExecuteApp( char *appname );
gxBool IsFullScreen();
void ChangeWindowMode( gxBool bWindow );
void ScreenCapture();

//typedef struct StCustomVertex
//{
//	Float32 x,y,z,rhw;
//	Float32 r,g,b,a;		//	Float32 a,r,g,b;
//	Float32 u,v;
//
//	Float32 sx,sy;
//	Float32 cx,cy;
//	Float32 rot;
//	Float32 fx,fy;			//flip
//	Float32 r2,g2,b2,a2;	//bllend
//
//} StCustomVertex;

//class CCommandList
//{
//public:
//	Uint32 eCommand;
//	Uint32 arg[4];
//	Sint32 x,y;
//	void*  pString;
//	Float32 opt;
//private:
//
//};


class CWindows
{
	//Windowsデバイス間での制御用クラス

public:

	enum eSamplingFilter {
		enSamplingNearest,
		enSamplingBiLenear,
	};

	CWindows()
	{
		m_bFullScreen    = gxFalse;
		m_bSoundEnable   = true;
		m_SamplingFilter = enSamplingNearest;
		m_bVirtualPad    = gxFalse;
	}

	~CWindows()
	{

	}

	void SetWindow( CoreWindow^ window )
	{
		m_Window = window;
	}

	CoreWindow^ GetWindow()
	{
		return m_Window;
	}

	void SetFullScreen( gxBool bFullScreen )
	{
		m_bFullScreen = bFullScreen;
	}

	gxBool IsFullScreen()
	{
		return m_bFullScreen;
	}

	void SetSoundEnable( gxBool bSoundEnable )
	{
		m_bSoundEnable = bSoundEnable;
	}

	gxBool IsSoundEnable()
	{
		return m_bSoundEnable;
	}

	eSamplingFilter GetRenderingFilter()
	{
		return m_SamplingFilter;
	}

	void  SetRenderingFilter( eSamplingFilter Filter )
	{
		m_SamplingFilter = Filter;
	}

	Windows::UI::Core::CoreWindow^ m_Window;
	//Platform::Agile<Windows::UI::Core::CoreWindow> m_Window;
	LONGLONG Vsyncrate;

	gxBool m_bFullScreen;
	gxBool m_bSoundEnable;
	gxBool m_bVirtualPad;

private:

	SINGLETON_DECLARE( CWindows );

	//サンプリングモード
	eSamplingFilter m_SamplingFilter;

};


