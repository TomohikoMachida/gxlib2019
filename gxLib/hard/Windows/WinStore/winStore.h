﻿#ifndef _WINSTORE_H_
#define _WINSTORE_H_

// アプリのメイン エントリ ポイントです。Windows シェルでアプリを接続し、アプリケーション ライフサイクル イベントを処理します。
ref class App sealed : public Windows::ApplicationModel::Core::IFrameworkView
{
public:
	App();

	// IFrameworkView メソッド。
	virtual void Initialize(Windows::ApplicationModel::Core::CoreApplicationView^ applicationView);
	virtual void SetWindow(Windows::UI::Core::CoreWindow^ window);
	virtual void Load(Platform::String^ entryPoint);
	virtual void Run();
	virtual void Uninitialize();

	void OnKeyDown      ( Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ args );
	void OnKeyUp        ( Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ args );
	void OnPointerMoved ( Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::PointerEventArgs^ args );
	void OnPointerPressed  ( Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::PointerEventArgs^ args );
	void OnPointerReleased ( Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::PointerEventArgs^ args );

protected:
	// アプリケーション ライフサイクル イベント ハンドラー。
	void OnActivated(Windows::ApplicationModel::Core::CoreApplicationView^ applicationView, Windows::ApplicationModel::Activation::IActivatedEventArgs^ args);
	void OnSuspending(Platform::Object^ sender, Windows::ApplicationModel::SuspendingEventArgs^ args);
	void OnResuming(Platform::Object^ sender, Platform::Object^ args);

	// ウィンドウ イベント ハンドラー。
	void OnWindowSizeChanged(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::WindowSizeChangedEventArgs^ args);
	void OnVisibilityChanged(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::VisibilityChangedEventArgs^ args);
	void OnWindowClosed(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::CoreWindowEventArgs^ args);

	// DisplayInformation イベント ハンドラー。
	void OnDpiChanged(Windows::Graphics::Display::DisplayInformation^ sender, Platform::Object^ args);
	void OnOrientationChanged(Windows::Graphics::Display::DisplayInformation^ sender, Platform::Object^ args);
	void OnDisplayContentsInvalidated(Windows::Graphics::Display::DisplayInformation^ sender, Platform::Object^ args);

private:
	//std::shared_ptr<DX::DeviceResources> m_deviceResources;
	//std::unique_ptr<DX11AppBaseMain> m_main;
	bool m_windowClosed;
	bool m_windowVisible;

};

//inline void ThrowIfFailed(HRESULT hr);
/*
{
	if (FAILED(hr))
	{
		// Win32 API エラーをキャッチするためのブレークポイントをこの行に設定します。
		throw Platform::Exception::CreateException(hr);
	}
}
*/


ref class AppSystem sealed : Windows::ApplicationModel::Core::IFrameworkViewSource
{
public:
	virtual Windows::ApplicationModel::Core::IFrameworkView^ CreateView();
};

#endif

