﻿#ifndef _CMEMORY_H_
#define _CMEMORY_H_

#define GAME_HEAP_SIZE (1024ull*1024*MAX_RAM_MB)

typedef struct StRAMHead {
	Uint32 index;
	size_t size;
	Uint8  level;
	Uint8  id[2];
	Uint8  dummy[1];
} StRAMHead;

enum {
	enMaxRam = 1024*1024,
};

void MemoryInit();
void MemoryDestroy( Sint8 level = -1 );

void UpdateMemoryStatus( Uint32* uNow , Uint32* uTotal , Uint32* uMax );

void* operator new( size_t size );
void* operator new[]( size_t size );
void  operator delete( void* ptr );
void  operator delete[]( void* ptr );
void* REALLOC( void* p , size_t sz );

#endif
