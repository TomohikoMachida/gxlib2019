#ifndef _COpenGLES2_H_
#define _COpenGLES2_H_

#ifdef PLATFORM_ANDROID
	#include <jni.h>
	#include <android/log.h>

	#include <EGL/egl.h>

	#if __ANDROID_API__ >= 24
	#include <GLES3/gl32.h>
	#elif __ANDROID_API__ >= 21
	#include <GLES3/gl31.h>
	#else
	#include <GLES3/gl3.h>
	#endif
#else
	//#include <GLES2/gl2.h>
	//#include <GLES2/gl2ext.h>
	#import <OpenGLES/ES3/gl.h>
	#import <OpenGLES/ES3/glext.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <gxLib/gxTexManager.h>

// 頂点シェーダーへの頂点ごとのデータの送信に使用します。
typedef struct StXMFLOAT4 {
	StXMFLOAT4( Float32 _x ,Float32 _y ,Float32 _z ,Float32 _w )
	{
		x = _x;
		y = _y;
		z = _z;
		w = _w;
	}
	Float32 x,y,z,w;
} StXMFLOAT4;

typedef struct StXMFLOAT2 {
	StXMFLOAT2( Float32 _x, Float32 _y )
	{
		x = _x;
		y = _y;
	}
	Float32 x,y;
} StXMFLOAT2;

typedef struct StXMVECTORF32 {
	Float32 a,r,g,b;
} StXMVECTORF32;

struct StPointLightInfo
{
	StPointLightInfo()
	{
	}

	Float32 plight_pos[3] = { 0.f , 0.f , 1.0f };
	Float32 plight_rgb[3] = { 1.0f , 1.0f , 1.0f };
	Float32 plight_intensity = 1.0f;
};

struct VertexPositionColorTexCoord
{
	StXMFLOAT4 pos;
	StXMFLOAT4 argb;
	StXMFLOAT2 uv;
	StXMFLOAT2 uvNormal;

	StXMFLOAT2 scale;
	StXMFLOAT2 offset;
	Float32    rot;
	StXMFLOAT2 flip;
	StXMFLOAT4 blend;

	StPointLightInfo pointlight;
    Float32 option[4];
};

typedef struct ConstantBufferForView3D
{
	Float32 pos[4];
} ConstantBufferForView3D;

typedef struct StViewPort {														// テクスチャテータ定義

	StViewPort()
	{
		TopLeftX = 0;
		TopLeftY = 0;
		Width = 640;
		Height = 480;
	}
	Float32 TopLeftX;
	Float32 TopLeftY;
	Float32 Width;
	Float32 Height;

} StViewPort;

struct TextureData {														// テクスチャテータ定義

	TextureData()
	{
		texture2D = 0;
		frameBufferObject = -1;
		format = GL_RGBA;
//		shaderResourceView = NULL;
//		renderTargetView = NULL;
	}

	GLuint		texture2D;				// テクスチャ本体
	GLuint		frameBufferObject;		// レンダーターゲットビュー
	StViewPort	ViewportSize;
	GLuint		format;
//	ID3D11ShaderResourceView *shaderResourceView;	// シェーダリソースビュー
};

//typedef struct StWindows_Foundation_Size {														// テクスチャテータ定義
//	StWindows_Foundation_Size()
//	{
//		Width = 640;
//		Height = 480;
//	}
//	Float32 Width;
//	Float32 Height;
//} StWindows_Foundation_Size;


// すべての DirectX デバイス リソースを制御します。
class COpenGLES2
{
	enum {
		enActiveTextureMax = 32,
		enAlbedoTextureMapSlot = 0,
		enNormalTextureMapSlot = 1,
		enCaptureTextureMapSlot = 2,
	};

	enum ETypeShader {
		enShaderDefault,
		enShaderBloom,
		enShaderBlur,
		enShaderRaster,
		enShaderNormal,
        enShaderFont,
        enShaderDev,
		enShaderMax,
	};

	enum {
		enBasicBuff,		//ノンテクスチャポリゴン用
		enWallPaper,		//256x256の固定壁紙
//		enNormalMap,		//法線マップ用
//		enPalletMap,		//パレット用

		enCaptureScreen,	//キャプチャ用
		enGameScreen0,		//Temporary
		enGameScreen1,		//WindowW x WindowHのゲーム画面
		enGameScreen2,		//WindowW x WindowHのゲーム画面

		//フレームバッファ
		enGamePostProcess0,	//WindowW x WindowHのゲーム画面
		enGamePostProcess1,	//WindowW/2 x WindowH/2のゲーム画面
		enGamePostProcess2,	//WindowW/2 x WindowH/2のゲーム画面
		enFrameBufferNum,
	};

	enum {
		//頂点バッファーと、インデックスバッファーのサイズ、１頂点36bytes => １三角 = 108bytes => 1MB = 9709ポリゴン
		enVertexBufferSize = 1024*1024*32,	//(MB)
		enIndexBufferSize  = 1024*1024*8,	//(MB)
		enTexturePageMax   = MAX_MASTERTEX_NUM,
		enSamplerMax = 2,
	};

	Float32 m_GameW = WINDOW_W;
	Float32 m_GameH = WINDOW_H;

public:
	COpenGLES2();
	~COpenGLES2();

	void Reset();

	void ReadTexture( int texPage  );

	void Present();

	void Init();
	void Update();
	void Render();

	void GetFrameBufferImage(gxChar* pLength);

	SINGLETON_DECLARE( COpenGLES2 );

private:

	void init();
	void createDevice();
	void reset();

	GLuint compileShader( GLchar const* const* vtxShader , size_t sz_vtx , GLchar const* const* pxlShader ,size_t sz_pxl );

	void render();
	//void MakeSwapChane();
	void initShader();
	void initTexture();
	void initOffScreen();
	void initVBO();
	void createFrameBuffer( TextureData *pTextureData );

	void renderGameObject( Sint32 start , Sint32 max );
	void renderSystem();

	void configTextureSampling();
	void configBlendState();

	gxBool makeTexture( TextureData *pTexture , int w =2048, int h=2048 , int bitDepth=32 , Uint8 *pData=NULL , Uint32 uSize=0 );

	void makeWallPaper();

	void changeShader( ETypeShader sheaderIndex=enShaderDefault , Float32 *pFloatValue = NULL );

#if 0
	HGLRC m_hRC;
#endif
	TextureData m_TextureData[ enTexturePageMax ];
	TextureData m_OffScreenTexture[ enFrameBufferNum ];

	// デバイス リソースへのキャッシュされたポインター。

	StViewPort m_BackBufferSize;

	//GLuint m_shaderProgram;
	//GLuint m_shaderProgramBloom;
	//GLuint m_shaderProgramBlur;
	//GLuint m_shaderProgramTest;

	GLuint m_vertexBufferObject;
	GLuint m_indexBufferObject;
//	GLuint m_vertexShader;
//	GLuint m_pixelShader;
	Uint8 *m_pVertexBuffer;
	Uint8 *m_pIndexBuffer;
	ConstantBufferForView3D m_ConstBuffer3dView;

	enum {
		enBlendTypeDefault,
		enBlendTypeAdd,
		enBlendTypeSub,
		enBlendTypeCross,
		enBlendTypeReverse,
		enBlendTypeXor,
		enBlendTypeScreen,
		enBlendMax,
	};

	GLuint m_ShaderProg[ enShaderMax ];

	GLuint m_ShadingWindowW;
	GLuint m_ShadingWindowH;

	//各種ステート

	// テクスチャ用のサンプラー
//	ID3D11SamplerState *m_SamplerState[enSamplerMax];
//	ID3D11BlendState	*m_pBlendState[enBlendMax];
//	ID3D11RasterizerState* m_pRasterizerState;

	gxBool m_bUpConvert;
	gxBool m_bInitCompleted;

	gxBool m_b3DView;

	Uint32 m_CurrentPage = 0;
	Uint32 m_PageBackBuffer  = 0;

	gxBool m_bScissorEnable = gxFalse;
	gxRect m_Scissor={};

    Sint32 m_FrameBufferIndex = -1;
};

#endif

