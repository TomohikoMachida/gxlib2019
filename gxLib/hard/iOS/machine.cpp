//------------------------------------------------------------
//
// machine.cpp
// マシン固有のデバイスへのアクセスを行うものはここに書く
// ハードウェアに依存する部分
//
//------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gx.h>
#include "COpenGLES2.h"
#include "COpenAL.h"
#include "CGamePad.h"
#include "CMemory.h"
#include "appMain.h"

SINGLETON_DECLARE_INSTANCE( CDeviceManager );

CDeviceManager::CDeviceManager()
{
}


CDeviceManager::~CDeviceManager()
{
}


void CDeviceManager::AppInit()
{
	static int nnn = 0;
	if( nnn == 0 )
	{
		CGameGirl::GetInstance()->Init();
        COpenGLES2::GetInstance()->Init();
        COpenAL::GetInstance()->Init();
	    CGamePad::GetInstance()->Init();
        MemoryInit();
	} else{
        CGameGirl::GetInstance()->SetResume();
    }

	CGameGirl::GetInstance()->AdjustScreenResolution();

	nnn ++;
}


void   CDeviceManager::GameInit()
{
    CGamePad::GetInstance()->Action();

}


gxBool CDeviceManager::GameUpdate()
{
/*
    static gxBool m_bGameInit = gxFalse;
	gxBool bExist = gxTrue;

	if( !m_bGameInit )
	{
		bExist = ::GameInit();
		m_bGameInit = gxTrue;
	}

	if( bExist )
	{
		//ゲームメインへ
		bExist = ::GameMain();
	}

	if( !bExist ) return gxFalse;
*/
	return gxTrue;
}


gxBool CDeviceManager::GamePause()
{
	return ::GamePause();
}


void   CDeviceManager::Render()
{
	if( CGameGirl::GetInstance()->IsResume() ) return;
    COpenGLES2::GetInstance()->Update();
    COpenGLES2::GetInstance()->Render();
}


/*
Float32 getFrameTime()
{
	static Uint64 oldCount = 0;
	static Uint64 freq = 0;

	static Sint64 current = 3;

	timespec ts;
	clock_gettime( CLOCK_MONOTONIC, &ts );

    if( current > 0 )
    {
        current  --;
        return 0.0f;
    }

	if (oldCount == 0 )
	{
		oldCount = ts.tv_sec*1000 + ts.tv_nsec/1000000;
	}

	Uint64 now = ts.tv_sec*1000 + ts.tv_nsec/1000000;
	Uint64 sa = ( now - oldCount );

	Float32 fElapsedTime = (Float32)(sa/1000.0f);

	return fElapsedTime;
}


 void   CDeviceManager::vSync()
{
	static Float32 s_fOld = getFrameTime();
	Float32 fps = FRAME_PER_SECOND;

	while ( gxTrue )
	{
		Float32 fNow = getFrameTime();;

		if( fNow >= s_fOld + ( 1.0f / fps  ) )
		{
			s_fOld = fNow;
			break;
		}
	}

}
*/
void CDeviceManager::vSync()
{
    //1/60秒の同期待ち
    static Float32 _TimeOld = gxLib::GetTime();
    
    Float32 _TimeNow;
    do
    {
        _TimeNow = gxLib::GetTime();
    }
    while( _TimeNow < ( _TimeOld + (1.0f/ FRAME_PER_SECOND) ) );
    
    _TimeOld = _TimeNow;
    
}


void   CDeviceManager::Flip()
{
    COpenGLES2::GetInstance()->Present();
}


void   CDeviceManager::Resume()
{
    //COpenGLES2::GetInstance()->Init();
}


void   CDeviceManager::Movie()
{
}


void   CDeviceManager::Play()
{
    COpenAL::GetInstance()->Action();
}


gxBool CDeviceManager::NetWork()
{
	return gxTrue;
}



void   CDeviceManager::UploadTexture(Sint32 sBank)
{
    COpenGLES2::GetInstance()->ReadTexture( sBank );
}

void   CDeviceManager::LogDisp(char* pString)
{
    printf("%s\n",pString);
}


void CDeviceManager::Clock( gxLib::Clock *pClock )
{
    //現在の時刻をミリ秒で取得する
    std::chrono::system_clock::time_point now;
    now = std::chrono::system_clock::now();
    time_t tt = std::chrono::system_clock::to_time_t(now);
    tm local_tm = *localtime(&tt);
    
    std::chrono::system_clock::duration tp = now.time_since_epoch();
    std::chrono::microseconds us = std::chrono::duration_cast<std::chrono::microseconds>(tp);
    
    pClock->Year  = local_tm.tm_year + 1900;    // years since 1900
    pClock->Month = local_tm.tm_mon + 1;        // months since January - [0, 11]
    pClock->Day   = local_tm.tm_mday;           // day of the month - [1, 31]
    pClock->DOW   = local_tm.tm_wday;             // days since Sunday - [0, 6]
    pClock->Hour  = local_tm.tm_hour;             // hours since midnight - [0, 23]
    pClock->Min   = local_tm.tm_min;             // minutes after the hour - [0, 59]
    pClock->Sec   = local_tm.tm_sec;             // seconds after the minute - [0, 60] including leap second
    pClock->MSec = (us.count()/1000) % (1000);
    pClock->USec = us.count() % (1000);
    
}



Uint8* CDeviceManager::LoadFile( const gxChar* pFileName , Uint32* pLength , Uint32 uLocation )
{
    const char* fname = (const char*)pFileName;
    char         path[2048];

    Uint8* pData = nullptr;

    if( uLocation == STORAGE_LOCATION_ROM )
    {
        //APP/ASSETS/
        GetResourcePath(path, sizeof path);
        strcat(path, "01_rom/");
    }
    else if( uLocation == STORAGE_LOCATION_INTERNAL)
    {
        //loadStorage
        
        //lib-disc
        GetLibraryPath(path, 1024);
        strcat(path, "/03_disk/");
        strcat(path, fname);
        pData = (Uint8*)_loadFile(path, (size_t*)pLength);
        if( pData == nullptr )
        {
            //res-disc
            GetResourcePath(path, sizeof path);
            strcat(path, "/02_disc/");
            strcat(path, fname);
            pData = (Uint8*)_loadFile(path, (size_t*)pLength);
            if( pData == nullptr )
            {
                //res-rom
                GetResourcePath(path, sizeof path);
                strcat(path, "/01_rom/");
                strcat(path, fname);
                pData = (Uint8*)_loadFile(path, (size_t*)pLength);
            }
        }
    }
    else if( uLocation == STORAGE_LOCATION_EXTERNAL)
    {
        //loadFile

        //DOCUMENTS/
        GetDocumentPath(path, 1024);
        strcat(path, "/");
        strcat(path, fname);
        pData = (Uint8*)_loadFile(path, (size_t*)pLength);

    }
/*
    else if( uLocation == STORAGE_LOCATION_DIRECT )
    {
        //DOCUMENTS/
        GetDocumentPath(path, 1024);
        strcat(path, "/");
    }
    else if( uLocation == STORAGE_LOCATION_CARD )
    {
        //DOCUMENTS/
        GetDocumentPath(path, 1024);
        strcat(path, "/");
    }
    strcat(path, fname);
    Uint8* ret = (Uint8*)_loadFile(path, (size_t*)pLength);
*/
    
    return (Uint8*)pData;
}


gxBool CDeviceManager::SaveFile( const gxChar* pFileName , Uint8* pReadBuf , Uint32 uSize , Uint32 uLocation )
{
    const char* fname = (const char*)pFileName;
    char         path[1024];

    if( uLocation == STORAGE_LOCATION_ROM )
    {
        return gxFalse;
    }
    else if( uLocation == STORAGE_LOCATION_INTERNAL)
    {
        //SaveStorageFile
        GetLibraryPath(path, 1024);
        strcat(path, "/03_disk/");
        strcat(path, fname);
        _saveFile(path, pReadBuf, uSize);
    }
    else if( uLocation == STORAGE_LOCATION_EXTERNAL)
    {
        //saveFile
        //document
        GetDocumentPath(path, 1024);
        strcat(path, "/");
        strcat(path, fname);
        _saveFile(path, pReadBuf, uSize);
    }
/*
    else if( uLocation == STORAGE_LOCATION_CARD )
    {
        //DOCUMENTS/
        GetDocumentPath(path, 1024);
        strcat(path, "/");
    }
    else if( uLocation == STORAGE_LOCATION_DIRECT )
    {
        //DOCUMENTS/
        GetDocumentPath(path, 1024);
        strcat(path, "/");
    }
*/

    return gxTrue;
}



#if 0
gxBool CDeviceManager::LoadConfig()
{
    //コンフィグファイルをロードする
    
    char buf[1024];
    sprintf( buf , "%s", FILENAME_CONFIG );
    
    //-------------------------------------------------------------------
    
    Uint8 *pData = NULL;
    Uint32 uSize = 0;
    
    pData = LoadFile( buf , &uSize , STORAGE_LOCATION_CARD );
    
    if( pData )
    {
        gxUtil::MemCpy( &gxLib::SaveData , pData , sizeof(gxLib::StSaveData) );
    }
    else
    {
        return gxFalse;
    }
    
    SAFE_DELETES( pData );
    
    return gxTrue;
}


gxBool CDeviceManager::SaveConfig()
{
    char buf[1024];
    sprintf( buf , "%s", FILENAME_CONFIG );
    
    SaveFile( buf , (Uint8*)&gxLib::SaveData , sizeof(gxLib::StSaveData) , STORAGE_LOCATION_CARD );
    
    return gxTrue;
}
#endif


void CDeviceManager::ToastDisp( gxChar* pMessage )
{
}


void CDeviceManager::OpenWebClient( gxChar* pURL )
{
}



gxBool CDeviceManager::SetAchievement( Uint32 index )
{
	return gxTrue;
}


gxBool CDeviceManager::GetAchievement( Uint32 index )
{
	return gxTrue;
}



//特殊
static pthread_t test_thread;

void   CDeviceManager::MakeThread( void* (*pFunc)(void*) , void * pArg )
{
	pthread_create( &test_thread, NULL, pFunc, (void *)pArg);
}


void   CDeviceManager::Sleep( Uint32 msec )
{
	struct timespec	 req, res;
	req.tv_sec  = 0;
	req.tv_nsec = msec * 1000000;

	nanosleep(&req, &res);
}


gxBool CDeviceManager::PadConfig( Sint32 padNo , Uint32 button )
{
	return gxTrue;
}



void CDeviceManager::UpdateMemoryStatus(Uint32* uNow, Uint32* uTotal, Uint32* uMax)
{
	Uint32 uNowByte;
	Uint32 uTotalByte;
	Uint32 uMaxByte;

	::UpdateMemoryStatus(&uNowByte, &uTotalByte, &uMaxByte);

	*uNow = (uNowByte >> 10) >> 10;
	*uTotal = (uTotalByte >> 10) >> 10;
	*uMax = (uMaxByte >> 10) >> 10;
}


wchar_t* CDeviceManager::UTF8toUTF16( gxChar*  pString  , size_t* pSize )
{
    setlocale(LC_ALL, "JPN");
    static wchar_t destBuf[FILENAMEBUF_LENGTH];

    mbstowcs(destBuf, pString, FILENAMEBUF_LENGTH );
    size_t len = wcslen(destBuf);
    if(pSize)
    {
        *pSize = len;
    }
    return destBuf;
}

#if 0
/*

unsigned long utf8ToUtf32(unsigned char* input, int* bytesInSequence)
{

    unsigned char c1, c2, c3, c4, c5, c6;

    *bytesInSequence = 1;
    if (!input)
    {

        return 0;
    }

    //0xxxxxxx (ASCII)      7bit
    c1 = input[0];
    if ((c1 & 0x80) == 0x00)
    {
        return c1;
    }

    //10xxxxxx              high-order byte
    if ((c1 & 0xc0) == 0x80)
    {
        return 0;
    }

    //0xFE or 0xFF          BOM (not utf-8)
    if (c1 == 0xfe || c1 == 0xFF)
    {
        return 0;
    }

    //110AAAAA 10BBBBBB     5+6bit=11bit
    c2 = input[1];
    if (((c1 & 0xe0) == 0xc0) &&
        ((c2 & 0xc0) == 0x80))
    {
        *bytesInSequence = 2;


        return ((c1 & 0x1f) << 6) | (c2 & 0x3f);
    }

    //1110AAAA 10BBBBBB 10CCCCCC        4+6*2bit=16bit
    c3 = input[2];
    if (((c1 & 0xf0) == 0xe0) &&
        ((c2 & 0xc0) == 0x80) &&
        ((c3 & 0xc0) == 0x80))
    {
        *bytesInSequence = 3;
        return ((c1 & 0x0f) << 12) | ((c2 & 0x3f) << 6) | (c3 & 0x3f);
    }

    //1111 0AAA 10BBBBBB 10CCCCCC 10DDDDDD      3+6*3bit=21bit
    c4 = input[3];
    if (((c1 & 0xf8) == 0xf0) &&
        ((c2 & 0xc0) == 0x80) &&
        ((c3 & 0xc0) == 0x80) &&
        ((c4 & 0xc0) == 0x80))
    {
        *bytesInSequence = 4;
        return ((c1 & 0x07) << 18) | ((c2 & 0x3f) << 12) | ((c3 & 0x3f) << 6) | (c4 & 0x3f);
    }

    //1111 00AA 10BBBBBB 10CCCCCC 10DDDDDD 10EEEEEE     2+6*4bit=26bit
    c5 = input[4];
    if (((c1 & 0xfc) == 0xf0) &&
        ((c2 & 0xc0) == 0x80) &&
        ((c3 & 0xc0) == 0x80) &&
        ((c4 & 0xc0) == 0x80) &&
        ((c5 & 0xc0) == 0x80))
    {
        *bytesInSequence = 4;
        return ((c1 & 0x03) << 24) | ((c2 & 0x3f) << 18) | ((c3 & 0x3f) << 12) | ((c4 & 0x3f) << 6) | (c5 & 0x3f);
    }

    //1111 000A 10BBBBBB 10CCCCCC 10DDDDDD 10EEEEEE 10FFFFFF        1+6*5bit=31bit
    c6 = input[5];
    if (((c1 & 0xfe) == 0xf0) &&
        ((c2 & 0xc0) == 0x80) &&
        ((c3 & 0xc0) == 0x80) &&
        ((c4 & 0xc0) == 0x80) &&
        ((c5 & 0xc0) == 0x80) &&
        ((c6 & 0xc0) == 0x80))
    {
        *bytesInSequence = 4;
        return ((c1 & 0x01) << 30) | ((c2 & 0x3f) << 24) | ((c3 & 0x3f) << 18) | ((c4 & 0x3f) << 12) | ((c5 & 0x3f) << 6) | (c6 & 0x3f);
    }

    return 0;
}

gxChar* CDeviceManager::UTF8toUTF32(gxChar* pString, size_t* pSize)
{

    //Uint8->Uint32に変換する

    size_t u8Len = strlen(pString);
    size_t maxSize = (u8Len+1) * 4;
    Uint8* pUTF32Buf = new Uint8[maxSize];

    int n = 0;
    int num = 0;
    Uint32 utf32Code = 0;

    Sint32 ii = 0;
    for ( ii = 0; ii < 100; ii++)
    {
        utf32Code = utf8ToUtf32((Uint8*)&pString[n], &num);
        n += num;

        Uint32* p = (Uint32*)&pUTF32Buf[ii * 4];
        *p = utf32Code;

        if (utf32Code == 0x00000000) break;

    }
    if (pSize)
    {
        *pSize = ii*4;
    }


    return (gxChar*)pUTF32Buf;
}
 */
#endif
/*
gxChar*  CDeviceManager::UTF8toUTF32(gxChar* pString, size_t* pSize)
{
	setlocale(LC_ALL, "JPN");
	static wchar_t destBuf[FILENAMEBUF_LENGTH];

	mbstowcs(destBuf, pString, FILENAMEBUF_LENGTH );

    size_t len = wcslen(destBuf);
    if(pSize)
    {
        *pSize = len*4;
    }

    return (gxChar*)destBuf;
}
*/

wchar_t* CDeviceManager::SJIStoUTF16( gxChar*  pString  , size_t* pSize )
{
	return NULL;
}


gxChar*  CDeviceManager::UTF16toUTF8( wchar_t* pUTF16buf, size_t* pSize )
{
	setlocale(LC_ALL, "JPN");
	static char destBuf[FILENAMEBUF_LENGTH];

	wcstombs( destBuf, pUTF16buf , FILENAMEBUF_LENGTH );

	return destBuf;
}


gxChar*  CDeviceManager::UTF16toSJIS( wchar_t* pUTF16buf, size_t* pSize )
{
	return NULL;
}


gxChar*  CDeviceManager::UTF8toSJIS ( gxChar*  pUTF8buf , size_t* pSize )
{
	return NULL;
}


gxChar*  CDeviceManager::SJIStoUTF8 ( gxChar*  pSJISbuf , size_t* pSize )
{
	return NULL;
}


