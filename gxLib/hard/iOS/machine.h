﻿// --------------------------------------------------------------------
//
// つじつまあわせ用共通ヘッダ
//
// --------------------------------------------------------------------

//#define FILE_FROM_ZIP
#define _USE_OPENGL
#define _USE_OPENAL

//---------------------------------------
//以下自動設定
//---------------------------------------

#define GX_BUILD_OPTIONx64



// ヘッダー ファイル:

#define WIN32_LEAN_AND_MEAN             // Windows ヘッダーから使用されていない部分を除外します。

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>

#include <stdlib.h>
#include <memory.h>
#include <stdarg.h>
#include <string.h>
#include <locale.h>

#include <cctype>
#include <string>
#include <iostream>
#include <map>
#include <vector>
#include <functional>
#include <cstdlib>
#include <chrono>

#pragma warning(disable : 4996)
#pragma warning(disable : 4819)
#pragma warning(disable : 4244)

class CiOS
{
public:
    enum {
        enSamplingNearest,
        enSamplingBiLenear,
    };
    
    CiOS()
    {
        //m_pJavaVM = NULL;
        //m_PackageName[0] = 0x00;
        
        //m_pOBBPath[0][0] = 0x00;
        //m_pOBBPath[1][0] = 0x00;
        //m_pSDPath[0] = 0x00;
        
        //m_WebViewURLString[0] = 0x00;
        
        //m_bCheckExtentionFile = gxFalse;
    }
#if 0
    void SetPackageName( const gxChar *pPackageName )
    {
        sprintf( m_PackageName , "%s" , pPackageName );
    }
    
    gxChar* GetPackageName()
    {
        return m_PackageName;
    }
    
    void SetOBB_Path( Uint32 index , gxChar *pOBBPath )
    {
        //------------------------------------------------------------
        //------------------------------------------------------------
        
        m_bCheckExtentionFile = gxTrue;
        
        if( pOBBPath == NULL )
        {
            m_pOBBPath[index][0] = 0x00;
        }
        else
        {
            sprintf( m_pOBBPath[index] , "%s" , pOBBPath );
        }
    }
    
    gxChar* GetOBB_Path( Uint32 index = 0 )
    {
        //------------------------------------------------------------
        //OBB
        //------------------------------------------------------------
        if( m_pOBBPath[index][0] == 0x00 ) return NULL;
        
        return &m_pOBBPath[index][0];
    }
    
    void SetSD_Path( Uint32 index , gxChar *pSDPath )
    {
        //------------------------------------------------------------
        //------------------------------------------------------------
        
        if( pSDPath == NULL )
        {
            m_pSDPath[0] = 0x00;
        }
        else
        {
            sprintf( m_pSDPath , "%s" , pSDPath );
        }
    }
    gxChar* GetSD_Path( Uint32 index = 0 )
    {
        //------------------------------------------------------------
        //OBB
        //------------------------------------------------------------
        if( m_pSDPath[index] == 0x00 ) return NULL;
        
        return &m_pSDPath[0];
    }
    
    gxBool IsCheckOBB()
    {
        return m_bCheckExtentionFile;
    }
    
    void Vibration( Sint32 sec );
    
    void SetAssetManager( AAssetManager *am )
    {
        m_pAssetManager = am;
    }
    
    void HttpRequest( Sint32 index , gxChar* url )
    {
        sprintf( m_HTTPURLString , "%s",url );
        m_HTTPURLIndex = index;
    }
#endif

    Uint32 GetRenderingFilter()
    {
        return m_RenderFilter;
    }
    
    static gxChar* GetClipBoardText();
    static void  SetClipBoardText( gxChar *pString );
    
    
//    JavaVM *m_pJavaVM;
//    gxChar m_WebViewURLString[1024]={0};
//    gxChar m_HTTPURLString[1024]={0};
//    Sint32 m_HTTPURLIndex = 0;
    SINGLETON_DECLARE( CiOS )
    
private:
    
//    gxChar m_PackageName[FILENAMEBUF_LENGTH];
//    gxChar m_pOBBPath[2][FILENAMEBUF_LENGTH];
//    gxChar m_pSDPath[FILENAMEBUF_LENGTH];
    
//    gxBool m_bCheckExtentionFile;
    
//    AAssetManager*  m_pAssetManager;
    
    Uint32 m_RenderFilter = enSamplingNearest;
};

