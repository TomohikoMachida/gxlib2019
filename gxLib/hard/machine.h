﻿// --------------------------------------------------------------------
//
// つじつまあわせ用共通ヘッダ
//
// --------------------------------------------------------------------
#ifndef _MACHINE_H_
#define _MACHINE_H_

#ifdef NDEBUG
	#define GX_RELEASE
#else
	#define GX_DEBUG
#endif

#define _USE_MULTITHREAD_

#ifdef PLATFORM_WINDOWS_DESKTOP
	#include "Windows/DirectX11/machine.h"

#elif defined PLATFORM_WINDOWS_STORE
	#include "Windows/WinStore/machine.h"

#elif defined PLATFORM_EXA
	#include "Windows/WinDesktop/machine.h"

#elif defined PLATFORM_ANDROID
	#include "AndroidStudio/machine.h"

#elif defined PLATFORM_IOS
    #include "iOS/machine.h"
#else
	#include "blank/machine.h"
#endif

#define GLOBAL_IP_ADDRESS_CHECK_URL "http://garuru.co.jp/api/gxLib.php?ope=GetIP"

//テクスチャキャッシュを使うか？
//同一テクスチャページに同一ファイル名のテクスチャが読み込まれたときに読み込みをスキップする
#define USE_TEXTURE_CACHE

//------------------------------------------------
//各プラットフォーム共通のラッピング関数
//------------------------------------------------

class CDeviceManager
{
public:

	CDeviceManager();
	virtual ~CDeviceManager();

	virtual void   AppInit();

	virtual void   GameInit();
	virtual gxBool GameUpdate();
	virtual gxBool GamePause();
	virtual void   Render();
	virtual void   vSync();
	virtual void   Flip();
	virtual void   Resume();
	virtual void   Movie();
	virtual void   Play();
	virtual gxBool NetWork();

	virtual void   UploadTexture(Sint32 sBank);
	virtual void   LogDisp(char* pString);
	virtual void   Clock( gxLib::Clock *pClock );

	virtual Uint8* LoadFile( const gxChar* pFileName , Uint32* pLength , Uint32 uLocation );
	virtual gxBool SaveFile( const gxChar* pFileName , Uint8* pReadBuf , Uint32 uSize , Uint32 uLocation );

	virtual void MakeThread( void* (*pFunc)(void*) , void * pArg=nullptr );
	virtual void Sleep( Uint32 msec );
	virtual gxBool PadConfig( Sint32 padNo , Uint32 button );

	void UpdateMemoryStatus(Uint32* uNow, Uint32* uTotal, Uint32* uMax);

	//ストレージへのファイルアクセス
	//virtual gxBool SaveStorageFile( const gxChar* pFileName , Uint8* pReadBuf , Uint32 uSize , Uint32 uLocation  );
	//virtual Uint8* LoadStorageFile( const gxChar* pFileName , Uint32* pLength );

/*
//	virtual gxBool LoadConfig();
//	virtual gxBool SaveConfig();
*/

	//特殊

	virtual void ToastDisp( gxChar* pMessage );
	virtual void OpenWebClient( gxChar* pURL );

	virtual gxBool SetAchievement( Uint32 index );
	virtual gxBool GetAchievement( Uint32 index );

    gxBool PadConfig();		//←同じ名前に注意


	static gxChar*  UTF8toUTF32(gxChar* pString, size_t* pSize = NULL);
	static gxChar*  UTF32toUTF8(gxChar* pString, size_t* pSize = NULL);

	static wchar_t* UTF8toUTF16( gxChar*  pString  , size_t* pSize = NULL );
	static gxChar*  UTF16toUTF8( wchar_t* pUTF16buf, size_t* pSize = NULL );

	static gxChar*  UTF16toSJIS( wchar_t* pUTF16buf, size_t* pSize = NULL );
	static wchar_t* SJIStoUTF16( gxChar*  pString  , size_t* pSize = NULL );
	static gxChar*  UTF8toSJIS ( gxChar*  pUTF8buf , size_t* pSize = NULL );
	static gxChar*  SJIStoUTF8 ( gxChar*  pSJISbuf , size_t* pSize = NULL );

	SINGLETON_DECLARE( CDeviceManager );

private:


};

//game Src側へのアクセス関数

gxBool GameInit();
gxBool GamePause();
gxBool GameMain();
gxBool GameSleep();
gxBool GameResume();
gxBool GameEnd();
gxBool GameReset();

#endif
