﻿#ifndef _CFILEZIP_H_
#define _CFILEZIP_H_

#include "miniz/miniz.h"
#include<algorithm>

class CFileZip
{
	typedef struct StSaveInfo
	{
		std::vector<Uint8> m_pSaveBuffer;
		size_t m_uSaveSize = 0;
	} StSaveInfo;

public:

	typedef struct StArcInfo {
		Uint32 fileSize    = 0x00;
		gxChar *pFileName1 = NULL;
		gxChar *pFileName2 = NULL;
		Uint8  *pFileData  = NULL;

		~StArcInfo()
		{
		}

		void Release()
		{
			SAFE_DELETES(pFileName1);
			SAFE_DELETES(pFileName2);
			SAFE_DELETES(pFileData);
		}

		void SetName(gxChar *pName)
		{
			SAFE_DELETES( pFileName1);
			SAFE_DELETES( pFileName2);

			size_t len = strlen(pName);

			//そのまんまのファイル名を記録
			pFileName1 = new gxChar[ len+1 ];
			sprintf(pFileName1, "%s", pName);

			//大文字ファイル名を記録
			pFileName2 = new gxChar[ len+1 ];
			sprintf(pFileName2, "%s", pName );
			gxUtil::StrUpr( pFileName2 );
		}

	} StArcInfo;

	CFileZip()
	{
		m_uFileSize   = 0;
		m_uFileNum    = 0;
	}

	~CFileZip()
	{
		if (m_pZArcLoad)
		{
			mz_zip_reader_end(m_pZArcLoad);
		}

		if (m_pZArcSave)
		{
			mz_zip_writer_end(m_pZArcSave);
		}

		//SAFE_DELETES( m_pFileInfo );
		SAFE_DELETES( m_pZArcLoad );
		SAFE_DELETES( m_pZArcSave );
		
		for (auto itr = m_FileInfo.begin(); itr != m_FileInfo.begin(); ++itr)
		{
			itr->Release();
		}

	}

	gxBool Load( gxChar *pFileName )
	{
		//zipを読み込んでファイルリストを作成する

		Uint8 *pData = NULL;
		Uint32 uSize = 0;

		pData = gxLib::LoadStorageFile( pFileName , &uSize );

		return Read( pData , uSize );
	}

	gxBool Read( Uint8 *Data , Uint32 uSize )
	{
		//zipを読み込んでファイルリストを作成する
		//pDataはmemcpyされないのでDecodeするまで呼び出し側でメモリを保持しておくこと

		if (m_pZArcLoad == NULL)
		{
			m_pZArcLoad = new mz_zip_archive;
			memset(m_pZArcLoad, 0, sizeof(mz_zip_archive));
		}

		if( mz_zip_reader_init_mem( m_pZArcLoad, Data , uSize , 0 ) )
		{
			m_uFileNum = mz_zip_reader_get_num_files(m_pZArcLoad);

			m_FileInfo.clear();

			for ( Uint32 ii = 0; ii < m_uFileNum; ii++)
			{
				mz_zip_archive_file_stat stat;
				if (mz_zip_reader_file_stat(m_pZArcLoad, ii, &stat))
				{
					StArcInfo info;
					info.SetName( stat.m_filename );
					info.fileSize = stat.m_uncomp_size;
					m_FileInfo.push_back(info);
				}
			}
		}
		else
		{
			return gxFalse;
		}
		return gxTrue;
	}

	Uint8* Decode( Uint32 index, gxChar *pFileName , Uint32* pLength)
	{
		m_FileInfo;

		if (index >= m_FileInfo.size())
		{
			pFileName[0] = 0x00;
			*pLength = 0x00;
			return NULL;
		}

		Uint8 *pData = Decode(m_FileInfo[index].pFileName1, pLength);

		sprintf(pFileName, "%s", m_FileInfo[index].pFileName1);

		return pData;
	}

	Uint8* Decode( gxChar *pFileName , Uint32* pLength )
	{
		//読み込んだZIPからファイルを取り出す
		//大文字小文字を判別しないようにした

		gxChar name[256];

		gxChar *pName = getRealFileName( pFileName );

		if( pName )
		{
			sprintf( name , "%s" , pName );
		}
		else
		{
			sprintf( name , "%s" , pFileName );
		}

		size_t extracted_size = 0;

		void *p;
		p = mz_zip_reader_extract_file_to_heap( m_pZArcLoad, name, &extracted_size, 0);

		*pLength = extracted_size;

		return (Uint8*)p;
	}


	gxBool AddFile(gxChar *pFileName, void *pData, size_t sz)
	{
		if (m_pZArcSave == NULL)
		{
			m_pZArcSave = new mz_zip_archive;
			memset(m_pZArcSave, 0, sizeof(mz_zip_archive));

			m_pZArcSave->m_pIO_opaque = this;
			m_pZArcSave->m_pWrite = &CFileZip::saveFunc;
			m_pZArcSave->m_pFree = &CFileZip::freeFunc;
			m_pZArcSave->m_pNeeds_keepalive = NULL;

			if (!mz_zip_writer_init_v2(m_pZArcSave, 0, MZ_ZIP_FLAG_CASE_SENSITIVE))
			{
				return gxFalse;
			}
		}

		mz_bool bSuccess = mz_zip_writer_add_mem(m_pZArcSave, pFileName, (const void *)pData, sz, MZ_DEFAULT_LEVEL);

		return bSuccess;
	}

	gxBool Save(gxChar *pFileName)
	{
		if (m_pZArcSave == NULL) return gxFalse;

		if (!mz_zip_writer_finalize_archive(m_pZArcSave))
		{
			return false;
		}

		size_t uSize = 0;
		Uint8* pData = GetSaveImage(&uSize);
		gxBool bResult = gxTrue;

		if (gxLib::SaveFile(pFileName, pData, uSize))
		{
			bResult = gxTrue;
		}

		for (auto itr = m_FileInfo.begin(); itr != m_FileInfo.begin(); ++itr)
		{
			SAFE_DELETES(itr->pFileName1);
			SAFE_DELETES(itr->pFileName2);
			SAFE_DELETES(itr->pFileData);
		}

		return bResult;
	}


	Uint32  GetFileNum()
	{
		return m_uFileNum;
	}

	std::vector<StArcInfo>* GetFileList()
	{
		return &m_FileInfo;
	}

	gxChar* GetFileName( Sint32 index , Uint32 *pSize = NULL )
	{
		StArcInfo *p = &m_FileInfo[ index ];

		if( p->pFileName1[0] != 0x00 )
		{
			*pSize = p->fileSize;
		}

		return p->pFileName1;
	}

	Uint32 GetFileSize( Sint32 index )
	{
		StArcInfo *p = &m_FileInfo[ index ];
		return p->fileSize;
	}


	Uint8* GetSaveImage(size_t *uSize)
	{
		*uSize = GetSaveInfo()->m_uSaveSize;

		return &GetSaveInfo()->m_pSaveBuffer[0];
	}

	StSaveInfo* GetSaveInfo()
	{
		return &SaveInfo;
	}

private:

	static void freeFunc(void *opaque, void *address)
	{
		SAFE_DELETES(address);
	}

	static size_t saveFunc(void *pOpaque, mz_uint64 file_ofs, const void *pBuf, size_t n)
	{
		CFileZip *p = (CFileZip*)pOpaque;
		StSaveInfo *q = p->GetSaveInfo();

		if (q->m_pSaveBuffer.size() < q->m_uSaveSize + n)
		{
			q->m_pSaveBuffer.resize(p->GetSaveInfo()->m_uSaveSize + n);
		}
		if (n > 0)
		{
			memcpy((Uint8*)&q->m_pSaveBuffer[q->m_uSaveSize], pBuf, n);
		}

		q->m_uSaveSize += n;

		return n;
	}

	gxChar *getRealFileName( gxChar *pFileName )
	{
		//大文字小文字を区別しないファイル名を返す

		static gxChar name[256];

		sprintf( name , "%s" , pFileName );
		gxUtil::StrUpr( name );

		for ( Uint32 ii = 0; ii < m_uFileNum; ii++)
		{
			//大文字同士のファイル名を比較して本当の名前を返す

			if( strcmp( m_FileInfo[ii].pFileName2 , name ) == 0 )
			{
				return m_FileInfo[ii].pFileName1;
			}
		}

		//sjisで再チャレンジ
		gxChar *pSJIS = CDeviceManager::UTF8toSJIS(pFileName);

		sprintf(name, "%s", pSJIS);
		gxUtil::StrUpr(name);

		for (Uint32 ii = 0; ii < m_uFileNum; ii++)
		{
			//本当のファイル名で比較する

			if (strcmp(m_FileInfo[ii].pFileName2, name) == 0)
			{
				return m_FileInfo[ii].pFileName1;
			}
		}

		SAFE_DELETE(pSJIS);

		return name;
	}


	Uint32 m_uFileSize   = 0;
	Uint32 m_uFileNum    = 0;

	std::vector<StArcInfo> m_FileInfo;
	mz_zip_archive* m_pZArcLoad = NULL;
	mz_zip_archive* m_pZArcSave = NULL;

	StSaveInfo SaveInfo;

};

#endif
