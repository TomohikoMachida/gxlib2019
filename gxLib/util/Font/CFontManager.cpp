//--------------------------------------------------------------------------
//
// CFontManager.cpp
// written bt tomi.
// 2019.04.14　更新
// 
//--------------------------------------------------------------------------

#include <gxLib.h>
#include <gxLib/Util/CFileTarga.h>
#include <gxLib/Util/ccsv.h>
#include <gxLib/gx.h>
#include "CFontManager.h"
#include "../CFileZip.h"

#include "fontTbl.cpp"

SINGLETON_DECLARE_INSTANCE( CFontManager );

CFontManager::CFontManager()
{
}

void CFontManager::Init()
{
	//m_fScale = 1.0f;
	//gxLib::SaveFile("utf8.zip" , ZipData, sizeof(ZipData) );

	//return;
	//MakeFontData();
	//return;

	m_FontSize =32;
	m_bOutLine = gxTrue;

	Float32 fColokStart = gxLib::GetTime();

	m_TexturePage = (MAX_MASTERTEX_NUM-3)*64;

	Uint32 uSize;
	Uint8 *pData;
	CFileZip zip;

	//zip.Load("fontU8.zip");
	zip.Read(ZipData, sizeof(ZipData));
	pData = zip.Decode("utf8.csv",&uSize);

	//gxLib::SaveFile("test.txt" , pData, uSize);

	CCsv csv;
	csv.ReadFile(pData,uSize);

	Sint32 max = csv.GetHeight();
	SInfo info;

	for (Sint32 ii = 0; ii < max; ii++)
	{
		Sint32  id;
		Sint32  pg;
		Sint32  u;
		Sint32  v;
		Sint32  w;
		Sint32  h;
		Sint32  ox;
		Sint32  oy;
		Sint32  xa;
		Sint32  ch;

		csv.GetCell(2, ii, id);
		csv.GetCell(4, ii, u);
		csv.GetCell(6, ii, v);
		csv.GetCell(8, ii, w);
		csv.GetCell(10, ii, h);
		csv.GetCell(12, ii, ox);
		csv.GetCell(14, ii, oy);
		csv.GetCell(16, ii, xa);
		csv.GetCell(18, ii, pg);
		csv.GetCell(20, ii, ch);

		info.id = id;
		info.pg = pg;;
		info.u  = u;
		info.v  = v;
		info.w  = w;
		info.h  = h;
		info.ox = ox;
		info.oy = oy;
		info.xa = xa;
		info.ch = ch;
		m_List[id] = info;

	}

	SAFE_DELETES(pData);

	pData = zip.Decode("utf8.tga",&uSize);
	gxLib::ReadTexture( (m_TexturePage+0)/* * 64*/, pData, uSize , 0x00000000);
	SAFE_DELETES(pData);
/*
	pData = zip.Decode("font_1.tga",&uSize);
	gxLib::ReadTexture( (m_TexturePage+1) * 64, pData, uSize , 0xff00ff00);
	SAFE_DELETES(pData);

	pData = zip.Decode("font_2.tga",&uSize);
	gxLib::ReadTexture( (m_TexturePage+2) * 64, pData, uSize , 0xff00ff00);
	SAFE_DELETES(pData);
*/

	gxLib::UploadTexture();

	Float32 fColokNow = gxLib::GetTime();
	Float32 benchTime = fColokNow - fColokStart;
	CGameGirl::GetInstance()->SetBenthmarkTime( benchTime );
	/*
		gxChar filePath[1024] = { 0 };
		gxChar fileName[1024] = { 0 };
		gxChar fileOnly[1024] = { 0 };
		gxChar temp[1024] = { 0 };

		gxUtil::GetFileNameWithoutPath("font.zip", fileName);
		gxUtil::GetFileNameWithoutExt(fileName, fileOnly);
		gxUtil::GetPath(textFile,filePath);

		gxLib::LoadTexture(0 * 64, "font_0.tga", 0xff00ff00);
		gxLib::LoadTexture(1 * 64, "font_1.tga", 0xff00ff00);
		gxLib::LoadTexture(2 * 64, "font_2.tga", 0xff00ff00);
		gxLib::UploadTexture();
		csv.LoadFile("font.csv");
	*/
}

void CFontManager::Load( Sint32 page , gxChar *pFileName , Sint32 fontSize )
{
	m_FontSize = fontSize;
	m_TexturePage = page;

	Uint32 uSize;
	Uint8* pData;
	CFileZip zip;

	zip.Load( pFileName );
	//zip.Read(ZipData, sizeof(ZipData));
	pData = zip.Decode("font.csv", &uSize);

	CCsv csv;
	csv.ReadFile(pData, uSize);

	Sint32 max = csv.GetHeight();
	SInfo info;

	for (Sint32 ii = 0; ii < max; ii++)
	{
		Sint32  id;
		Sint32  pg;
		Sint32  u;
		Sint32  v;
		Sint32  w;
		Sint32  h;
		Sint32  ox;
		Sint32  oy;
		Sint32  xa;
		Sint32  ch;

		csv.GetCell(2, ii, id);
		csv.GetCell(4, ii, u);
		csv.GetCell(6, ii, v);
		csv.GetCell(8, ii, w);
		csv.GetCell(10, ii, h);
		csv.GetCell(12, ii, ox);
		csv.GetCell(14, ii, oy);
		csv.GetCell(16, ii, xa);
		csv.GetCell(18, ii, pg);
		csv.GetCell(20, ii, ch);

		info.id = id;
		info.pg = pg;;
		info.u = u;
		info.v = v;
		info.w = w;
		info.h = h;
		info.ox = ox;
		info.oy = oy;
		info.xa = xa;
		info.ch = ch;
		m_List[id] = info;

	}

	SAFE_DELETES(pData);

	pData = zip.Decode("FontRGBA.tga", &uSize);
	gxLib::ReadTexture((m_TexturePage + 0), pData, uSize, 0xff00ff00);
	SAFE_DELETES(pData);

	gxLib::UploadTexture();

}


void CFontManager::MakeFontData()
{
	Uint32 uSize = 0;
	Uint8* pData;

	pData = gxLib::LoadFile("fontU8.zip",&uSize);

	gxChar* pText = new gxChar[uSize*8];
	Uint32 uLength = 0;

	uLength += sprintf( &pText[uLength], "Uint8 ZipData[]={\r\n");
	for (Uint32 ii = 0; ii < uSize; ii++)
	{
		if (ii % 16 == 0)
		{
			uLength += sprintf(&pText[uLength], "\r\n");
		}

		uLength += sprintf(&pText[uLength], "0x%02x,", pData[ii]);
	}

	uLength += sprintf(&pText[uLength], "};\r\n");

	gxLib::SaveFile("gxLib/util/font/fontTbl.cpp", (Uint8*)pText, uLength);

	//CFileText txt;
	//txt.AddLine("0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,");
	//txt.SaveFile("gxlib/util/font/fontTbl.cpp");
	SAFE_DELETES(pText);
	SAFE_DELETES(pData);

}
void CFontManager::calc(gxChar* pStr, size_t sz)
{
	//utf32で受け取った文字列から表示するのに必要な情報を集める
	Sint32 space = m_FontSize/10;

	//size_t len = wcslen(pStr);

	size_t len = sz / 4;

	m_Sprites.clear();
	m_LineWidth.clear();

	m_Width = 0;
	m_Height = 0;

	Sint32 x = 0, y = 0, z = 0, w = 0, h = 0;
	Sint32 lineHeight = 0;
	Sint32 ln = 0;

	for (size_t ii = 0; ii < len; ii++)
	{
		Uint32* q2 = (Uint32*)&pStr[ii * 4];
		Uint32 id = *q2;

		auto itr = m_List.find(id);

		if (itr == m_List.end())
		{
			if (id == '\n')
			{
				m_LineWidth.push_back(x);
				ln++;

				x = 0;
				y += lineHeight;
				m_Height += lineHeight;
				continue;
			}
			else
			{
				continue;
			}
		}

		if (id == '\t' || id == ' ' || id == 12288 /*　*/)
		{
			id = ' ';
			itr = m_List.find(id);
			x += 16;
			continue;
		}
		if (id == '\n')
		{
			m_LineWidth.push_back(x);
			ln++;

			x = 0;
			y += lineHeight;
			m_Height += lineHeight;
			continue;
		}

		SInfo info = itr->second;

		StSpriteEX sprex;
		sprex.lineIndex = ln;
		sprex.spr.pos = { x,y,0 };
		sprex.spr.albedo = {
			//				(info.pg+m_TexturePage) * 64,
							(m_TexturePage),// * 64,
							info.u,
							info.v,
							info.w,
							info.h,
							0,//-info.ox,
							0,//-info.oy,
		};

		w = info.w;// +info.ox;
		h = (info.h + /*info.oy +*/ space*4);

		if (id < 256 )
		{
			//半角か等幅指定の時は大きさを決め打ちにする
			w = (m_FontSize / 2 );// + space;
		}
		else if (m_bTouhaba)
		{
			w = m_FontSize + space;
		}
		else
		{
			w = w+space;
		}

		x += w;

		if (h > lineHeight) lineHeight = h;
		if (x > m_Width)    m_Width = x;

		sprex.spr.shader = gxShaderFont;
		sprex.spr.option[0] = info.pg;
		m_Sprites.push_back(sprex);
	}

	m_LineWidth.push_back(x);
	m_Height += lineHeight;
	ln++;

}

void CFontManager::draw( Sint32 ax , Sint32 ay, Sint32 prio , Uint32 atr , Uint32 argb , Float32 fRot , Float fScale )
{
    //実際の描画リクエストの発行

    if (atr & ATR_STR_RIGHT)
    {
        for (int ii = 0; ii < m_Sprites.size(); ii++)
        {
            auto p = &m_Sprites[ii];
            Sint32 w = m_LineWidth[p->lineIndex];
            p->spr.pos.x = m_Width - w + p->spr.pos.x - m_Width;

        }
    }
    else if (atr & ATR_STR_CENTER)
    {
        for (int ii = 0; ii < m_Sprites.size(); ii++)
        {
            auto p = &m_Sprites[ii];
            Sint32 w = m_LineWidth[p->lineIndex];
            p->spr.pos.x = m_Width/2 - w/2 + p->spr.pos.x- m_Width/2;
        }
    }

    if (atr & ATR_STR_MID)
    {
        for (int ii = 0; ii < m_Sprites.size(); ii++)
        {
            auto p = &m_Sprites[ii];
            Sint32 h = m_Height / 2;// m_LineWidth.size();
            p->spr.pos.y -= h;

        }
    }
    if (atr & ATR_STR_BOTTOM)
    {
        for (int ii = 0; ii < m_Sprites.size(); ii++)
        {
            auto p = &m_Sprites[ii];
            Sint32 h = m_Height;// / 2;// m_LineWidth.size();
            p->spr.pos.y -= h;

        }
    }

	//
	if( m_bOutLine )
	{
		Sint32 ox = 0;
		Sint32 oy = 0;
		Uint32 argb2 = 0x20FFFFFF;

		//argb2 = 0xFFFF0000;

		for (int ii = 0; ii < m_Sprites.size(); ii++)
	    {
	        for( Float32 fr=0.0f ; fr<360.0f; fr+=45.0f)
	        {
				ox = gxUtil::Cos(fr)*2.0f;
				oy = gxUtil::Sin(fr)*2.0f;
				drawFont( m_Sprites[ii] , ax+ox, ay+oy, prio, atr, argb2, fRot, m_fScale );
	        }

	    }
	}


    for (int ii = 0; ii < m_Sprites.size(); ii++)
    {
		drawFont( m_Sprites[ii] , ax, ay, prio, atr, argb, fRot, m_fScale );
    }


/*
        //outline
        gxLib::StSprite spr = m_Sprites[ii].spr;
        //spr.argb = SET_ALPHA(0.25f , spr.argb);//0x80808080;
        spr.argb = 0x80202020;

        mx = m_Sprites[ii].spr.pos.x;
        my = m_Sprites[ii].spr.pos.y;
        
        for( Float32 fr=0.0f ; fr<360.0f; fr+=45.0f)
        {
            spr.pos.x = mx + gxUtil::Cos(fr)*4.0f;
            spr.pos.y = my + gxUtil::Sin(fr)*4.0f;
            gxLib::PutSpriteEx(&spr);
        }
*/


    //gxLib::DrawBox(ax, ay, ax + m_Width, ay + m_Height, 100, gxFalse,ATR_DFLT, 0xff00ff00, 3.0f);

}


void CFontManager::drawFont(StSpriteEX sprite , Sint32 ax, Sint32 ay, Sint32 prio, Uint32 atr, Uint32 argb, Float32 fRot, Float fScale)
{
	if (fRot != 0.0f)
	{
		Sint32 mx = 0, my = 0;
		gxPoint pnt;

		pnt.x = sprite.spr.pos.x;
		pnt.y = sprite.spr.pos.y;
		gxUtil::RotationPoint(&pnt, fRot);
		sprite.spr.pos.x = pnt.x;
		sprite.spr.pos.y = pnt.y;
		sprite.spr.pos.z = prio;
		sprite.spr.rot.z = fRot;
	}

	if (fScale != 1.0f)
	{
		sprite.spr.pos.x *= fScale;
		sprite.spr.pos.y *= fScale;
		sprite.spr.scl.x = fScale;
		sprite.spr.scl.y = fScale;
	}

	sprite.spr.pos.x += ax;
	sprite.spr.pos.y += ay;
	sprite.spr.pos.z = prio;

	sprite.spr.argb = argb;

	gxLib::PutSpriteEx(&sprite.spr);

	/*
	gxLib::PutSprite(
		&sprite.spr.albedo,
		sprite.spr.pos.x,
		sprite.spr.pos.y,
		sprite.spr.pos.z,
		0,
		sprite.spr.argb,
		sprite.spr.rot.fz,
		sprite.spr.scl.fx,
		sprite.spr.scl.fy
	);
	*/

}

