//--------------------------------------------------------------------------
//
// CFontManager.h
// written bt tomi.
// 2013.01.21　更新
// 
//--------------------------------------------------------------------------
#ifndef _CFONTMANAGER_H_
#define _CFONTMANAGER_H_

#include <gxLib.h>

#define DEFAULT_FONT_SIZE (1.0f)

class CFontManager
{
public:
	struct SInfo {
		wchar_t  id;
		Uint16  pg;
		Uint16  u;
		Uint16  v;
		Uint16  w;
		Uint16  h;
		Uint16  ox;
		Uint16  oy;
		Uint16  xa;
		Uint16  ch;
	};

	struct StSpriteEX
	{
		int lineIndex = 0;
		gxLib::StSprite spr;
	};

	CFontManager();

	~CFontManager()
	{
	}

	void Init();
	void Load(Sint32 page, gxChar* pFileName , Sint32 fontSize );

	Sint32 GetFontSize()
	{
		return m_FontSize;
	}

	void SetScale(Float32 fScale)
	{
		m_fScale = fScale;
	}

	Float32 GetScale()
	{
		return m_fScale;
	}

	Sint32 Printf(Sint32 x, Sint32 y, Sint32 prio, Uint32 atr, Uint32 argb, gxChar* str, ...)
	{
		va_list app;
		va_start(app, str);
		vsprintf(m_Temp, str, app);
		va_end(app);

		return Print(x, y, prio, atr, argb, m_Temp);

	}

	Sint32 Print(Sint32 x, Sint32 y, Sint32 prio, Uint32 atr, Uint32 argb, gxChar* str )
	{
		size_t sz = 0;
		gxChar* p32 = CDeviceManager::UTF8toUTF32(str , &sz);

		calc(p32,sz);

		draw(x, y, prio, atr, argb, 0.0f, 1.0f);

		SAFE_DELETES(p32);

		return m_Width;
	}
    void draw( Sint32 ax , Sint32 ay, Sint32 prio , Uint32 atr , Uint32 argb , Float32 fRot , Float fScale );

	void Test()
	{
		//テストルーチン

		wchar_t* pStr = L"-ABCDE0123456789\nＡＢＣＤＥ０１２３４５６７８９\n改行後の制御\n(C)　ガルルソフトウェア研究所\n最終行のテストてす";

	//	calc(pStr);

		//描画

		Sint32 ax, ay;
		ax = WINDOW_W / 2;
		ay = WINDOW_H / 2;

		//m_Attribute |= ATR_STR_CENTER;
		//m_Attribute |= ATR_STR_RIGHT;
		//m_fRotation = gxLib::GetGameCounter() % 360;
		//m_Attribute |= ATR_STR_MID;
		//m_Attribute |= ATR_STR_BOTTOM;

		draw(ax, ay, 100,m_Attribute, m_ARGB, m_fRotation, m_fScale);
	}

	void MakeFontData();

	SINGLETON_DECLARE( CFontManager );

private:


	void calc(gxChar* pStr, size_t sz);
	void drawFont(StSpriteEX spr , Sint32 ax, Sint32 ay, Sint32 prio, Uint32 atr, Uint32 argb, Float32 fRot, Float fScale);


	std::map<wchar_t, SInfo> m_List;
	std::vector<StSpriteEX> m_Sprites;
	std::vector<int> m_LineWidth;

	Sint32 m_FontSize = 48;
	Sint32 m_Width = 0;
	Sint32 m_Height = 0;

	Float32 m_fScale    = 1.0f;
	Float32 m_fRotation = 0.0f;
	Uint32  m_Attribute = 0;
	Uint32  m_ARGB = 0xffffffff;
	gxBool  m_bOutLine = gxFalse;

	enum {
		enTempLength = 512,
	};
	gxChar m_Temp[enTempLength];

	Sint32 m_TexturePage = 0;

	gxBool m_bTouhaba = gxFalse;// True;

};


#if 0
void calc2(wchar_t* pStr)
{
	//表示するのに必要な情報を集める

	size_t len = wcslen(pStr);

	m_Sprites.clear();
	m_LineWidth.clear();

	m_Width = 0;
	m_Height = 0;

	Sint32 x = 0, y = 0, z = 0, w = 0, h = 0;
	Sint32 lineHeight = 0;
	Sint32 ln = 0;

	for (size_t ii = 0; ii < len; ii++)
	{
		wchar_t id = pStr[ii];

		auto itr = m_List.find(id);

		if (itr == m_List.end())
		{
			if (id == '\n')
			{
				m_LineWidth.push_back(x);
				ln++;

				x = 0;
				y += lineHeight;
				m_Height += lineHeight;
				continue;
			}
			else
			{
				continue;
			}
		}

		if (id == '\t' || id == ' ' || id == 12288 /*　*/)
		{
			id = ' ';
			itr = m_List.find(id);
			x += 16;
			continue;
		}

		SInfo info = itr->second;

		StSpriteEX sprex;
		sprex.lineIndex = ln;
		sprex.spr.pos = { x,y,0 };
		sprex.spr.albedo = {
			//				(info.pg+m_TexturePage) * 64,
							(m_TexturePage),// * 64,
							info.u,
							info.v,
							info.w,
							info.h,
							-info.ox,
							-info.oy,
		};

		w = info.w + info.ox;
		h = info.h + info.oy;

		if (m_bTouhaba)
		{
			x += 18;
		}
		else
		{
			x += w;
		}

		if (h > lineHeight) lineHeight = h;
		if (x > m_Width)    m_Width = x;

		sprex.spr.shader = gxShaderFont;
		sprex.spr.option[0] = info.pg;
		m_Sprites.push_back(sprex);
	}

	m_LineWidth.push_back(x);
	m_Height += lineHeight;
	ln++;

	//gxLib::DrawPoint(m_Width , m_Height , 100, ATR_DFLT, 0xC080F080 , 8.0f);
}
#endif

#endif

