﻿#ifndef _CBOX2D_MANAGER_H_
#define _CBOX2D_MANAGER_H_

#include "box2d/box2d.h"

#define PIX(a) ((a)/100.f)
#define XIP(a) ((a)*100.f)

class CPhyzics
{

public:

	enum enBodyType {
		enBodyTypeStatic,
		enBodyTypeDynamic,
	};

	enum {
		enObjMax = 1024,
		enPhyzPrioDebug0 =254,
		enPhyzPrioDebug1 =255,
	};


	CPhyzics()
	{ 
		//pzLib::Init();

		b2Vec2 gravity(0, 1.f);

		m_pWorld = new b2World( gravity );

		m_pObj = new StObj[enObjMax];

		memset( m_pObj, 0x00, sizeof( StObj )*enObjMax );

		m_sObjCnt = 0;

		m_bDebugOn = gxTrue;
	}

	~CPhyzics()
	{
		delete[] m_pObj;
		SAFE_DELETE(m_pWorld);
	}

	void SetGravity( Float32 x , Float y )
	{
		b2Vec2 gravity( x, y );

		m_pWorld->SetGravity( gravity );
	}

	void Action()
	{
		float32 timeStep = 3.5f / 60.0f;
		Sint32 velocityIterations = 8;
		Sint32 positionIterations = 1;

		m_pWorld->Step( timeStep, velocityIterations, positionIterations);
	}

	Sint32 Polygon( Sint32 x, Sint32 y , gxPoint *pt , Sint32 num , Uint32 maskBits= 0xd9d9 );

	Sint32 Box( Sint32 x , Sint32 y  , Sint32 x1 , Sint32 y1 , Sint32 x2 , Sint32 y2 , Float32 rot=0.f , Uint16 maskBits = 0xffff );

	Sint32 SetLink( Sint32 src , Sint32 dst , Sint32 x , Sint32 y , Sint32 limit1=0, Sint32 limit2=0 );

	void Draw();

	void SetBodyType( Sint32 n , enBodyType type )
	{
		b2BodyType tbl[]={
			b2_staticBody,
			b2_dynamicBody
			//b2_kinematicBody,
		};

		GetBody( n )->SetType( tbl[type] );
	}

	b2Body* GetBody( Sint32 n )
	{
		return m_pObj[n].m_pBody != NULL?m_pObj[n].m_pBody : NULL;
	}

	b2Joint* GetLink( Sint32 n )
	{
		return m_pObj[n].m_pLink;
	}

	void DestroyObj( Sint32 n )
	{

		if( m_pObj[n].bUse )
		{
			m_pObj[n].bUse = gxFalse;
			m_sObjCnt --;

			if( m_pObj[n].type == 1 )
			{
				m_pWorld->DestroyBody( GetBody( n ) );
			}
			else if( m_pObj[n].type == 2 )
			{
				//m_pWorld->DestroyJoint( GetLink( n ) );
			}
		}
	}

	void AddPower( Sint32 objID , Sint32 x , Sint32 y );
	void AddTorque( Sint32 objID , Sint32 torque );
	void SetDebugDraw( gxBool bDebugOn )
	{
		m_bDebugOn = bDebugOn;
	}

	SINGLETON_DECLARE( CPhyzics );

private:


	Sint32 addJoint( b2Joint * pObj )
	{
		for(Sint32 ii=0;ii<enObjMax;ii++)
		{
			if( !m_pObj[ii].bUse )
			{
				m_pObj[ii].id      = ii;
				m_pObj[ii].type    = 2;
				m_pObj[ii].bUse    = gxTrue;
				m_pObj[ii].m_pBody = NULL;
				m_pObj[ii].m_pLink = pObj;
				m_sObjCnt ++;
				return ii;
			}
		}
		gxLib::DebugLog( (gxChar*) "!!!!!!!!! m_pObj[%d] 配列に空きがありません、enObjMaxの値を増やしてください。 !!!!!!!!!", enObjMax );
		return -1;
	}

	Sint32 addObj( b2Body * pObj )
	{
		for(Sint32 ii=0;ii<enObjMax;ii++)
		{
			if( !m_pObj[ii].bUse )
			{
				m_pObj[ii].id      = ii;
				m_pObj[ii].type    = 1;
				m_pObj[ii].bUse    = gxTrue;
				m_pObj[ii].m_pBody = pObj;
				m_pObj[ii].m_pLink = NULL;
				m_sObjCnt ++;
				return ii;
			}
		}

		gxLib::DebugLog( (gxChar*) "!!!!!!!!! m_pObj[%d] 配列に空きがありません、enObjMaxの値を増やしてください。 !!!!!!!!!", enObjMax );
		return -1;
	}

	void drawBox(b2Body *p);
	void drawJoint(b2Joint* p);

	b2World* m_pWorld;

	typedef struct StObj
	{
		Sint32 type;
		Sint32 id;
		b2Body  *m_pBody;
		b2Joint *m_pLink;
		gxBool bUse;
	} StObj;

	StObj *m_pObj;
	Sint32 m_sObjCnt;

	gxBool m_bDebugOn;

};


#endif

