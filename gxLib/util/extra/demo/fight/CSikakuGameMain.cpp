﻿//------------------------------------------------
//
//
// 対戦格闘メイン
//
//
//------------------------------------------------

#include <gxLib.h>
#include "sikaku.h"
//#include "../netVersus.h"

//void game();
void gameSikaku( gxBool bOnlyDraw );

CSikakuGameMain::CSikakuGameMain()
{
	//------------------------------------------------
	//------------------------------------------------

	m_sCurrent = 0;
	m_bServer = gxFalse;
	m_sGameSeq = 0;

	m_Name[0][0] = 0x00;
	m_Name[1][0] = 0x00;

	CSub::PlayAudio( enAudioBgm );

	m_sWinCnt[0] = 0;
	m_sWinCnt[1] = 0;

	sprintf( &m_Name[0][0] , "1P" ); 
	sprintf( &m_Name[1][0] , "2P" ); 

	m_bGameOver = gxFalse;
	m_sGameMode = 0;

	m_bOnline = gxFalse;

	reset();

}


CSikakuGameMain::~CSikakuGameMain()
{
	//------------------------------------------------
	//------------------------------------------------

	gxLib::StopAudio( enAudioBgm );
	delete m_pPlayer[0];
	delete m_pPlayer[1];
}


void CSikakuGameMain::reset()
{
	//------------------------------------------------
	//------------------------------------------------

	CActManager::DeleteInstance();

	CSikakuPlayer *p1,*p2;

// コンストラクタに移動
//	m_Name[0][0] = 0x00;
//	m_Name[1][0] = 0x00;

	m_sZoneTimer = 0;

	m_pPlayer[0] = p1 = new CSikakuPlayer( 0 );
	m_pPlayer[1] = p2 = new CSikakuPlayer( 1 );

	p1->SetEnemy( p2 );
	p2->SetEnemy( p1 );

	m_AreaX  = (enStageWidth-enRingWidth)/2;
	m_WorldX = m_AreaX*100;
	m_WorldY = 0;

	m_sStopFrame = 0;
	m_ComboCnt[0] = 0;
	m_ComboCnt[1] = 0;

	m_sHP[0] = 1000;
	m_sHP[1] = 1000;

	m_sCB[0] = 1000;
	m_sCB[1] = 1000;

	for(Sint32 ii=0;ii<2;ii++)
	{
		m_sTobidouguCnt[ ii ] = 0;
	}

	m_sSlow = 0;

	m_sQuakeFrame = 0;
	m_fQuakeY = 0.f;

	m_sFirstAtack = -1;
}

void CSikakuGameMain::Action()
{
	//------------------------------------------------
	//------------------------------------------------

	m_sZoneTimer ++ ;

	switch( m_sGameSeq ){
	case 0:
		if( m_bServer )
		{
			m_sCurrent = 0;
		}
		else
		{
			m_sCurrent = 1;
		}
		changeSeq( 1000 );
		break;

	case 1000:
		seqGameInit();
		break;

	case 2000:
		seqGameMain();
		break;

	case 3000:
		seqGameResult();
		break;

	case 4000:
		reset();
		changeSeq( 1000 );
		break;

	case 5000:
		seqGameEnd();
		break;
	}

	//UIコントロール
	uiControl();
}


void CSikakuGameMain::seqGameInit()
{
	//------------------------------------------------
	//ラウンドコール
	//------------------------------------------------

	CActManager::GetInstance()->Action();
	CCollisionManager::GetInstance()->Action();

	if( m_sZoneTimer > 96 )
	{
		m_pPlayer[0]->Go();
		m_pPlayer[1]->Go();
		changeSeq( 2000 );
	}
	else
	{
		gxLib::Printf( 220 , 128, 200 , ATR_DFLT , 0xffffffff , "Ready!" );
	}

}

void CSikakuGameMain::seqGameMain()
{
	//------------------------------------------------
	//ファイト
	//------------------------------------------------

	if( m_sZoneTimer < 60 )
	{
		gxLib::Printf( 220 , 128, 200 , ATR_DFLT , 0xffffffff , "Fight!" );
	}

	if( m_sSlow > 0 )
	{
		m_sSlow --;

		if( m_sZoneTimer%2 == 0 )
		{
			return;
		}
	}

	CActManager::GetInstance()->Action();
	CCollisionManager::GetInstance()->Action();

	if( m_sStopFrame > 0 )
	{
		//画面停止
		m_sStopFrame --;
		return;
	}

	if( m_sQuakeFrame > 0 )
	{
		//画面揺らし
		m_sQuakeFrame --;
		m_fQuakeY = (gxUtil::Cos( (gxLib::GetGameCounter()*16)%360 ) * m_sQuakeFrame )/2.f;
	}

	if( !m_pPlayer[0]->IsExist() || !m_pPlayer[1]->IsExist() )
	{
		//リザルトへ
		m_pPlayer[0]->Stop();
		m_pPlayer[1]->Stop();

		if( m_pPlayer[0]->IsExist() )
		{
			m_pPlayer[0]->SetWinner();
		}
		else if( m_pPlayer[1]->IsExist() )
		{
			m_pPlayer[1]->SetWinner();
		}

		changeSeq( 3000 );
	}

	playerControl();

	m_WorldX = m_AreaX*100;

}


void CSikakuGameMain::seqGameResult()
{
	//------------------------------------------------
	//リザルト
	//------------------------------------------------

	CActManager::GetInstance()->Action();
	CCollisionManager::GetInstance()->Action();

	if( !m_pPlayer[0]->IsExist() && !m_pPlayer[1]->IsExist() )
	{
		gxLib::Printf( 64 , 128, 200 , ATR_DFLT , 0xffffffff , "Draw" );
	}
	else if( m_pPlayer[0]->IsExist() )
	{
		gxLib::Printf( 224 , 128, 200 , ATR_DFLT , 0xffffffff , "1p Win" );
		if( m_sZoneTimer == 120 )
		{
			m_sWinCnt[0] ++;
		}
	}
	else if( m_pPlayer[1]->IsExist() )
	{
		gxLib::Printf( 224 , 128, 200 , ATR_DFLT , 0xffffffff , "2p Win" );
		if( m_sZoneTimer == 120 )
		{
			m_sWinCnt[1] ++;
		}
	}

	if( 1 )//m_sGameMode == enGameMode3PointMatch )
	{
		//3PointMatch
		if( m_sWinCnt[0] >= 2 || m_sWinCnt[1] >= 2 )
		{
			changeSeq( 5000 );
		}
	}

	if( m_sZoneTimer > 180 )
	{
		changeSeq( 4000 );
	}

}


void CSikakuGameMain::seqGameEnd()
{
	//------------------------------------------------
	//ゲーム終了
	//------------------------------------------------


	if( m_sZoneTimer == 60 )
	{
		CSub::StopAudio( enAudioBgm );
		m_bGameOver = gxTrue;
	}

}


void CSikakuGameMain::uiControl()
{
	//------------------------------------------------
	//UI関連のコントロール
	//------------------------------------------------


	for(Sint32 ii=0;ii<2;ii++)
	{
		Sint32 n = m_pPlayer[ii]->GetComboCnt();

		if( m_sFirstAtack == -1 )
		{
			if( m_ComboCnt[ii] > 0 )
			{
				//First Atack
				m_sFirstAtack = ii;
				new CEffComboDisp( 1-m_sFirstAtack , -1 );
			}
		}

		if( n == 0 && m_ComboCnt[ii]>1 )
		{
			new CEffComboDisp( 1-ii , m_ComboCnt[ii] );
			m_ComboCnt[ii] = 0;
		}
		m_ComboCnt[ii] = n;

		if( m_pPlayer[ii]->GetHP() < m_sHP[ii] )
		{
			m_sHP[ii] --;
			m_sHP[ii] += ( m_pPlayer[ii]->GetHP() - m_sHP[ii] )/10;
		}

		m_sCB[ii] += ( m_pPlayer[ii]->GetCB() - m_sCB[ii] )/10;

		if( m_pPlayer[ii]->GetCB() < m_sCB[ii] )
		{
			m_sCB[ii] --;
		}
		else
		{
			m_sCB[ii] ++;
		}
	}

}


void CSikakuGameMain::playerControl()
{
	//------------------------------------------------
	//
	//------------------------------------------------

	gxPos *p1,*p2;

	p1 = m_pPlayer[0]->GetPos();
	p2 = m_pPlayer[1]->GetPos();

	Sint32 x1 = m_AreaX ,x2 = x1 + enRingWidth;

	if( m_sZoneTimer%2 == 0 )
	{
		if( p1->x < x1*100 )
		{
			x1 = p1->x/100;
			x2 = x1 + enRingWidth;
		}
		else if( p1->x > x2*100 )
		{
			x2 = p1->x/100;
			x1 = x2 - enRingWidth;
		}
		else
		{
			x1 = (p1->x+p2->x)/200 - enRingWidth/2;
			x2 = x1 + enRingWidth;
		}
	}
	else
	{
		if( p2->x < x1*100 )
		{
			x1 = p2->x/100;
			x2 = x1 + enRingWidth;
		}
		else if( p2->x > x2*100 )
		{
			x2 = p2->x/100;
			x1 = x2 - enRingWidth;
		}
		else
		{
			x1 = (p1->x+p2->x)/200 - enRingWidth/2;
			x2 = x1 + enRingWidth;
		}
	}

	if( x1 < 0 ) x1 = 0;
	if( x1 > enStageWidth-enRingWidth ) x1 = enStageWidth-enRingWidth;

	m_AreaX = x1;

	Sint32 cx,w=2400;

	if( ABS( p1->x - p2->x )<= w*2 && ABS( p1->y - p2->y ) <= w*2 )
	{
		//近づきすぎ

		cx = (p1->x + p2->x)/2;

		if( p1->x < p2->x )
		{
			//プレイヤー１が左
			p1->x += ( cx-w - p1->x)/5.f;
			p2->x += ( cx+w - p2->x)/5.f;
		}
		else
		{
			p1->x += ( cx+w - p1->x)/5.f;
			p2->x += ( cx-w - p2->x)/5.f;
		}
	}

	x1 = m_AreaX ,x2 = x1 + enRingWidth;

	if( p1->x < 0*100 ) p1->x = (0)*100;
	if( p1->x > enStageWidth*100 ) p1->x = (enStageWidth)*100;

	if( p2->x < 0*100 ) p2->x = (0+1)*100;
	if( p2->x > enStageWidth*100 ) p2->x = (enStageWidth)*100;


	//壁際判定

	if( p1->x <= 1*100 || p1->x >=enStageWidth*100 )
	{
		m_pPlayer[0]->SetKabegiwa( gxTrue );
	}
	else
	{
		m_pPlayer[0]->SetKabegiwa( gxFalse );
	}

	if( p2->x <= 100*100 || p2->x >=enStageWidth*100 )
	{
		m_pPlayer[1]->SetKabegiwa( gxTrue );
	}
	else
	{
		m_pPlayer[1]->SetKabegiwa( gxFalse );
	}

	for(Sint32 ii=0;ii<enPlayerMax;ii++)
	{
		//飛び道具カウンタチェック
		if( m_sTobidouguCnt[ ii ] > 0 ) m_sTobidouguCnt[ ii ] --;
	}
}


void CSikakuGameMain::Draw()
{
	//------------------------------------------------
	//背景描画
	//------------------------------------------------
	CActManager::GetInstance()->Draw();

	Sint32 qy = m_fQuakeY;

	Sint32 ax = -m_WorldX/100;
	Sint32 x1,x2;

	x1 = ax + 8;
	x2 = x1 + enStageWidth/2 - 16;

	gxLib::DrawBox( x1 , 32 +qy, x2 ,256+qy , 0 , gxFalse, ATR_DFLT , 0xff00ff00 );

	x1 = ax + enStageWidth/2;
	x2 = x1 + enStageWidth/2 - 16;

	gxLib::DrawBox( x1 , 32+qy , x2 ,256+qy , 0 , gxFalse, ATR_DFLT , 0xff00ff00 );

	for(Sint32 ii=-enStageWidth/2;ii<enStageWidth/2+64;ii+=64)
	{
		x1 = ii+ax;
		x2 = (ii+ax)*2.f;
		x1 += enStageWidth/2;
		x2 += enStageWidth/2;
		x2 += 160/2;
		gxLib::DrawLine( x1 , 256+qy , x2 ,320+qy , 0 , ATR_DFLT , 0xff00ff00 );
	}


	if( m_sStopFrame > 0 )
	{
		//スーパーアーツ状態
		gxLib::DrawBox( 0 , 0 , WINDOW_W ,WINDOW_H , 64 , gxTrue, ATR_DFLT , 0x80808080 );
	}

//	m_sWinCnt[0] = 2;
	for(Sint32 ii=0;ii<2;ii++)
	{
		Sint32 ax,ay;
		ax = 32 + ii*20;
		ay = 64;

		gxLib::DrawBox( ax , ay , ax + 16 , ay + 16 , 64 , ( ii < m_sWinCnt[0] )? gxTrue : gxFalse , ATR_DFLT , 0xff00ff00 );
	}

	for(Sint32 ii=0;ii<2;ii++)
	{
		Sint32 ax,ay;
		ax = WINDOW_W-32-16 - ii*20;
		ay = 64;

		gxLib::DrawBox( ax , ay , ax + 16 , ay + 16 , 64 , ( ii < m_sWinCnt[1] )? gxTrue : gxFalse  , ATR_DFLT , 0xff00ff00 );
	}

	drawGauge();
	drawComboGauge();

}


void CSikakuGameMain::GetWorldPos( gxPos *pPos )
{
	//------------------------------------------------

	//------------------------------------------------

	Sint32 wx = CSikakuGameMain::GetInstance()->GetWorldX();

	pPos->x -= wx;
}

void CSikakuGameMain::drawGauge()
{
	//------------------------------------------------
	//ゲージ1P
	//------------------------------------------------

	Sint32 x1,x2,z = 200;
	Sint32 w = 0;

	//赤
	w = enHPGaugeLength*m_sHP[0]/1000;
	if( w <= 0 ) w = 0;
	x2 = WINDOW_W/2 -32;
	x1 = x2 - w;
	gxLib::DrawBox( x1 , 32 , x2 ,56 , z , gxTrue, ATR_DFLT , 0xffff0000 );

	//黄色
	w = enHPGaugeLength*m_pPlayer[0]->GetHP()/1000;
	if( w <= 0 ) w = 0;
	x2 = WINDOW_W/2 -32;
	x1 = x2 - w;
	gxLib::DrawBox( x1 , 32 , x2 ,56 , z , gxTrue, ATR_DFLT , 0xffffff00 );

	//枠
	x2 = WINDOW_W/2 -32;
	x1 = x2 - enHPGaugeLength;
	gxLib::DrawBox( x1 , 32 , x2 ,56 , z , gxFalse, ATR_DFLT , 0xffffffff );

	gxLib::Printf( x1,16 ,z , ATR_DFLT , 0xffff0000 , "%s", GetPlayerName( 0 )  );
	//--------------------------------------------------------------------------

	//ゲージ2P

	//赤
	w = enHPGaugeLength*m_sHP[1]/1000;
	if( w <= 0 ) w = 0;
	x1 = WINDOW_W/2 +32;
	x2 = x1 + w;
	gxLib::DrawBox( x1 , 32 , x2 ,56 , z , gxTrue, ATR_DFLT , 0xffff0000 );

	//黄色
	w = enHPGaugeLength*m_pPlayer[1]->GetHP()/1000;
	if( w <= 0 ) w = 0;
	x1 = WINDOW_W/2 +32;
	x2 = x1 + w;
	gxLib::DrawBox( x1 , 32 , x2 ,56 , z , gxTrue, ATR_DFLT , 0xffffff00 );

	//枠
	x1 = WINDOW_W/2 +32;
	x2 = x1 + enHPGaugeLength;
	gxLib::DrawBox( x1 , 32 , x2 ,56 , z , gxFalse, ATR_DFLT , 0xffffffff );

	gxLib::Printf( x2-strlen( GetPlayerName( 1 )) *6 ,16 ,z , ATR_DFLT , 0xffff0000 , "%s", GetPlayerName( 1 )  );

}


void CSikakuGameMain::drawComboGauge()
{
	//------------------------------------------------
	//ゲージ1P
	//------------------------------------------------

	Sint32 x1,x2 ,z = 200;
	Sint32 w = 0,h = 18,y;
	Sint32 max = enCBGaugeLength;

	y = 280;

	//赤
	w = max*m_sCB[0]/1000;
	if( w <= 0 ) w = 0;
	x2 = WINDOW_W/2 -128;
	x1 = x2 - w;
	gxLib::DrawBox( x1 , y , x2 ,y+h , z , gxTrue, ATR_DFLT , 0xff00ffff );

	//枠
	x2 = WINDOW_W/2 -128;
	x1 = x2 - max;
	gxLib::DrawBox( x1 , y , x2 ,y+h , z , gxFalse, ATR_DFLT , 0xffffffff );

	//--------------------------------------------------------------------------

	//ゲージ2P

	//赤
	w = max*m_sCB[1]/1000;
	if( w <= 0 ) w = 0;
	x1 = WINDOW_W/2 +128;
	x2 = x1 + w;
	gxLib::DrawBox( x1 , y , x2 ,y+h , z , gxTrue, ATR_DFLT , 0xff00ffff );

	//枠
	x1 = WINDOW_W/2 +128;
	x2 = x1 + max;
	gxLib::DrawBox( x1 , y , x2 ,y+h , z , gxFalse, ATR_DFLT , 0xffffffff );

}


SINGLETON_DECLARE_INSTANCE( CSikakuGameMain );

void CDraw::PutSprite(
	Sint32 x,		Sint32 y,	Sint32 prio,
	Sint32 page,	Sint32 u, 	Sint32 v,	Sint32 w,	Sint32 h,

	Sint32 cx,
	Sint32 cy,
	Uint32 atr,
	Uint32 col,
	Float32 r ,
	Float32 sx,
	Float32 sy)
{
	Sint32 wx = CSikakuGameMain::GetInstance()->GetWorldX();

	gxLib::PutSprite( (x-wx)/100,y/100,prio,page,u,v,w,h,cx,cy,atr,col,r,sx,sy);
}

void CDraw::PutSprite( gxSprite* pSpr, Sint32 x,Sint32 y,	Sint32 prio,Uint32 atr,	Uint32 col,	Float32 r,	Float32 sx,	Float32 sy	)
{
	//------------------------------------------------
	//------------------------------------------------

	gxLib::PutSprite(x/100,y/100,prio,pSpr->page,pSpr->u,pSpr->v,pSpr->w,pSpr->h,pSpr->cx,pSpr->cy,atr,col,r,sx,sy);
}

void gameSikaku( gxBool bOnlyDraw )
{
	//-------------------------------------
	//
	//-------------------------------------

//	void MainMenuTest();
//	MainMenuTest();
//	return;

	//-----------------------------------------
	//ネット対戦中
	//-----------------------------------------
/*
	if( CNetVS::GetInstance()->IsReady() )
	{
		//ネット対戦の情報が整った

		if( CNetVS::GetInstance()->IsServer() )
		{
			CSikakuGameMain::GetInstance()->SetServer();
		}

		CSikakuGameMain::GetInstance()->SetPlayerInfo( 0 , CNetVS::GetInstance()->GetName( 0 ) );
		CSikakuGameMain::GetInstance()->SetPlayerInfo( 1 , CNetVS::GetInstance()->GetName( 1 ) );
	}
*/
	if( !bOnlyDraw )
	{
		CSikakuGameMain::GetInstance()->Action();
	}

	CSikakuGameMain::GetInstance()->Draw();


}

