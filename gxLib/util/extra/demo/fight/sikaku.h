﻿//------------------------------------------------
//
//
// 共通ヘッダ
//
// written by tomi.2013.01.18
//------------------------------------------------
enum {
	enTexMain = -1,
};

#define _COD(n) ((n)*100)
#define _DIV(n) ((n)/100)

#include <gxLib/Util/extra/action/CActManager.h>
#include <gxLib/Util/extra/action/CCollisionManager.h>

#include "CSikakuPlayer.h"
#include "CSikakuEffect.h"

enum {
	DIR_RIGHT = 1,
	DIR_LEFT  = -1,

	enStageWidth = 640,
	enRingWidth  = 480,

	enHPGaugeLength = 180,
	enCBGaugeLength = 96,
};

enum {
	enAudioBazooka  ,
	enAudioBltClr   ,
	enAudioDash     ,
	enAudioExplosion,
	enAudioHit      ,
	enAudioLanding  ,
	enAudioPurge    ,
	enAudioBoost    ,
	enAudioJump     ,
	enAudioKnouckle ,
	enAudioCrash    ,
	enAudioBgm      ,
};

//------------------------------------------------
//汎用ライブラリラッパー
//------------------------------------------------

class CSub
{
public:
	CSub(){}
	~CSub(){}

	static void PlayAudio( Sint32 index )
	{
		gxLib::PlayAudio( index );
		gxLib::SetAudioVolume ( index , 0.8f);
	}
	static void StopAudio( Sint32 index )
	{
		gxLib::StopAudio( index , 120 );
	}


private:

};

class CDraw
{
public:
	CDraw(){}
	~CDraw(){}

	static void PutSprite(
		Sint32 x,		Sint32 y,	Sint32 prio,
		Sint32 page,	Sint32 u, 	Sint32 v,	Sint32 w,	Sint32 h,

		Sint32 cx=0,
		Sint32 cy=0,
		Uint32 atr = ATR_DEFAULT,
		Uint32 col = ARGB_DEFAULT,
		Float32 r  = 0,
		Float32 sx = 1.0f,
		Float32 sy = 1.0f	);
/*
		{
			gxLib::PutSprite(x/100,y/100,prio,page,u,v,w,h,cx,cy,atr,col,r,sx,sy);
		}
*/
	static void PutSprite(
		gxSprite* pSpr, Sint32 x,		Sint32 y,	Sint32 prio,	Uint32 atr = ATR_DEFAULT,	Uint32 col = ARGB_DEFAULT,
		Float32 r  = 0,	Float32 sx = 1.0f,	Float32 sy = 1.0f	);
/*
		{
			gxLib::PutSprite(x/100,y/100,prio,pSpr->page,pSpr->u,pSpr->v,pSpr->w,pSpr->h,pSpr->cx,pSpr->cy,atr,col,r,sx,sy);
		}
*/
private:


};



class CSikakuGameMain
{
public:
	enum {
		enPlayerMax = 2,
	};

	enum {
		enGameModeIronMan,
		enGameMode3PointMatch,
	};

	CSikakuGameMain();
	~CSikakuGameMain();

	void Action();
	void Draw();

	void SetServer()
	{
		m_bServer = gxTrue;
	}

	Sint32 GetCurrent()
	{
		return m_sCurrent;
	}

	void TheWorld( Sint32 frm )
	{
		m_sStopFrame = frm;
	}

	gxBool IsTheWorld( )
	{
		if( m_sStopFrame > 0 ) return gxTrue;

		return gxFalse;
	}

	Sint32 GetWorldX()
	{
		return m_WorldX;
	}

	void SetSlow( Sint32 frm )
	{
		m_sSlow = frm;
	}

	void SetPlayerInfo( Sint32 sPlayer , gxChar *pName )
	{
		if( sPlayer < 0 || sPlayer >= 2 ) return;

		if( pName )
		{
			sprintf( m_Name[sPlayer] , "%s" , pName );
		}
	}

	gxChar *GetPlayerName(Sint32 n)
	{
		if( n < 0 || n >= 2 ) return NULL;

		return m_Name[n];
	}

	void GetWorldPos( gxPos *pPos );

	void SetTobidougu( Sint32 sPlayer )
	{
		m_sTobidouguCnt[sPlayer] = 8;
	}

	gxBool IsTobidougu( Sint32 sPlayer )
	{
		if( m_sTobidouguCnt[sPlayer] > 0 ) return gxTrue;

		return gxFalse;
	}

	void Quake( Sint32 frm , Sint32 level )
	{
		if( level > 8 ) level = 8;
		if( level < 1 ) level = 1;

		m_sQuakeFrame = frm;
		m_fQuakeY = level * 8;
	}

	void SetGameMode( Sint32 sMode )
	{
		m_sGameMode = sMode;
	}

	gxBool IsGameOver()
	{
		return m_bGameOver;
	}

	void SetOnline( gxBool bOnline )
	{
		m_bOnline = bOnline;
	}

	gxBool IsOnline()
	{
		return m_bOnline;
	}

	SINGLETON_DECLARE( CSikakuGameMain );

private:

	void changeSeq( Sint32 seq )
	{
		m_sZoneTimer = 0;
		m_sGameSeq = seq;
	}

	void reset();

	void seqGameInit();
	void seqGameMain();
	void seqGameResult();
	void seqGameEnd();

	void uiControl();
	void playerControl();

	void drawGauge();
	void drawComboGauge();

	gxChar m_Name[enPlayerMax][255];

	Sint32 m_sStopFrame;

	Sint32 m_WorldX,m_WorldY;
	Sint32 m_AreaX,m_AreaY;


	CSikakuPlayer* m_pPlayer[enPlayerMax];
	Sint32 m_ComboCnt[enPlayerMax];
	Sint32 m_sHP[enPlayerMax];
	Sint32 m_sCB[enPlayerMax];
	Sint32 m_sTobidouguCnt[enPlayerMax];

	Sint32 m_sZoneTimer;
	Sint32 m_sSlow;

	Sint32 m_sGameSeq;

	gxBool m_bServer;

	Sint32 m_sCurrent;

	Sint32  m_sQuakeFrame;
	Float32 m_fQuakeY;

	Sint32 m_sFirstAtack;

	Sint32 m_sWinCnt[2];

	Sint32 m_sGameMode;
	gxBool m_bGameOver;

	gxBool m_bOnline;
};

