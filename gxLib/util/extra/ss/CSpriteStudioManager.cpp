﻿#include <gxLib.h>
#include <gxLib/util/CCsv.h>
#include "CSpriteStudioManager.h"
#include <gxLib/util/CFileZip.h>
#include <gxLib/util/CFileTarga.h>

SINGLETON_DECLARE_INSTANCE( CSpriteStudioManager );


void CSpriteStudioManager::Action()
{
	
}

void CSpriteStudioManager::Draw()
{
/*
	SSHeader *pHeader = &m_Animes[0];

	Sint32 z = 100;

	Float32 w = 1024;// 1080;
	Float32 h = 1736;// 1736;
	Float32 mw = 0;
	Float32 mh = 0;// 1736 / 2;

	Float32 winw,winh;

	winw = 720/2.0f;
	winh = 1280/2.0f;

	static Sint32 koma = 0;

	koma ++;

	Sint32 frm = koma*pHeader->fps/60.0f;
	frm = frm % pHeader->indexNum;

	//pHeader->pImageList[n]

	Sint32 start    = pHeader->pIndexList[frm].start;
	Sint32 partsMax = pHeader->pIndexList[frm].partsNum;

	for (Sint32 ii = 0; ii < partsMax; ii++)
	{
		if( partsMax >= 1000 )
		{
			partsMax = partsMax;
		}

		SSAnimeList *p = &pHeader->pAnimList[start + ii ];

		Uint32 atr = ATR_DFLT;

		if (p->aType == 1) atr = ATR_ALPHA_CRS;
		if (p->aType == 2) atr = ATR_ALPHA_ADD;
		if (p->aType == 3) atr = ATR_ALPHA_SUB;


		Sint32 r = (p->argb1 &0xff000000)>>24;
		Sint32 g = (p->argb1 &0x00ff0000)>>16;
		Sint32 b = (p->argb1 &0x0000ff00)>>8;
		Sint32 a = (p->argb1 &0x000000ff)>>0;

		Uint32 blendARGB = 0x00000000;

		Uint32 argb  = ARGB_DFLT;

		//Sint32 ra = 255 - a;
		//blendARGB = (ra << 24) | (r << 16) | (g << 8) | b;

		Float32 fAlpha = p->alpha;

		gxLib::DrawLine( winw * (mw + p->vx1) / w, winh*(-mh + p->vy1*-1) / h, winw*(mw + p->vx2) / w, winh*(-mh + p->vy2*-1) / h, z, ATR_DFLT, 0x4000ff00, 2);
		gxLib::DrawLine( winw * (mw + p->vx2) / w, winh*(-mh + p->vy2*-1) / h, winw*(mw + p->vx4) / w, winh*(-mh + p->vy4*-1) / h, z, ATR_DFLT, 0x4000ff00, 2);
		gxLib::DrawLine( winw * (mw + p->vx4) / w, winh*(-mh + p->vy4*-1) / h, winw*(mw + p->vx3) / w, winh*(-mh + p->vy3*-1) / h, z, ATR_DFLT, 0x4000ff00, 2);
		gxLib::DrawLine( winw * (mw + p->vx3) / w, winh*(-mh + p->vy3*-1) / h, winw*(mw + p->vx1) / w, winh*(-mh + p->vy1*-1) / h, z, ATR_DFLT, 0x4000ff00, 2);


		Float32 uvRatio = 1.0f;

		gxLib::PutTriangle(
			winw * (mw + p->vx1) / w, winh*(mh + p->vy1*-1) / h,  p->u / uvRatio        , p->v / uvRatio,
			winw * (mw + p->vx2) / w, winh*(mh + p->vy2*-1) / h, (p->u + p->w) / uvRatio, p->v / uvRatio,
			winw * (mw + p->vx3) / w, winh*(mh + p->vy3*-1) / h, (p->u + 0) / uvRatio   , (p->v + p->h) / uvRatio,
			z, p->texID*64 , atr, SET_ALPHA(fAlpha, argb) , blendARGB );

		gxLib::PutTriangle(
			winw * (mw + p->vx2) / w, winh*(mh + p->vy2*-1) / h, (p->u + p->w) / uvRatio, p->v / uvRatio,
			winw * (mw + p->vx4) / w, winh*(mh + p->vy4*-1) / h, (p->u + p->w) / uvRatio, (p->v + p->h) / uvRatio,
			winw * (mw + p->vx3) / w, winh*(mh + p->vy3*-1) / h, (p->u + 0)    / uvRatio, (p->v + p->h) / uvRatio,
			z, p->texID * 64, atr, SET_ALPHA(fAlpha, argb) , blendARGB );

	}
*/
}


//void CSpriteStudioManager::LoadJSON( Sint32 tpg , gxChar *pJSON )
//{
//
//}

void CSpriteStudioManager::LoadSSAE_CSV( Sint32 tpg , gxChar *pFileName )
{
	SSHeader* pHeader = &m_Animes[0];
	//
	CCsv *pCsv = new CCsv();
	pCsv->LoadFile( pFileName );

	cnvCSVtoAnimData( pCsv , &m_Animes[0]);

	gxChar path[FILENAMEBUF_LENGTH];
	gxChar texName[FILENAMEBUF_LENGTH];
	gxUtil::GetPath(pFileName, path );

	for( Sint32 ii=0; ii<pHeader->imageNum; ii++ )
	{
		sprintf( texName, "%s\\%s", path, pHeader->pImageList[ii].name);
		gxLib::LoadTexture( ii*64 , texName );
	}
	gxLib::UploadTexture();

	SAFE_DELETE( pCsv );

}
void CSpriteStudioManager::SetLoop(Sint32 ssaeIndex, gxBool bLoopOn)
{
	SSHeader* pHeader = &m_Animes[ssaeIndex];
	pHeader->m_bLoop = bLoopOn;

}

void CSpriteStudioManager::PutAnime( Sint32 ssaeIndex , Sint32 x , Sint32 y , Sint32 prio, Float32 sec , Uint32 attribute , Uint32 argb , Uint32 blend )
{

	SSHeader *pHeader = &m_Animes[ssaeIndex];

	Sint32 z = prio;

	Float32 w = pHeader->w;
	Float32 h = pHeader->h;
	Float32 mw = pHeader->merginW;
	Float32 mh = pHeader->merginH;

	Float32 winw, winh;

	winw = WINDOW_W;
	winh = WINDOW_H;

	
	Sint32 frm = sec * pHeader->fps;

	if (sec < 0.0f)
	{
		pHeader->m_NowFrame += 1.0f;
	}

	if (pHeader->m_bLoop)
	{
		frm = frm % pHeader->indexNum;
	}
	else
	{
		if (frm >= pHeader->indexNum)
		{
			frm = pHeader->indexNum-1;
		}
	}


	Sint32 start = pHeader->pIndexList[frm].start;
	Sint32 partsMax = pHeader->pIndexList[frm].partsNum;

	for (Sint32 ii = 0; ii < partsMax; ii++)
	{
		SSAnimeList* p = &pHeader->pAnimList[start + ii];

		Uint32 atr = ATR_DFLT;

		if (p->aType == 1) atr = ATR_ALPHA_CRS;
		if (p->aType == 2) atr = ATR_ALPHA_ADD;
		if (p->aType == 3) atr = ATR_ALPHA_SUB;


		Sint32 r = (p->argb1 & 0xff000000) >> 24;
		Sint32 g = (p->argb1 & 0x00ff0000) >> 16;
		Sint32 b = (p->argb1 & 0x0000ff00) >> 8;
		Sint32 a = (p->argb1 & 0x000000ff) >> 0;

		Uint32 blendARGB = 0x00000000;

		Uint32 argb = ARGB_DFLT;

		//Sint32 ra = 255 - a;
		//blendARGB = (ra << 24) | (r << 16) | (g << 8) | b;

		Float32 fAlpha = p->alpha;

		Sint32 x1, y1, x2, y2, x3, y3,x4,y4;
		x1 = x + p->vx1;// winw* (+mw + p->vx1) / w;
		y1 = y + p->vy1*-1;// winh* (-mh + p->vy1 * -1) / h;
		x2 = x + p->vx2;// winw* (+w + p->vx2) / w;
		y2 = y + p->vy2 * -1;// winh* (-mh + p->vy2 * -1) / h;
		x3 = x + p->vx3;// winw* (mw + p->vx3) / w;
		y3 = y + p->vy3 * -1;// winh* (-mh + p->vy3 * -1) / h;
		x4 = x + p->vx4;// winw* (mw + p->vx4) / w;
		y4 = y + p->vy4 * -1;// winh* (-mh + p->vy4 * -1) / h;

//		gxLib::DrawLine( x1, y1, x2, y2, z, ATR_DFLT, 0x4000ff00, 2);
//		gxLib::DrawLine( x2, y2, x4, y4, z, ATR_DFLT, 0x4000ff00, 2);
//		gxLib::DrawLine( x4, y4, x3, y3, z, ATR_DFLT, 0x4000ff00, 2);
//		gxLib::DrawLine( x3, y3, x1, y1, z, ATR_DFLT, 0x4000ff00, 2);

		Sint32 tpg = m_Animes[0].pTexturePageIndex[p->texID];;// pHeader->pTexturePageIndex[p->texID];

		Float32 uvRatio = 1.0f;

		Float32 u[4],v[4];
		u[0] = p->u / uvRatio;
		v[0] = p->v / uvRatio;

		u[1] = (p->u + p->w) / uvRatio;
		v[1] =  p->v / uvRatio;

		u[2] = (p->u + 0)    / uvRatio;
		v[2] = (p->v + p->h) / uvRatio;

		u[3] = (p->u + p->w) / uvRatio;
		v[3] = (p->v + p->h) / uvRatio;

		if( m_SSAnimeOverRide.find(p->ID) != m_SSAnimeOverRide.end() )
		{
			auto q = &m_SSAnimeOverRide[p->ID];

			u[0] = q->u;
			v[0] = q->v;

			u[1] = q->u+q->w;
			v[1] = q->v;

			u[2] = q->u;
			v[2] = q->v+ q->h;

			u[3] = q->u+ q->w;
			v[3] = q->v+ q->h;
		}


		gxLib::PutTriangle(
			x1 , y1, u[0] , v[0],	// p->u / uvRatio         , p->v / uvRatio,
			x2 , y2, u[1] , v[1],	//(p->u + p->w) / uvRatio , p->v / uvRatio,
			x3 , y3, u[2] , v[2],	//(p->u + 0)    / uvRatio , (p->v + p->h) / uvRatio,
			z, tpg, atr, SET_ALPHA(fAlpha, argb), blendARGB);

		gxLib::PutTriangle(
			x2, y2, u[1],v[1],	//(p->u + p->w) / uvRatio ,  p->v / uvRatio,
			x4, y4, u[3],v[3],	//(p->u + p->w) / uvRatio , (p->v + p->h) / uvRatio,
			x3, y3, u[2],v[2],	//(p->u + 0)    / uvRatio , (p->v + p->h) / uvRatio,
			z, tpg, atr, SET_ALPHA(fAlpha, argb), blendARGB);

	}

}


gxBool CSpriteStudioManager::MMakeSSPK( gxChar *pFileName )
{
	//SSPKデータを作成する

	gxBool MakeSSPK( gxChar *pFileName );

	return MakeSSPK( pFileName );
}


gxBool CSpriteStudioManager::LoadSSPK( Sint32 tpg , gxChar *pFileName)
{
	Uint8* pData = NULL;
	Uint32 uSize = 0;

	pData = gxLib::LoadStorageFile( pFileName , &uSize );

	if( pData )
	{
		CFileZip *pZA = new CFileZip();
		pZA->Read(pData, uSize );

		//std::vector<CFileZip::StArcInfo> *pList = pZA->GetFileList( );

		void *p;
		Uint32 uSize = 0;
		p = pZA->Decode( "index.csv", &uSize);

		if ( p == NULL ) return gxFalse;

		CCsv *pCsv = new CCsv();
		pCsv->ReadFile( (Uint8*)p, uSize);

		gxChar *pStr = pCsv->VLOOKUP("max", 0,1);

		if (!pStr)
		{
			SAFE_DELETES(p);
			SAFE_DELETE(pCsv);
			return gxFalse;
		}

		m_MaxAnimNum = atoi(pStr);

		if (m_MaxAnimNum < 0)
		{

		}

		SAFE_DELETES(m_Animes);

		m_Animes = new SSHeader[ m_MaxAnimNum ];

		gxChar pCsvName[512];
		Uint32 uZipSize = 0;

		for( Sint32 ii=0; ii<pCsv->GetHeight(); ii++ )
		{
			if( strcmp( "csv",pCsv->GetCell(0,ii) ) == 0 )
			{
				Sint32 n = atoi( pCsv->GetCell(1,ii) );
				sprintf( pCsvName , "%s" , pCsv->GetCell(2,ii) );

				Uint8 *pZipData = pZA->Decode( pCsvName, &uZipSize );
				CCsv *pCsv2 = new CCsv();

				pCsv2->ReadFile( pZipData, uZipSize);
				cnvCSVtoAnimData(pCsv2, &m_Animes[ n ] );
				SAFE_DELETE(pCsv2);
				SAFE_DELETES(pZipData);
			}
		}

		//テクスチャデータの読み込みと設定

		Uint32 uZipSize2;
		Sint32 tgtPage = tpg;
		gxBool bFirst = gxTrue;

		for (Sint32 ii = 0; ii < m_Animes[0].imageNum; ii++)
		{
			Uint8* pZipData = pZA->Decode(m_Animes[0].pImageList[ii].name, &uZipSize2);

			CFileTarga tga;
			tga.ReadFile(pZipData, uZipSize2);

			if (!bFirst)
			{
				if (tga.GetWidth() > 1024 || tga.GetHeight() > 1024 )
				{
					tgtPage += 64;
				}
				else
				{
					tgtPage += 16;
				}
			}
			else
			{
				bFirst = gxFalse;
			}
			m_Animes[0].pTexturePageIndex[ii] = tgtPage;

			gxLib::ReadTexture( tpg, tga.GetFileImage(), tga.GetFileSize() );
			SAFE_DELETES(pZipData);

			tpg += 16;
		}

		gxLib::UploadTexture();

		SAFE_DELETES(p);
		SAFE_DELETE(pCsv);
		SAFE_DELETE( pZA );

		//m_Header = m_Animes[0];

		return gxTrue;
	}

	return gxFalse;
}

gxBool CSpriteStudioManager::cnvCSVtoAnimData( CCsv* pCsv, SSHeader *pHead )
{
	//CSVからアニメデータを抜き出す

	//まずデータのスキャンをする
	Sint32 anmDataNum = 0;
	Sint32 idxDataNum = 0;
	Sint32 imgDataNum = 0;

	for( Sint32 ii=0; ii<pCsv->GetHeight(); ii++ )
	{
		gxChar *pString = pCsv->GetCell( 0 , ii );

		if( pString[0] == 0x00 ) continue;

		if( strcmp( pString , "frm" ) == 0 )
		{
			//フレームデータを発見
			anmDataNum++;
		}
		else if( strcmp( pString , "index" ) == 0 )
		{
			//インデックスデータを発見
			idxDataNum ++;
		}
		else if( strcmp( pString , "images" ) == 0 )
		{
			imgDataNum ++;
		}
		else if( strcmp( pString , "fps" ) == 0 )
		{
			pString = pCsv->GetCell( 1 , ii );
			pHead->fps = atof( pString );

		}
		else if( strcmp( pString , "CanvasWidth" ) == 0 )
		{
			pString = pCsv->GetCell( 1 , ii );
			pHead->w = atof( pString );
		}
		else if( strcmp( pString , "CanvasHeight" ) == 0 )
		{
			pString = pCsv->GetCell( 1 , ii );
			pHead->h = atof( pString );
		}
		else if( strcmp( pString , "MarginWidth" ) == 0 )
		{
			pString = pCsv->GetCell( 1 , ii );
			pHead->merginW = atoi( pString );
		}
		else if( strcmp( pString , "MarginHeight" ) == 0 )
		{
			pString = pCsv->GetCell( 1 , ii );
			pHead->merginH = atoi( pString );
		}
	}

	pHead->animeNum = anmDataNum;
	pHead->imageNum = imgDataNum;
	pHead->indexNum = idxDataNum;

	pHead->pImageList = new SSImageList[imgDataNum];
	pHead->pAnimList  = new SSAnimeList[anmDataNum];
	pHead->pIndexList = new SSIndexList[idxDataNum];
	pHead->pTexturePageIndex = new Sint32[imgDataNum];
	

	anmDataNum = 0;
	idxDataNum = 0;
	imgDataNum = 0;

	for( Sint32 ii=0; ii<pCsv->GetHeight(); ii++ )
	{
		gxChar *pString = pCsv->GetCell( 0 , ii );

		if( pString[0] == 0x00 ) continue;

		if( strcmp( pString , "frm" ) == 0 )
		{
			//フレームデータを発見
			SSAnimeList *p = &pHead->pAnimList[anmDataNum];
			p->frm    = atoi( pCsv->GetCell( 1 , ii ) );
			p->ID     = atoi( pCsv->GetCell( 2 , ii ) );
			p->texID  = atoi( pCsv->GetCell( 3 , ii ) );
			p->u      = atof( pCsv->GetCell( 4 , ii ) );
			p->v      = atof( pCsv->GetCell( 5 , ii ) );
			p->w      = atof( pCsv->GetCell( 6 , ii ) );
			p->h      = atof( pCsv->GetCell( 7 , ii ) );
			p->vx1    = atof( pCsv->GetCell( 8 , ii ) );
			p->vy1    = atof( pCsv->GetCell( 9 , ii ) );
			p->argb1  = atoi( pCsv->GetCell( 10, ii ) );
			p->vx2    = atof( pCsv->GetCell( 11, ii ) );
			p->vy2    = atof( pCsv->GetCell( 12, ii ) );
			p->argb2  = atoi( pCsv->GetCell( 13, ii ) );
			p->vx3    = atof( pCsv->GetCell( 14, ii ) );
			p->vy3    = atof( pCsv->GetCell( 15, ii ) );
			p->argb3  = atoi( pCsv->GetCell( 16, ii ) );
			p->vx4    = atof( pCsv->GetCell( 17, ii ) );
			p->vy4    = atof( pCsv->GetCell( 18, ii ) );
			p->argb4  = atoi( pCsv->GetCell( 19, ii ) );
			p->aType  = atoi( pCsv->GetCell( 20, ii ) );
			p->alpha  = atof( pCsv->GetCell( 21, ii ) );
			p->bType  = atoi( pCsv->GetCell( 22, ii ) );
			p->blend  = atof( pCsv->GetCell( 23, ii ) );
			p->flip_x = atoi( pCsv->GetCell( 24, ii ) );
			p->flip_y = atoi( pCsv->GetCell( 25, ii ) );

			p->dummy[0] = 0;
			p->dummy[1] = 0;
			p->dummy[2] = 0;
			p->dummy[3] = 0;
			p->dummy[4] = 0;
			p->dummy[5] = 0;

			anmDataNum++;
		}
		else if( strcmp( pString , "index" ) == 0 )
		{
			//インデックスデータを発見
			pString = pCsv->GetCell( 1 , ii );
			Sint32 frm = atoi( pString );

			SSIndexList *p = &pHead->pIndexList[ frm ];

			p->start    = atoi( pCsv->GetCell( 2 , ii ) );
			p->partsNum = atoi( pCsv->GetCell( 3 , ii ) );

			idxDataNum ++;
		}
		else if( strcmp( pString , "images" ) == 0 )
		{
			//イメージデータを発見
			pString = pCsv->GetCell( 1 , ii );
			Sint32 idx = atoi( pString );

			SSImageList *p = &pHead->pImageList[ idx ];

			pString = pCsv->GetCell( 2 , ii );
			sprintf(p->name, "%s", pString);
			imgDataNum ++;
		}
	}

	return gxTrue;

}

