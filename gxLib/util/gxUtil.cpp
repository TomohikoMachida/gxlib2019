//--------------------------------------------------
//
// gxTexdManager.h
// CRC値や、円と円、線と線の当たり判定など
// よく使うコードをまとめています
//
//--------------------------------------------------

#include <gxLib.h>
#include "../gx.h"
#include "../gxNetworkManager.h"
#include "../gxFileManager.h"
#include "../gxPadManager.h"

gxUtil::CKeyBoard gxUtil::m_KeyBoard;
gxUtil::CJoyPad   gxUtil::m_JoyPad[PLAYER_MAX];
gxUtil::CTouch    gxUtil::m_Touch[GX_TOUCH_MAX];

extern gxChar *JoyStringTbl[];
extern gxChar *KeyStringTbl[];

Uint32 gxUtil::atox( gxChar *p )
{
	int n,ret=0;

	for(int i=gxUtil::StrLen(p)-1,j=0 ;i>=0;i--,j++)
	{	
		switch(p[i]){
		 case '0':  n=0;	break;
		 case '1':  n=1;	break;
		 case '2':  n=2;	break;
		 case '3':  n=3;	break;
		 case '4':  n=4;	break;
		 case '5':  n=5;	break;
		 case '6':  n=6;	break;
		 case '7':  n=7;	break;
		 case '8':  n=8;	break;
		 case '9':  n=9;	break;

		 case 'A':  case 'a':   n=10;   break;
		 case 'B':  case 'b':   n=11;   break;
		 case 'C':  case 'c':   n=12;   break;
		 case 'D':  case 'd':   n=13;   break;
		 case 'E':  case 'e':   n=14;   break;
		 case 'F':  case 'f':   n=15;   break;
		 default:
			return 0;
		}

		ret |= (n<<(4*j));
	}

	return ret;
}

gxBool gxUtil::GetFileNameWithoutPath( gxChar * in , gxChar *out)
{
	//ファイル名だけを取り出す

	for(int i=gxUtil::StrLen(in);i>0;i--)
	{
		if( in[i]=='\\' || in[i]=='/')
		{
			sprintf( out,"%s",&in[i+1]);
			return gxTrue;
		}
	}

	sprintf(out, "%s", in);
	return gxFalse;
}


gxBool gxUtil::GetPath( gxChar *in , gxChar *out )
{
	//ファイルのパスだけを取り出す

	for(int i=gxUtil::StrLen(in);i>0;i--)
	{
		if( in[i]=='\\' || in[i]=='/')
		{
			sprintf(out,"%s",&in[0]);
			out[i] = 0x00;
			return gxTrue;
		}
	}
	return gxFalse;
}

gxBool gxUtil::GetExt( gxChar *in , gxChar* out )
{
	//拡張子を得る

	for(int i=gxUtil::StrLen(in);i>=0;i--)
	{
		if(in[i]=='.')
		{
			sprintf(out,"%s",&in[i]);
			//out[i] = 0x00;
			return gxTrue;
		}
	}

	return gxFalse;

}

gxBool gxUtil::GetFileNameWithoutExt( gxChar *in , gxChar *out)
{
	//拡張子をカットする

	for(int i=gxUtil::StrLen(in);i>0;i--)
	{
		if(in[i]=='.')
		{
			sprintf(out,"%s",in);
			out[i] = 0x00;
			return gxTrue;
		}
	}

	out[0] = 0x00;
	return gxFalse;
}




Float32 gxUtil::GetTargetRotation( Float32 target_angle , Float32 my_angle )
{
	//自分の向いている方向に対して
	//ターゲット角度を－１８０～０～１８０の角度で返す
	Float32 sabun;

	while(my_angle>360){
		my_angle-=360;
	}
	while(my_angle<0){
		my_angle+=360;
	}
	while(target_angle>360){
		target_angle-=360;
	}
	while(target_angle<0){
		target_angle+=360;
	}

	sabun = target_angle-my_angle;

	if(sabun<-180){
		sabun = 360+sabun;
	}else if(sabun>180){
		sabun = (180-(sabun-180))*-1;
	}

	return sabun;

}


void gxUtil::StrUpr( gxChar* pStr )
{
	size_t len = gxUtil::StrLen(pStr);

	for(size_t ii=0; ii<len; ii++ )
	{
		if (pStr[ii] < 0x80)
		{
			pStr[ii] = toupper(pStr[ii]);
		}
	}
}


size_t gxUtil::StrLen( gxChar* pStr )
{
	size_t len = strlen(pStr);

	return len;
}


void gxUtil::MemSet( void* pMem ,Uint8 val , size_t sz )
{
	Uint8 *pMem8 = (Uint8*)pMem;

	for(size_t ii=0; ii<sz; ii++ )
	{
		pMem8[ii] = val;
	}
}


void gxUtil::MemCpy(void* pDst , void* pSrc , size_t sz )
{

	Uint32 *pMemSrc = (Uint32*)pSrc;
	Uint32 *pMemDst = (Uint32*)pDst;

	for(size_t ii=0; ii<sz/4; ii++ )
	{
		pMemDst[ii] = pMemSrc[ii];
	}

	Uint8 *pMemSrc8 = (Uint8*)pSrc;
	Uint8 *pMemDst8 = (Uint8*)pDst;

	Uint32 sz2 = (sz/4)*4;
	for(size_t ii=sz2; ii<sz; ii++ )
	{
		pMemDst8[ii] = pMemSrc8[ii];
	}

}


//多角形を描画します
Uint32 gxUtil::DrawPolygon(
		Sint32 num,
		gxVector2D *pXY,
		gxVector2D *pUV,
		Uint32 *argb,
		Sint32 prio,
		Uint32 atr ,
		Sint32 page,
		gxBool bFill )
{

	//三角形分割

	//

	return 0;
}



Uint32 gxUtil::PutFreeSprite(
	Sint32 x,Sint32 y,Sint32 prio,
	gxVector2D *pXY,
	Sint32 tpg,
	Sint32 u,Sint32 v , Sint32 w , Sint32 h,
	Sint32 divNum,
	Uint32 atr,
	Uint32  argb,
	Float32 fRot,
	Float32 fx,
	Float32 fy
	)
{
	gxPoint pos[4];

	Sint32 div = divNum+1;

	if( div < 2  ) div = 2;
	if( div > 16 ) div = 16;

	gxBool bDebug = gxFalse;
	Uint32 debARGB = 0xff00ff00;

	for( Sint32 ii=0; ii<4; ii++ )
	{
		pos[ii].x = pXY[ii].x;
		pos[ii].y = pXY[ii].y;

		pos[ii].x *= fx;
		pos[ii].y *= fy;

		gxUtil::RotationPoint( &pos[ii] , fRot );

		if( bDebug )
		{
			gxLib::DrawBox( pos[ii].x -3 +x, pos[ii].y -3+y , pos[ii].x +3 +x, pos[ii].y +3 +y, MAX_PRIORITY_NUM,gxTrue , ATR_DFLT , 0xffffffff );
		}
	}

	Sint32 max = div*div;

	gxPoint* pMat = new gxPoint[max];
	gxPoint* pTx  = new gxPoint[max];

	//四辺の分割座標を確定

	for( Sint32 ii=0;ii<div;ii++ )
	{
		pMat[ ii ].x = pos[0].x + 1.0f * ii * ( pos[1].x - pos[0].x ) /( div-1 );
		pMat[ ii ].y = pos[0].y + 1.0f * ii * ( pos[1].y - pos[0].y ) /( div-1 );

		pMat[ (div-1)*div+ii ].x = pos[3].x + 1.0f * ii * ( pos[2].x - pos[3].x ) /( div-1 );
		pMat[ (div-1)*div+ii ].y = pos[3].y + 1.0f * ii * ( pos[2].y - pos[3].y ) /( div-1 );

		pMat[ ii*div ].x = pos[0].x + 1.0f * ii * ( pos[3].x - pos[0].x ) /( div -1 );
		pMat[ ii*div ].y = pos[0].y + 1.0f * ii * ( pos[3].y - pos[0].y ) /( div -1 );

		pMat[ ii*div+(div-1) ].x = pos[1].x + 1.0f * ii * ( pos[2].x - pos[1].x ) /( div -1 );
		pMat[ ii*div+(div-1) ].y = pos[1].y + 1.0f * ii * ( pos[2].y - pos[1].y ) /( div -1 );

		if( bDebug )
		{
			gxLib::DrawPoint( pMat[ ii             ].x+x , pMat[ ii                ].y+y , prio+1 ,ATR_DFLT , debARGB );
			gxLib::DrawPoint( pMat[ (div-1)*div+ii ].x+x , pMat[(div - 1)*div + ii ].y+y , prio+1 ,ATR_DFLT , debARGB );
			gxLib::DrawPoint( pMat[ ii*div         ].x+x , pMat[ ii*div ].y+y            , prio+1 ,ATR_DFLT , debARGB );
			gxLib::DrawPoint( pMat[ ii*div+(div-1) ].x+x , pMat[ ii*div+(div-1) ].y+y    , prio+1 ,ATR_DFLT , debARGB );
		}
	}

	//中の補完座標を策定

	for( Sint32 ii=1;ii<div-1;ii++ )
	{
		for( Sint32 xx=1;xx<div-1;xx++ )
		{
			Float32 x1 = pMat[ ii*div+0 ].x;
			Float32 y1 = pMat[ ii*div+0 ].y;
			Float32 x2 = pMat[ ii*div+(div-1) ].x;
			Float32 y2 = pMat[ ii*div+(div-1) ].y;

			pMat[ ii*div+xx ].x = x1 + xx * (x2-x1)/(div-1);
			pMat[ ii*div+xx ].y = y1 + xx * (y2-y1)/(div-1);

			if( bDebug )
			{
				gxLib::DrawPoint( pMat[ ii*div+xx ].x+x , pMat[ ii*div+xx ].y+y , prio+1 ,ATR_DFLT , debARGB );
			}
		}
	}

	for( Sint32 yy=0;yy<div;yy++)
	{
		for( Sint32 xx=0;xx<div;xx++)
		{
			pTx[ yy*div + xx ].x = u + 1.0f * xx *  w  / (div-1);
			pTx[ yy*div + xx ].y = v + 1.0f * yy *  h  / (div-1);
		}
	}


	for( Sint32 yy=0;yy<div-1;yy++)
	{
		for( Sint32 xx=0;xx<div-1;xx++)
		{
			Sint32 p1,p2,p3,p4;
			p1 = yy*div + xx;
			p2 = yy*div + xx+1;
			p3 = yy*div + xx+1+div;
			p4 = (yy+1)*div + xx;


			if( bDebug )
			{
				gxLib::DrawTriangle(
						pMat[ p1 ].x+x , pMat[ p1 ].y+y ,
						pMat[ p2 ].x+x , pMat[ p2 ].y+y ,
						pMat[ p4 ].x+x , pMat[ p4 ].y+y ,
						prio+1 , gxFalse,ATR_DFLT , debARGB );

				gxLib::DrawTriangle(
						pMat[ p2 ].x+x , pMat[ p2 ].y+y ,
						pMat[ p4 ].x+x , pMat[ p4 ].y+y ,
						pMat[ p3 ].x+x , pMat[ p3 ].y+y ,
						prio+1 ,gxFalse,ATR_DFLT , debARGB );
			}

			gxLib::PutTriangle(
					pMat[ p1 ].x+x , pMat[ p1 ].y+y ,	pTx[p1].x , pTx[p1].y,
					pMat[ p2 ].x+x , pMat[ p2 ].y+y ,	pTx[p2].x , pTx[p2].y,
					pMat[ p4 ].x+x , pMat[ p4 ].y+y ,	pTx[p4].x , pTx[p4].y,
					prio , tpg,
					atr , argb );

			gxLib::PutTriangle(
					pMat[ p2 ].x+x , pMat[ p2 ].y+y ,	pTx[p2].x , pTx[p2].y,
					pMat[ p4 ].x+x , pMat[ p4 ].y+y ,	pTx[p4].x , pTx[p4].y,
					pMat[ p3 ].x+x , pMat[ p3 ].y+y ,	pTx[p3].x , pTx[p3].y,
					prio , tpg,
					atr , argb );

		}
	}


	SAFE_DELETES( pMat );
	SAFE_DELETES( pTx );

	return 0;
}


void gxUtil::DrawSonicBoom( Sint32 x , Sint32 y , Sint32 prio , Float32 fRadius  , Float32 fWidth , Float32 fDistPow, Uint32 tpg , Uint32 atr,Uint32 argb )
{
	//Float32 fRadius・・・・円の半径
	//Float32 fWidth ・・・・縁の大きさ（ゼロの場合は中心からFill）
	//Float32 fDistortion・・歪みの強度（0～1.0f）
	//Sint32 x , Sint32 y , Sint32 prio	・・・中心位置
	//Uint32 tpg ・・・使用するテクスチャページ（通常はTEXPAGE_CAPTURE）, Uint32 atr , Uint32 argb , Float32 fRadius , Float32 fWidth , Float32 fDistortion )

	fDistPow = CLAMP(fDistPow, 0.0f, 1.0f);
	fWidth   = CLAMP(fWidth, 0.0f, fRadius*2);

	Float32 fRot = 0.0f;
	Sint32 div = 32;
	Float32 fDistortion = 45 * fDistPow;

	gxRect  *pRectV = new gxRect[div];;
	gxRect  *pRectUV = new gxRect[div];;

	Float32 fx1,fy1;
	Float32 fx2,fy2;

	for( Sint32 ii=0; ii<div;ii++ )
	{
		fRot = (360.0f / div)*ii;
		fx1 = gxUtil::Cos( fRot );
		fy1 = gxUtil::Sin( fRot );
		fx2 = gxUtil::Cos( fRot+fDistortion );
		fy2 = gxUtil::Sin( fRot+fDistortion );

		if( fWidth <= 0.0f )
		{
			pRectV[ii].x1 = 0.0f;
			pRectV[ii].y1 = 0.0f;
			pRectV[ii].x2 = fx1*fRadius;
			pRectV[ii].y2 = fy1*fRadius;

			pRectUV[ii].x1 = fx1 * fRadius*0.1f;
			pRectUV[ii].y1 = fx1 * fRadius*0.1f;
			pRectUV[ii].x2 = fx1 * fRadius*1.1f;
			pRectUV[ii].y2 = fy1 * fRadius * 1.1f;
		}
		else
		{
			pRectV[ii].x1 = fx1 * (fRadius - fWidth / 2);
			pRectV[ii].y1 = fy1 * (fRadius - fWidth / 2);
			pRectV[ii].x2 = fx1 * (fRadius + fWidth / 2);
			pRectV[ii].y2 = fy1 * (fRadius + fWidth / 2);

			pRectUV[ii].x1 = fx2 * (fRadius - fWidth / 2);
			pRectUV[ii].y1 = fy2 * (fRadius - fWidth / 2);
			pRectUV[ii].x2 = fx2 * (fRadius + fWidth / 2);
			pRectUV[ii].y2 = fy2 * (fRadius + fWidth / 2);
		}
	}


	Sint32 x1,y1,x2,y2;
	Sint32 x3,y3,x4,y4;

	Sint32 u1,v1,u2,v2;
	Sint32 u3,v3,u4,v4;
	for( Sint32 ii=0; ii<div;ii++ )
	{
		Sint32 n1 = ii;
		Sint32 n2 = (ii+1)%div;
		Sint32 n3 = (ii+2)%div;

		//Sint32 m1 = ii;
		//Sint32 m2 = (ii+1)%div;
		//Sint32 m3 = (ii+2)%div;

		if (fWidth <= 0.0f)
		{
			x1 = pRectV[n1].x1 + x;
			y1 = pRectV[n1].y1 + y;
			x2 = pRectV[n1].x2 + x;
			y2 = pRectV[n1].y2 + y;
			x3 = pRectV[n2].x1 + x;
			y3 = pRectV[n2].y1 + y;
			x4 = pRectV[n2].x2 + x;
			y4 = pRectV[n2].y2 + y;


			u1 = pRectUV[n1].x1 + x;
			v1 = pRectUV[n1].y1 + y;
			u2 = pRectUV[n1].x2 + x;
			v2 = pRectUV[n1].y2 + y;
			u3 = pRectUV[n2].x1 + x;
			v3 = pRectUV[n2].y1 + y;
			u4 = pRectUV[n2].x2 + x;
			v4 = pRectUV[n2].y2 + y;
		}
		else
		{
			x1 = pRectV[n1].x1 + x;
			y1 = pRectV[n1].y1 + y;
			x2 = pRectV[n1].x2 + x;
			y2 = pRectV[n1].y2 + y;
			x3 = pRectV[n2].x1 + x;
			y3 = pRectV[n2].y1 + y;
			x4 = pRectV[n2].x2 + x;
			y4 = pRectV[n2].y2 + y;


			u1 = pRectUV[n1].x1 + x;
			v1 = pRectUV[n1].y1 + y;
			u2 = pRectV[n1].x2 + x;
			v2 = pRectV[n1].y2 + y;
			u3 = pRectUV[n2].x1 + x;
			v3 = pRectUV[n2].y1 + y;
			u4 = pRectV[n2].x2 + x;
			v4 = pRectV[n2].y2 + y;

		}


		gxLib::PutTriangle(
			x1,y1,u1,v1,
			x2,y2,u2,v2,
			x3,y3,u3,v3,
			prio,
			tpg,
			atr , argb );

		gxLib::PutTriangle(
			x2,y2,u2,v2,
			x4,y4,u4,v4,
			x3,y3,u3,v3,
			prio,
			tpg,
			atr , argb );
	}
	SAFE_DELETES(pRectV);
	SAFE_DELETES(pRectUV);
}


Sint32 gxUtil::OpenWebFile( gxChar *pURL , gxChar *pUserName , gxChar *pPassWord )
{
	Sint32 index = 0;

	index = gxNetworkManager::GetInstance()->OpenURL( pURL );

	return index;
}


gxBool gxUtil::CloseWebFile(Uint32 uIndex)
{
	gxNetworkManager::GetInstance()->CloseURL(uIndex);

	return gxTrue;
}


Sint32 gxUtil::IsDownloadWebFile( Uint32 index , Float32 *pRatio )
{
	gxNetworkManager::StHTTP *http;

	http = gxNetworkManager::GetInstance()->GetHTTPInfo(index );

	if( pRatio )
	{
		size_t max = http->uFileSize;
		size_t now = http->uReadFileSize;

		if (now == 0 || max == 0)
		{
			*pRatio = 0.0f;
		}
		else if (now == max)
		{
			*pRatio = 100.0f;
		}
		else
		{
			size_t ratio = 10000 * now / max;

			Float32 fRatio = ratio / 100.0f;

			*pRatio = fRatio;
		}
	}

	if ( http->IsNowLoading() ) 
	{
        return gxFalse;
	}

	return gxTrue;
}


gxBool gxUtil::IsDownloadSucceeded(Uint32 index)
{
	gxNetworkManager::StHTTP *http;

	http = gxNetworkManager::GetInstance()->GetHTTPInfo(index);

	size_t max = http->uFileSize;
	size_t now = http->uReadFileSize;

	if (!http->IsNowLoading())
	{
		if (now >= max && max > 0 )
		{
			return gxTrue;
		}
	}

	return gxFalse;
}


Uint8* gxUtil::GetDownloadWebFile( Uint32 uIndex , size_t* uSize )
{
	gxNetworkManager::StHTTP *http;
	http = gxNetworkManager::GetInstance()->GetHTTPInfo( uIndex  );

	*uSize = http->uFileSize;

	return http->pData;
}

gxBool gxUtil::IsSameEXT( gxChar *in1 , gxChar* in2 )
{
	//同じ拡張子か？
	gxChar ext1[256] = {0};
	gxChar ext2[256] = {0};

	GetExt( in1 ,ext1 );
	GetExt( in2 ,ext2 );

	StrUpr( ext1 );
	StrUpr( ext2 );

	if( strcmp( ext1 , ext2 ) == 0 )
	{
		return gxTrue;
	}

	return gxFalse;
}


Sint32 gxUtil::LoadFile( gxChar* pFileName , ESTORAGE_LOCATION location , std::function<void( Sint32 id , Uint8 *pData , Uint32 uSize )>func )
{
	Sint32 id = gxFileManager::GetInstance()->LoadReq( pFileName , STORAGE_LOCATION_AUTO, [pFileName, func](Sint32 id)
	{
		Uint8* pData = gxFileManager::GetInstance()->GetFileAddr( id );
		Uint32 uSize = gxFileManager::GetInstance()->GetFileSize( id );

		if( func )
		{
			func(id,pData,uSize);
		}

		if( pData == nullptr || uSize == 0 )
		{
			GX_DEBUGLOG("Failed! .. gxUtil::LoadTextureASync(\"%s\");", pFileName);
			return;
		}
	});

	//Sint32 index = gxFileManager::GetInstance()->LoadReq( pFileName , location );	//card&rom&dvd捜索
	//GX_DEBUGLOG("gxUtil::LoadFile(\"%s\");",pFileName );

	return id;
}

gxBool gxUtil::IsLoadFile( Sint32 id , Uint8 **pData , size_t* pLength )
{
	if( !gxFileManager::GetInstance()->IsLoadEnd( id ) )
	{
		return gxFalse;
	}

	*pLength = gxFileManager::GetInstance()->GetFileSize(id);
	*pData   = gxFileManager::GetInstance()->GetFileAddr(id);

	gxFileManager::GetInstance()->Clear( id );

	if( *pData == NULL )
	{
		GX_DEBUGLOG("[gxLib::Error]gxUtil::LoadFile　読み込み失敗(\"%d\");" , id );
		return gxFalse;
	}

	return gxTrue;
}

gxBool gxUtil::IsNowLoading()
{
	return gxFileManager::GetInstance()->IsLoadTaskExist();
}

//テクスチャをファイルからマスターテクスチャへ読み込みます
gxBool gxUtil::LoadTexture ( Uint32 texPage , gxChar* pFileName, Uint32 colorKey ,Uint32 ox , Uint32 oy )
{
/*
Sint32 id = gxFileManager::GetInstance()->LoadReq( pFileName , STORAGE_LOCATION_AUTO , [pFileName,texPage,colorKey,ox,oy](Sint32 id)
		{
			Uint8* pData = gxFileManager::GetInstance()->GetFileAddr( id );
			Uint32 uSize = gxFileManager::GetInstance()->GetFileSize( id );

			if( pData == nullptr || uSize == 0 )
			{
				GX_DEBUGLOG("Failed! .. gxUtil::LoadTextureASync(\"%s\");", pFileName);
				return;
			}

			gxLib::ReadTexture( texPage , pData , uSize , colorKey , ox , oy);
			GX_DEBUGLOG("gxUtil::LoadTextureASync(\"%s\");", pFileName);
		});
*/
		gxUtil::LoadFile(pFileName, STORAGE_LOCATION_AUTO, [pFileName, texPage, colorKey, ox, oy](Sint32 id, Uint8* pData, Uint32 uSize)
		{
			if (pData == nullptr || uSize == 0)
			{
				GX_DEBUGLOG("Failed! .. gxUtil::LoadTextureASync(\"%s\");", pFileName);
				return;
			}

			gxLib::ReadTexture(texPage, pData, uSize, colorKey, ox, oy);
			GX_DEBUGLOG("gxUtil::LoadTextureASync(\"%s\");", pFileName);
			SAFE_DELETES(pData);

		});

	return gxTrue;
}

struct FileReq {
	Uint32 uIndex = 0;
	Uint8 *pData;
	Uint32 uSize;
};

void* SoundUpload( void *p )
{
	FileReq *q = (FileReq*)p;

	gxLib::ReadAudio(q->uIndex, q->pData, q->uSize);

	return nullptr;
}

//サウンドファイルをファイルから指定バンクに読み込みます
gxBool gxUtil::LoadAudio( Uint32 uIndex , gxChar* pFileName , ... )
{
	gxChar temp[1024];

	va_list app;
	va_start(app, pFileName);
	vsprintf(temp, pFileName, app);
	va_end(app);

	
		gxUtil::LoadFile(temp, STORAGE_LOCATION_AUTO, [temp, uIndex](Sint32 id, Uint8* pData, Uint32 uSize)
		{
			if (pData == nullptr || uSize == 0)
			{
				GX_DEBUGLOG("Failed! .. gxUtil::LoadAudioASync(\"%s\");", temp);
				return;
			}

/*
static FileReq req;
			req.pData = pData;
			req.uIndex = uIndex;
			req.uSize = uSize;
			gxLib::CreateThread( SoundUpload , (void*)&req );
*/
			gxLib::ReadAudio(uIndex, pData, uSize);
			GX_DEBUGLOG("gxUtil::LoadAudioASync(\"%s\");", temp);
			SAFE_DELETES(pData);

		});


	return gxTrue;
}

gxBool gxUtil::IsLoadComplete()
{
	return !gxFileManager::GetInstance()->IsLoadTaskExist();
}



std::string gxUtil::GetUpperPath(std::string path)
{
	size_t sz = path.size();

	std::string path2 = path;
	char buf[512];

	sprintf(buf, "%s", path.c_str());

	for (size_t ii = sz-1-1; ii > 0; ii--)
	{
		if (buf[ii] == '\\' || buf[ii] == '/')
		{
			buf[ii+1] = 0x00;
			break;
		}
	}

	path2 = buf;

	return path2;

}
std::string gxUtil::GetRelativePath(gxChar* pCurrentPath, gxChar* path)
{
	std::string path1 = pCurrentPath;
	std::string path2 = path;

	int num = path2.find( path1 );

	if (num == std::string::npos)
	{
		return path;
	}

	char buf[512];
	sprintf(buf, "%s", path);

	size_t sz = strlen(pCurrentPath);
	if (buf[num + sz] == '\\' || buf[num + sz] == '/')
	{
		sz++;
	}
	return &buf[num + sz];
}

std::string gxUtil::GetMD5(Uint8 *pData, size_t sz)
{
	MD5 md5;
	md5.update( (uint8_t*)pData, (uint8_t*)((uint8_t*)pData + (sz)));

	std::string str;
	md5.hex_digest(str);
	return str;
	//return md5.GetHash();
}


gxUtil::CJoyPad::CJoyPad()
{
	for( Sint32 ii=0; ii<PLAYER_MAX; ii++ )
	{
		m_JoyPad[ii].id = ii;
	}
}


Float32 gxUtil::CJoyPad::GetRotationAngle(Float32 *pRot )
{
    Float32 fDist = gxUtil::Distance( gxLib::Joy(0)->lx , gxLib::Joy(0)->ly );

    gxBool bPush = gxFalse;
    
    Float32 fRot = 0.0f;

    if( fDist > 0.2f )
    {
        bPush = gxTrue;
        fRot = gxUtil::Atan( gxLib::Joy(0)->lx , gxLib::Joy(0)->ly );
        fRot -= 90.0f;
        NORMALIZE(fRot);
    }
    else
    {
        bPush = gxTrue;
        switch(gxLib::Joy(0)->psh&(JOY_U|JOY_D|JOY_R|JOY_L)){
        case JOY_U:
            fRot = 0.0f;
            break;
        case JOY_U|JOY_R:
            fRot = 45.0f;
            break;
        case JOY_R:
            fRot = 90.0f;
            break;
        case JOY_R|JOY_D:
            fRot = 135.0f;
            break;
        case JOY_D:
            fRot = 180.0f;
            break;
        case JOY_D|JOY_L:
            fRot = 225.0f;
            break;
        case JOY_L:
            fRot = 270.0f;
            break;
        case JOY_U|JOY_L:
            fRot = 315.0f;
            break;
        default:
            bPush = gxFalse;
            break;
        }
    }
    
    *pRot = fRot;

    if( !bPush ) return gxFalse;

    return gxTrue;
}

gxBool gxUtil::CJoyPad::IsTrigger( Uint32 key )
{
	if( gxLib::Joy(id)->trg&key ) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CJoyPad::IsDoubleClick(Uint32 key)
{
	if (gxLib::Joy(id)->dcl & key) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CJoyPad::IsLongTap(Uint32 key)
{
	if (gxLib::Joy(id)->tap & key) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CJoyPad::IsPush( Uint32 key )
{
	if( gxLib::Joy(id)->psh&key ) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CJoyPad::IsRepeat( Uint32 key )
{
	if( gxLib::Joy(id)->rep&key ) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CJoyPad::IsRelease( Uint32 key )
{
	if( gxLib::Joy(id)->rls&key ) return gxTrue;
	return gxFalse;
}


gxPoint* gxUtil::CJoyPad::GetLeftStick()
{
	m_LeftStick.x = gxLib::Joy(id)->lx;
	m_LeftStick.y = gxLib::Joy(id)->ly;
	return &m_LeftStick;
}

gxPoint* gxUtil::CJoyPad::GetRightStick()
{
	m_LeftStick.x = gxLib::Joy(id)->rx;
	m_LeftStick.y = gxLib::Joy(id)->ry;

	return &m_LeftStick;
}


gxChar* gxUtil::CJoyPad::GetString( EJoyBit btn )
{
	//ボタン名を返す

	Uint8 index = gxPadManager::GetInstance()->CnvJoytoIndex( btn );

	return JoyStringTbl[index];
}

gxUtil::CKeyBoard* gxUtil::KeyBoard()
{
	//キーボード構造体を返す

	return &gxUtil::m_KeyBoard;
}


gxUtil::CJoyPad* gxUtil::JoyPad( Uint32 player )
{
	//プレイヤーを指定したジョイパッド構造体を返す

	return &m_JoyPad[player];
}

gxUtil::CTouch* gxUtil::Touch(Uint32 id)
{
	m_Touch[id].id = id;

	return &m_Touch[id];
}

gxBool gxUtil::CKeyBoard::IsTrigger( Uint32 key )
{
	//コントローラーのボタンが押された瞬間か？

	if( gxLib::KeyBoard(key)&enStatTrig ) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CKeyBoard::IsPush( Uint32 key )
{
	//コントローラーのボタンは押しっぱなしか？

	if( gxLib::KeyBoard(key)&enStatPush ) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CKeyBoard::IsRepeat( Uint32 key )
{
	//コントローラーのボタンがリピートしているか？

	if( gxLib::KeyBoard(key)&enStatRepeat ) return gxTrue;
	return gxFalse;
}

gxBool gxUtil::CKeyBoard::IsRelease( Uint32 key )
{
	//コントローラーのボタンが離されたか？

	if( gxLib::KeyBoard(key)&enStatRelease ) return gxTrue;
	return gxFalse;
}

gxKey::KeyType gxUtil::CKeyBoard::GetInputKey()
{
	//キーボードが押されたことを検出する

	for(Uint32 ii=0; ii<gxKey::KEYMAX; ii++ )
	{
		
		if( gxLib::KeyBoard( (gxKey::KeyType)ii )&enStatTrig )
		{
			return (gxKey::KeyType)ii;
		}
	}
	return gxKey::KEYNONE;
}


gxChar* gxUtil::CKeyBoard::GetKeyString( gxKey::KeyType key )
{
	//gxKeyに対応したキーボードの文字を返す

	if( key >= gxKey::KEYMAX ) return "-----";

	return KeyStringTbl[key];
};


gxBool gxUtil::CTouch::IsTrigger()
{

	Uint32 tbl[]={	MOUSE_L,	MOUSE_R,	MOUSE_M };

	return (gxLib::Joy(0)->trg&tbl[id])? gxTrue : gxFalse;
}

gxBool gxUtil::CTouch::IsPush()
{
	Uint32 tbl[]={	MOUSE_L,	MOUSE_R,	MOUSE_M };
	if (gxLib::Joy(0)->psh & MOUSE_L)
	{
		Sint32 mx = 0;
		mx++;
	}
	Uint32 psh = gxLib::Joy(0)->psh;

	return (psh&tbl[id])? gxTrue : gxFalse;
}

gxBool gxUtil::CTouch::IsRepeat()
{
	Uint32 tbl[]={	MOUSE_L,	MOUSE_R,	MOUSE_M };

	return (gxLib::Joy(0)->rep&tbl[id]) != 0;
}

gxBool gxUtil::CTouch::IsRelease()
{
	Uint32 tbl[]={	MOUSE_L,	MOUSE_R,	MOUSE_M };

	if ((gxLib::Joy(0)->rls & tbl[id]))
	{
		int nnn = 0;
		nnn++;
	}
	return (gxLib::Joy(0)->rls&tbl[id]) != 0;
}

gxBool gxUtil::CTouch::IsDoubleTap()
{
	return (gxLib::Joy(0)->dcl & MOUSE_L) != 0;
}

gxBool gxUtil::CTouch::IsLongTap()
{
	return ((gxLib::Joy(0)->tap & MOUSE_L)!=0);
}

gxBool gxUtil::CTouch::IsFlick(Uint32 key)
{
	if( IsRelease() )
	{
		Sint32 x1 = gxLib::Touch(id)->sx;
		Sint32 y1 = gxLib::Touch(id)->sy;
		Sint32 x2 = gxLib::Touch(id)->ex;
		Sint32 y2 = gxLib::Touch(id)->ey;

		Float32 fRot  = gxUtil::Atan( x2-x1 , y2-y1 );
		Float32 fDist = gxUtil::Distance( x2-x1 , y2-y1 );

		if( fDist >= 32.0f )
		{
			Uint32 joy = 0x00;
			NORMALIZE(fRot);

			if( fRot > (360.0f-45) || fRot < ( 45.0f) )      joy |= JOY_R;
			if( fRot > ( 90.0f-45) && fRot < ( 90.0f+45.0) ) joy |= JOY_D;
			if( fRot > (180.0f-45) && fRot < (180.0f+45.0) ) joy |= JOY_L;
			if( fRot > (270.0f-45) && fRot < (270.0f+45.0) ) joy |= JOY_U;

			return (joy&key)? gxTrue : gxFalse;
		}
	}

	return gxFalse;
}

Float32 gxUtil::CTouch::GetFlickRotation()
{
	if( IsRelease() )
	{
		Sint32 x1 = gxLib::Touch(id)->sx;
		Sint32 y1 = gxLib::Touch(id)->sy;
		Sint32 x2 = gxLib::Touch(id)->ex;
		Sint32 y2 = gxLib::Touch(id)->ey;

		Float32 fRot  = gxUtil::Atan( x2-x1 , y2-y1 );
		Float32 fDist = gxUtil::Distance( x2-x1 , y2-y1 );

		if( fDist >= 100.0f )
		{
			NORMALIZE(fRot);
			return fRot;
		}
	}

	return 0.0f;
}


gxPoint* gxUtil::CTouch::Position()
{
	m_Touch[id].m_Position.x = gxLib::Touch(id)->x;
	m_Touch[id].m_Position.y = gxLib::Touch(id)->y;

	return &m_Position;
}

gxBool gxUtil::CTouch::IsZoom(Float32 *pRatio , Float32 min , Float32 max )
{
	//ズーム操作（ピンチイン、ピンチアウト）したか？
	//pRatioはズーム率

	Float32 whl = gxLib::Joy(0)->whl;

	if(  whl != 0 )
	{
		//ホイール操作があった場合はこっちを優先
		if( whl < 0 )
		{
			m_fPinchScale += (max - m_fPinchScale) / 10.0f;
		}
		else
		{
			m_fPinchScale += (min - m_fPinchScale)/10.0f;
		}

		m_fPinchScale = CLAMP(m_fPinchScale, min, max);
		m_PinchCnt = FRAME_PER_SECOND/2;

		*pRatio = m_fPinchScale;
		return gxTrue;
	}
	else
	{
		if( m_PinchCnt > 0 )
		{
			m_PinchCnt --;
			*pRatio = m_fPinchScale;
			return gxTrue;
		}
		else
		{
			m_fPinchScale = 1.0f;
		}
	}


	//ツーフィンガー操作があった場合はピンチイン、アウトを検証する

	Sint32 x1 = gxLib::Touch(0)->sx;
	Sint32 y1 = gxLib::Touch(0)->sy;
	Sint32 x3 = gxLib::Touch(0)->ex;
	Sint32 y3 = gxLib::Touch(0)->ey;

	Sint32 x2 = gxLib::Touch(1)->sx;
	Sint32 y2 = gxLib::Touch(1)->sy;
	Sint32 x4 = gxLib::Touch(1)->ex;
	Sint32 y4 = gxLib::Touch(1)->ey;

	*pRatio = m_fPinchScale;

	if( (gxLib::Touch(1)->stat&enStatPush) == 0x00)
	{
		return gxFalse;
	}

	Float32 fDist1 = gxUtil::Distance( x1 ,y1 , x2 , y2 );
	Float32 fDist2 = gxUtil::Distance( x3 ,y3 , x4 , y4 );

	gxLib::Printf( 32, 32+32*0,100,ATR_STR_LEFT , 0xffffffff , "%d,%d" , x1,y1 );
	gxLib::Printf( 32, 32+32*1,100,ATR_STR_LEFT , 0xffffffff , "%d,%d" , x3,y3 );
	gxLib::Printf( 32, 32+32*2,100,ATR_STR_LEFT , 0xffffffff , "%d,%d" , x2,y2 );
	gxLib::Printf( 32, 32+32*3,100,ATR_STR_LEFT , 0xffffffff , "%d,%d" , x4,y4 );

	if (fDist2 <= 0.0f) return 0.0f;
	m_fPinchScale = (fDist2 / fDist1);

	m_fPinchScale = CLAMP(m_fPinchScale, min, max);

	*pRatio = m_fPinchScale;

	return gxTrue;
}


gxBool gxUtil::CTouch::IsTap( Sint32 x , Sint32 y , Sint32 w , Sint32 h)
{
	if( !IsPush() ) return gxFalse;

	gxPoint* p = Position();

	if( p->x < x || p->x >= (x+w)) return gxFalse;
	if( p->y < y || p->y >= (y+h)) return gxFalse;

	return gxTrue;
}


gxBool gxUtil::CTouch::IsTap( Sint32 x , Sint32 y , Float length )
{
	if (!IsPush()) return gxFalse;

	gxPoint* p = Position();

	if( Distance( p->x -x , p->y-y ) >= length ) return gxFalse;

	return gxTrue;
}

void gxUtil::RoundButton::Update()
{
	gxBool bPush    = gxFalse;
	gxBool bRelease = gxFalse;
	gxBool bDecide  = gxFalse;
	gxBool bInRange = gxFalse;

/*
	Sint32 gw,gh,sw,sh;
	gxLib::GetDeviceResolution( &gw , &gh , &sw , &sh );

	gxPos pos;
	pos.x = m_Pos.x / WINDOW_W;
	pos.y = m_Pos.y / WINDOW_H;
*/
	Sint32 mx,my;

	for( Sint32 ii=0; ii<3; ii++ )
	{
		gxBool bInRound = gxFalse;

		gxPos pos1,pos2;

		pos1.x = gxUtil::Touch(ii)->Position()->x;
		pos1.y = gxUtil::Touch(ii)->Position()->y;

		pos2.x = m_Pos.x;
		pos2.y = m_Pos.y;

		if( m_Pos.z >= MAX_PRIORITY_NUM )
		{
			gxLib::GetNativePosition( &pos1.x , &pos1.y );
			//gxLib::GetNativePosition( &pos2.x , &pos2.y );
		}

		if (Distance(pos1.x - pos2.x ,  pos1.y - pos2.y) <= m_fRadius)
		{
			bInRound = gxTrue;
		}

		if (m_PushCnt == 0)
		{
			if (bInRound && gxUtil::Touch(ii)->IsTrigger() )
			{
				//初めて押して範囲内ならトリガー成功
				bPush = gxTrue;
				break;
			}
		}
		else
		{
			//離したことを検出

			if (bInRound && gxUtil::Touch(ii)->IsPush())
			{
				//ボタンが押されている間は押しっぱなしは継続
				bPush = gxTrue;

				if (bInRound)
				{
					bInRange = gxTrue;
				}
				else
				{
					bInRange = gxFalse;
				}

			}
			else if (gxUtil::Touch(ii)->IsRelease())
			{
				//離したときに範囲内にいるか？
				if (bInRound)
				{
					bDecide = gxTrue;
				}
			}
		}

	}
	if( bPush )
	{
		m_PushCnt ++;
	}
	else
	{
		if (bDecide)
		{
			m_PushCnt = -1;
		}
		else
		{
			m_PushCnt = 0;
		}
	}

}

void gxUtil::RoundButton::Draw()
{
	if (m_PushCnt > 0)
	{
		Float32 fRadius = m_fRadius*2.0f;
		gxLib::DrawCircle(m_Pos.x, m_Pos.y, m_Pos.z, ATR_DFLT, m_ARGB, fRadius , 3.0f );
	}
	else
	{
		gxLib::DrawPoint(m_Pos.x, m_Pos.y, m_Pos.z, ATR_DFLT, m_ARGB, m_fRadius);
	}
	
	if( m_pButtonName )
	{
		gxLib::Printf( m_Pos.x, m_Pos.y, m_Pos.z, ATR_STR_CENTER| ATR_STR_MID, 0xffffffff, "%s" , m_pButtonName );
	}
}

void SampleHTTP()
{
	Uint8 *pData;
	size_t uSize;

	static Sint32 seq = 0;
	static Sint32 index;

	if( seq == 0 )
	{
		index = gxUtil::OpenWebFile("http://garuru.co.jp/api/basic.php?ope=GetIP");
		seq++;
	}

	if (gxUtil::IsDownloadWebFile(index))
	{
		pData = gxUtil::GetDownloadWebFile(index, &uSize);
	}
}



void gxUtil::FileNames::Set( gxChar *pStr , ... )
{
	gxChar *pTemp = new gxChar[1024];
	
	va_list app;
	va_start(app, pStr);
	vsprintf(pTemp, pStr, app);
	va_end(app);

	init(pTemp);

	SAFE_DELETES(pTemp);
}

void gxUtil::FileNames::init(gxChar *pStr )
{
	clear();
	m_Temp.clear();

	size_t sz32 = 0;
	m_pFileName32 = CDeviceManager::UTF8toUTF32(pStr, &sz32);

	for (Sint32 ii = 0; ii < sz32; ii+=4)
	{
		Uint32 word = *(Uint32*)(&m_pFileName32[ii]);
		if (word == '\\' )
		{
			*(Uint32*)(&m_pFileName32[ii]) = '/';
		}
	}

	// 「//」を検出してただす

	for (Sint32 ii = 0; ii < sz32-4; ii += 4)
	{
		Uint32 word1 = *(Uint32*)(&m_pFileName32[ii]);
		Uint32 word2 = *(Uint32*)(&m_pFileName32[ii+4]);
		if (word1 == '/' && word2 == '/')
		{
			gxUtil::MemCpy(&m_pFileName32[ii] , &m_pFileName32[ii+4] , sz32-ii);
			sz32 -= 4;
		}
	}


	size_t sz = 0;
	FullName = CDeviceManager::UTF32toUTF8( m_pFileName32 , &sz);
	m_Temp.push_back(FullName);

	gxBool bFoundEXT = gxFalse;
	Sint32 extIndex = -1;

	for (Sint32 ii = sz32-4; ii > 0; ii-=4)
	{
		Uint32 word = *(Uint32*)(&m_pFileName32[ii]);
		if (word == '.' && !bFoundEXT)
		{
			//拡張子を発見した ".ext"の形式で返す

			bFoundEXT = gxTrue;

			extIndex = ii;
			size_t sz1 = 0;
			Ext = CDeviceManager::UTF32toUTF8(&m_pFileName32[ii+0], &sz1);
			m_Temp.push_back(Ext);

		}
		else if (word == '/' || ii == 0 )
		{
			//区切りを発見したのでファイル名を記録する
			size_t sz1 = 0;
			FileName = CDeviceManager::UTF32toUTF8(&m_pFileName32[ii+4], &sz1);
			m_Temp.push_back(FileName);

			//拡張子なしのファイル名も記録しておく
			if (extIndex != -1)
			{
				size_t sz2 = 0;
				Uint32* p = new Uint32[sz32/4];
				gxUtil::MemCpy((Uint8*)p, m_pFileName32, sz32);

				p[extIndex / 4] = 0x00000000;
				FileNameWithoutExt = CDeviceManager::UTF32toUTF8( (gxChar*)&p[ii / 4+1 ], &sz1);
				m_Temp.push_back(FileNameWithoutExt);
			}


			if (ii != 0)
			{
				//ファイルパスも記録する
				size_t sz2 = 0;
				gxChar* p = new gxChar[sz32];
				gxUtil::MemCpy(p, m_pFileName32, sz32);

				p[ii + 0] = 0x00;
				p[ii + 1] = 0x00;
				p[ii + 2] = 0x00;
				p[ii + 3] = 0x00;
				FilePath = CDeviceManager::UTF32toUTF8(p, &sz2);
				m_Temp.push_back(FilePath);
				SAFE_DELETES(p);
			}


			break;
		}
	}

	if (FileNameWithoutExt == nullptr && FileName)
	{
		FileNameWithoutExt = FileName;
	}
/*
	FullName = nullptr;
		gxChar* FileName = nullptr;
		gxChar* FilePath = nullptr;
		gxChar* Ext = nullptr;
		gxChar* FileNameWithOutExt = nullptr;
		gxChar* RelativePath = nullptr;
*/

}

void gxUtil::FileNames::clear()
{
	if(m_pFileName32)
	{
		SAFE_DELETES(m_pFileName32);
	}

	for(int ii=0; ii<m_Temp.size(); ii++ )
	{
		SAFE_DELETES( m_Temp[ii] );
	}
}

void gxUtil::FileNames::ChangeExt(gxChar* pExt)
{
	gxChar* p = new gxChar[1024];
	sprintf(p, "%s/%s%s", FilePath, FileNameWithoutExt, pExt);

	init(p);

	SAFE_DELETES(p);

}

void gxUtil::FileNames::ChangePath(gxChar* pPath)
{
	gxChar* p = new gxChar[1024];

	sprintf(p, "%s/%s", pPath, FileName);

	init(p);

	SAFE_DELETES(p);


}

