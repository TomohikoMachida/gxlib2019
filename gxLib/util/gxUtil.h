//--------------------------------------------------
//
// gxUtil.cpp
// 便利な関数群
//
//--------------------------------------------------
#ifndef _GXUTIL_H_
#define _GXUTIL_H_
//#include <gxLib/Util/CCollisionManager.h>
//#include <gxLib/Util/CActManager.h>

class gxUtil
{
public:
	gxUtil()
	{
	}

	~gxUtil()
	{
	}


	//ファイルパス、名前、拡張子の変換
	static Uint32 atox( gxChar *p );
	static gxBool GetFileNameWithoutPath(gxChar *in,gxChar *out);
	static gxBool GetPath( gxChar *in , gxChar *out );
	static gxBool GetExt ( gxChar *in , gxChar* out );
	static gxBool GetFileNameWithoutExt(gxChar *in , gxChar *out);
	static gxBool IsSameEXT( gxChar *in1 , gxChar* in2 );

	static std::string GetMD5(Uint8 *pData, size_t sz);

	static std::string GetUpperPath(std::string path);
	static std::string GetRelativePath( gxChar* pcurrentPath , gxChar* path );
	static void StrUpr( gxChar* pStr );
	static size_t StrLen( gxChar* pStr );
	static void MemSet( void* pMem , Uint8 val, size_t sz );
	static void MemCpy( void* pDst , void *pSrc , size_t sz );

	//非同期のHTTPアクセス

	static Sint32 OpenWebFile  ( gxChar *pUrl , gxChar *pUserName = NULL , gxChar *pPassWord = NULL );
	static Sint32 IsDownloadWebFile( Uint32 index , Float32 *fRatio=NULL );
	static gxBool IsDownloadSucceeded(Uint32 index);
	static Uint8* GetDownloadWebFile(Uint32 uIndex, size_t* uSize);
	static gxBool CloseWebFile(Uint32 uIndex);

	//非同期のファイルアクセス
	static Sint32 LoadFile( gxChar* pFileName , ESTORAGE_LOCATION location = STORAGE_LOCATION_AUTO, std::function<void(Sint32 id , Uint8 *pData , Uint32 uSize)>func=nullptr );
	static gxBool IsLoadFile( Sint32 id , Uint8 **pData , size_t* pLength );
	static gxBool IsNowLoading();

	//テクスチャをファイルからマスターテクスチャへ読み込みます
	static gxBool LoadTexture ( Uint32 texPage , gxChar* fileName , Uint32 colorKey=0xff00ff00 ,Uint32 ox = 0 , Uint32 oy = 0 );

	//サウンドファイルをファイルから指定バンクに読み込みます
	static gxBool LoadAudio( Uint32 uIndex , gxChar* pFileName , ... );

	//ロードタスクがすべて完了しているか？
	static gxBool IsLoadComplete();

	//数学系

	static Float32 Cos( Float32 deg );
	static Float32 Sin( Float32 deg );
	static Float32 Atan( Float32 x ,Float32 y );
	static Float32 Sqrt( Float32 n );

	//２点間の距離を返します
	static Float32 Distance( Float32 x ,Float32 y );
	static Float32 Distance( Float32 x1 ,Float32 y1 , Float32 x2 ,Float32 y2);

	//座標を回転させます
	static void RotationPoint( gxPoint *pPt, Float32 fRot );

	//目標角度への回転方向と量を算出する
	static Float32 GetTargetRotation(Float32 target_angle, Float32 my_angle);

	static void BezierCurve( Float32 fromX, Float32 fromY, Float32 cpX, Float32 cpY, Float32 cpX2, Float32 cpY2, Float32 toX, Float32 toY, Float32 div ,std::vector<gxPoint> &path );

	//点と多角形の内外判定
	static gxBool IsInsidePolygon( gxPoint &p, gxPoint* poly , Uint32 length );

	//線と線の衝突判定
	static gxBool IsCross( gxPoint &r1, gxPoint &r2, gxPoint &p1, gxPoint &p2 );

	//多角形同士の当たり判定
	static gxBool PolyvsPoly( gxPoint *p1 , Uint32 length1 , gxPoint* p2 , Uint32 length2 );

	//円と円の当たり判定
	static gxBool CirclevsCircle( gxPoint *p1 , Float32 fRadius1 , gxPoint *p2 , Float32 fRadius2 );

	static Uint32 PutFreeSprite(
		Sint32 x,Sint32 y,
		Sint32 prio,
		gxVector2D *pUV,
		Sint32 tpg,
		Sint32 u,Sint32 v , Sint32 w , Sint32 h,
		Sint32 divNum = 3,
		Uint32  atr = ATR_DFLT,
		Uint32  argb = ARGB_DFLT,
		Float32 fRot = 0.0f,
		Float32 fx = 1.0f,
		Float32 fy = 1.0f
		);

	//多角形を描画します
	static Uint32 DrawPolygon(
			Sint32 num,
			gxVector2D *pXY,
			gxVector2D *pUV,
			Uint32 *argb,
			Sint32 prio,
			Uint32 atr ,
			Sint32 page,
			gxBool bFill );

	struct CKeyBoard
	{
		gxBool IsTrigger( Uint32 key );
		gxBool IsPush( Uint32 key );
		gxBool IsRepeat( Uint32 key );
		gxBool IsRelease( Uint32 key );
		gxKey::KeyType GetInputKey();
		gxChar* GetKeyString( gxKey::KeyType key );
	};

	struct CJoyPad
	{

	public:
		Sint32 id = 0;

		CJoyPad();
		~CJoyPad()
		{
		}
        Float32 GetRotationAngle( Float32 *pRot );
		gxBool IsTrigger( Uint32 key );
		gxBool IsPush( Uint32 key );
		gxBool IsRepeat( Uint32 key );
		gxBool IsRelease( Uint32 key );
		gxBool IsDoubleClick(Uint32 key);
		gxBool IsLongTap(Uint32 key);
		gxPoint* GetLeftStick();
		gxPoint* GetRightStick();
		gxChar*  GetString( EJoyBit btn );

	private:
		gxPoint m_LeftStick;
		gxPoint m_RightStick;
		gxPoint m_Mouse;
	};

	struct CTouch
	{
		//keyはMOUSE_L / MOUSE_M / MOUSE_Rのどれか
		gxBool IsTrigger();
		gxBool IsPush();
		gxBool IsRepeat();
		gxBool IsRelease();
		gxBool IsDoubleTap();
		gxBool IsLongTap();
		gxBool IsFlick(Uint32 key);
		gxBool IsZoom(Float32* pRatio , Float32 min = 0.5f , Float32 max = 5.0f);

		Float32 GetFlickRotation();

		gxBool IsTap( Sint32 x , Sint32 y , Sint32 w , Sint32 h);
		gxBool IsTap( Sint32 x , Sint32 y , Float length );
		gxPoint* Position();

		Sint32 id;
		gxPoint m_Position;

		Float32 m_fPinchScale = 0.0f;
		Sint32  m_PinchCnt = 0;

	private:
	};

	static CKeyBoard *KeyBoard();
	static CJoyPad   *JoyPad( Uint32 player );
	static CTouch    *Touch(Uint32 id = 0);

	//衝撃波
	static void DrawSonicBoom( Sint32 x , Sint32 y , Sint32 prio , Float32 fRadius  , Float32 fWidth = 0.0f , Float32 fDistPow = 0.1f , Uint32 tpg=TEXPAGE_CAPTURE , Uint32 atr=ATR_DFLT,Uint32 argb=ARGB_DFLT );


	class RoundButton
	{
	public:

		RoundButton( Sint32 x , Sint32 y , Sint32 prio , Uint32 argb , Float32 radius)
		{
			m_Pos.x = x;
			m_Pos.y = y;
			m_Pos.z = prio;
			m_ARGB = argb;
			m_fRadius = radius;
			m_bOn = gxTrue;
			m_PushCnt = 0;
		}

		~RoundButton(){};
		void SetPos(Sint32 x, Sint32 y, Sint32 prio )
		{
			m_Pos.x = x;
			m_Pos.y = y;
			m_Pos.z = prio;
		}
		void Update();
		void Draw();
		void SetOn( gxBool bOn )
		{
			m_bOn = bOn;
		}

		gxBool IsTrigger()
		{
			if( m_PushCnt == -1 ) return gxTrue;
			return gxFalse;
		}

		void SetText( gxChar *pButtonName )
		{
			m_pButtonName = pButtonName;
		}

	private:
		gxPos   m_Pos={0,0,0};
		Float32 m_fRadius = 32.0f;
		Uint32  m_ARGB = 0xffffffff;
		gxBool  m_bOn = gxTrue;
		Sint32  m_PushCnt = 0;

		gxChar *m_pButtonName = nullptr;
	};


	class FileNames
	{
	public:

		FileNames()
		{
		}

		~FileNames()
		{
			clear();
		}

		void Set( gxChar *pStr , ... );

		void ChangeExt(gxChar *pExt);
		void ChangePath(gxChar *pPath);

		gxChar* FullName = nullptr;
		gxChar* FileName = nullptr;
		gxChar* FilePath = nullptr;
		gxChar* Ext = nullptr;
		gxChar* FileNameWithoutExt = nullptr;
		//gxChar* RelativePath = nullptr;

	private:

		void init(gxChar* pStr);
		void clear();
		std::vector<gxChar*> m_Temp;
		gxChar *m_pFileName32 = nullptr;
	};

private:

	static CKeyBoard m_KeyBoard;
	static CJoyPad   m_JoyPad[PLAYER_MAX];
	static CTouch    m_Touch[GX_TOUCH_MAX];

};


class CircleCircle
{
public:
	typedef struct StCircle {
		//円の定義用
		Float32 x,y,r;
		StCircle()
		{
			x = y = 0;
			r = 1;
		}
	} StCircle;

	CircleCircle();
	~CircleCircle();

	void SetCircle( Sint32 id , Float32 x , Float32 y , Float32 r )
	{
		//円を定義する
		m_Circle[id].x = (Float32)x;
		m_Circle[id].y = (Float32)y;
		m_Circle[id].r = (Float32)r;
	}

	void SetNear( Float32 x , Float32 y )
	{
		//近い座標を設定する
		m_Near.x = x;
		m_Near.y = y;
	}

	gxPoint* GetKouten( Sint32 sIndex = 0 )
	{
		//交点座標を返す
		return &m_Kouten[sIndex];
	}

	Sint32 Calc();
	Sint32 GetNearest()
	{
		return m_sNearest;
	}
private:

	gxPoint    m_Near;		//交点が２つある場合に近い点を探す際の参照する座標
	Sint32     m_sNearest;	//交点１、２の最も近い点
	StCircle   m_Circle[2];	//円１、２
	gxPoint    m_Kouten[2];	//交点１，２

};


#include <iterator>
#include <cstdint>

class MD5 {
private:
	std::uint32_t 	a0_;
	std::uint32_t 	b0_;
	std::uint32_t 	c0_;
	std::uint32_t 	d0_;
	std::uint32_t*	m_array_first_;
	std::uint32_t	m_array_[16];

private:
	static std::uint32_t left_rotate(std::uint32_t x, std::uint32_t c) {
		return (x << c) | (x >> (32 - c));
	}

	template <class OutputIterator>
	static void uint32_to_byte(std::uint32_t n, OutputIterator & first) {
		*first++ = n & 0xff;
		*first++ = (n >> 8) & 0xff;
		*first++ = (n >> 16) & 0xff;
		*first++ = (n >> 24) & 0xff;
	}

	template <class OutputIterator>
	static void uint32_to_hex(std::uint32_t n, OutputIterator & first) {
		const char * hex_chars = "0123456789abcdef";

		std::uint32_t b;

		b = n & 0xff;
		*first++ = hex_chars[b >> 4];
		*first++ = hex_chars[b & 0xf];

		b = (n >> 8) & 0xff;
		*first++ = hex_chars[b >> 4];
		*first++ = hex_chars[b & 0xf];

		b = (n >> 16) & 0xff;
		*first++ = hex_chars[b >> 4];
		*first++ = hex_chars[b & 0xf];

		b = (n >> 24) & 0xff;
		*first++ = hex_chars[b >> 4];
		*first++ = hex_chars[b & 0xf];
	}

private:
	void reset_m_array() {
		m_array_first_ = &m_array_[0];
	}

	template<class InputIterator>
	static std::uint8_t input_u8(const InputIterator& it)
	{
		return *it;
	}

	template <class InputIterator>
	void bytes_to_m_array(InputIterator & first, std::uint32_t* m_array_last)
	{
		/*
					uint8_t * p = (uint8_t*)(&(*first));
					*m_array_first_ = (*p);

					first++;
					p = (uint8_t*)(&(*first));
					*m_array_first_ |= (*p)<<8;
		*/
		for (; m_array_first_ != m_array_last; ++m_array_first_)
		{
			*m_array_first_ = input_u8(first++);
			*m_array_first_ |= input_u8(first++) << 8;
			*m_array_first_ |= input_u8(first++) << 16;
			*m_array_first_ |= input_u8(first++) << 24;
		}
	}

	template<class InputIterator>
	void true_bit_to_m_array(InputIterator& first, std::ptrdiff_t chunk_length)
	{
		switch (chunk_length % 4) {
		case 0:
			*m_array_first_ = 0x00000080;
			break;
		case 1:
			*m_array_first_ = input_u8(first++);
			*m_array_first_ |= 0x00008000;
			break;
		case 2:
			*m_array_first_ = input_u8(first++);
			*m_array_first_ |= input_u8(first++) << 8;
			*m_array_first_ |= 0x00800000;
			break;
		case 3:
			*m_array_first_ = input_u8(first++);
			*m_array_first_ |= input_u8(first++) << 8;
			*m_array_first_ |= input_u8(first++) << 16;
			*m_array_first_ |= 0x80000000;
			break;
		}
		++m_array_first_;
	}
	void zeros_to_m_array(std::uint32_t* m_array_last) {
		for (; m_array_first_ != m_array_last; ++m_array_first_) {
			*m_array_first_ = 0;
		}
	}

	void original_length_bits_to_m_array(std::uint64_t original_length_bits) {
#if 1
		* m_array_first_++ = uint32_t(original_length_bits);
		*m_array_first_++ = uint32_t(original_length_bits >> 32);
#else
		original_length_bits &= 0xffffffffffffffff;
		*m_array_first_++ = (original_length_bits) & 0x00000000ffffffff;
		*m_array_first_++ = (original_length_bits & 0xffffffff00000000) >> 32;
#endif
	}

	void hash_chunk() {
		static std::uint32_t const k_array[64] = {
			0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
			0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
			0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
			0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
			0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
			0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
			0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
			0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
			0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
			0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
			0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
			0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
			0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
			0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
			0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
			0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391
		};
		static std::uint32_t const s_array[64] = {
			7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
			5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
			4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
			6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21
		};

		std::uint32_t A = a0_;
		std::uint32_t B = b0_;
		std::uint32_t C = c0_;
		std::uint32_t D = d0_;

		std::uint32_t F;
		unsigned int g;

		for (unsigned int i = 0; i < 64; ++i) {
			if (i < 16) {
				F = (B & C) | ((~B) & D);
				g = i;
			}
			else if (i < 32) {
				F = (D & B) | ((~D) & C);
				g = (5 * i + 1) & 0xf;
			}
			else if (i < 48) {
				F = B ^ C ^ D;
				g = (3 * i + 5) & 0xf;
			}
			else {
				F = C ^ (B | (~D));
				g = (7 * i) & 0xf;
			}

			std::uint32_t D_temp = D;
			D = C;
			C = B;
			B += left_rotate(A + F + k_array[i] + m_array_[g], s_array[i]);
			A = D_temp;
		}

		a0_ += A;
		b0_ += B;
		c0_ += C;
		d0_ += D;
	}

public:
	template <class InputIterator>
	void update(InputIterator first, InputIterator last) {

		std::uint64_t original_length_bits = std::distance(first, last) * 8;

		std::ptrdiff_t chunk_length;
		while ((chunk_length = std::distance(first, last)) >= 64) {
			reset_m_array();
			bytes_to_m_array(first, &m_array_[16]);
			hash_chunk();
		}

		reset_m_array();
		bytes_to_m_array(first, m_array_ + chunk_length / 4);
		true_bit_to_m_array(first, chunk_length);

		if (chunk_length >= 56) {
			zeros_to_m_array(&m_array_[16]);
			hash_chunk();

			reset_m_array();
			zeros_to_m_array(&m_array_[16] - 2);
			original_length_bits_to_m_array(original_length_bits);
			hash_chunk();
		}
		else {
			zeros_to_m_array(&m_array_[16] - 2);
			original_length_bits_to_m_array(original_length_bits);
			hash_chunk();
		}
	}

public:

	MD5()
		: a0_(0x67452301),
		b0_(0xefcdab89),
		c0_(0x98badcfe),
		d0_(0x10325476)
	{}

	template <class Container>
	void digest(Container & container) {
		container.resize(16);
		auto it = container.begin();

		uint32_to_byte(a0_, it);
		uint32_to_byte(b0_, it);
		uint32_to_byte(c0_, it);
		uint32_to_byte(d0_, it);
	}

	template <class Container>
	void hex_digest(Container & container) {
		container.resize(32);
		auto it = container.begin();

		uint32_to_hex(a0_, it);
		uint32_to_hex(b0_, it);
		uint32_to_hex(c0_, it);
		uint32_to_hex(d0_, it);
	}

	uint8_t *GetHash()
	{
		static uint32_t hash[5] = { a0_,b0_,c0_,d0_ ,0x00000000};

		hash[0] = a0_;
		hash[1] = b0_;
		hash[2] = c0_;
		hash[3] = d0_;

		return (uint8_t*)&hash[0];



		static uint8_t ret[65];

		uint8_t *p = (uint8_t*)&hash[0];

		for (int ii = 0; ii < 4; ii++)
		{
			ret[ii * 4 + 0] = p[ii * 4 + 0];
			ret[ii * 4 + 1] = p[ii * 4 + 1];
			ret[ii * 4 + 2] = p[ii * 4 + 2];
			ret[ii * 4 + 3] = p[ii * 4 + 3];
		}
		ret[64] = 0x00;
		return &ret[0];
	}

};

#endif
