/*

	センサー
*/

package jp.co.garuru;

import android.app.Activity;
import android.os.Bundle;
//
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.content.Context;
import java.util.List;

public class gxCensor implements SensorEventListener {

	public float[] m_SensorMagne  = new float[3];
	public float[] m_SensorAccel  = new float[3];
	public float[] m_SensorGyro   = new float[3];
	public float[] m_SensorOrient = new float[3];

	Context m_View;
	SensorManager m_Manager;

	gxCensor()
	{
	}

	void Init( Context view )
	{
		m_View = view;
		m_Manager = (SensorManager)view.getSystemService(Context.SENSOR_SERVICE);
		initSensors();
	}

	void initSensors()
	{
		//ここで初期化しないとセンサーが稼働しないので
		//コールバックされない
		List<Sensor> sensors = m_Manager.getSensorList(Sensor.TYPE_ALL);

		for (Sensor sensor : sensors) {

			if (sensor.getType() == Sensor.TYPE_ORIENTATION) {
				m_Manager.registerListener(this, sensor,  SensorManager.SENSOR_DELAY_GAME);
			}

			if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
				m_Manager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);
			}

			if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
				m_Manager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);
			}

			if (sensor.getType() == Sensor.TYPE_GYROSCOPE) {
				m_Manager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);
			}
		}
	}

	public float[] GetFloatArray()
	{
		float[] arrays = new float[12];

		arrays[0] =m_SensorGyro[0];
		arrays[1] =m_SensorGyro[1];
		arrays[2] =m_SensorGyro[2];

		arrays[3] =m_SensorAccel[0];
		arrays[4] =m_SensorAccel[1];
		arrays[5] =m_SensorAccel[2];

		arrays[6] =m_SensorMagne[0];
		arrays[7] =m_SensorMagne[1];
		arrays[8] =m_SensorMagne[2];

		arrays[9] =m_SensorOrient[0];
		arrays[10] =m_SensorOrient[1];
		arrays[11] =m_SensorOrient[2];

		return arrays;
	}

	@Override
	public void onSensorChanged(SensorEvent event)
	{
		switch (event.sensor.getType()) {
			case Sensor.TYPE_ORIENTATION:
				m_SensorOrient = event.values.clone();
				break;
			case Sensor.TYPE_ACCELEROMETER:
				m_SensorAccel = event.values.clone();
				break;
			case Sensor.TYPE_MAGNETIC_FIELD:
				m_SensorMagne = event.values.clone();
				break;
			case Sensor.TYPE_GYROSCOPE:
				m_SensorGyro = event.values.clone();
				break;
			default:
				break;
		}
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy)
	{

	}
}

