/*
	Input Device管理
	マルチタッチ & Vibration & コントローラー & キーボード
	http://hasami-yoshibo.hatenablog.com/entry/2013/05/05/000044
*/

package jp.co.garuru;

import android.app.Activity;
import android.content.Context;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
//
import android.view.View;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.os.Vibrator;
//
import android.view.KeyEvent;
import android.util.Log;

public class gxInput implements OnTouchListener {

	Context m_View;
	static final int m_MaxTouch = 5;

	public class TouchStat
	{
		TouchStat()
		{
			pos_x = 0;
			pos_y = 0;
			status = 0;
			id = -1;
		}

		public void Reset()
		{
			status = 0;
			id = -1;
		}

		public float pos_x;
		public float pos_y;
		public int status;
		public int id;
	}

	public int GetMax()
	{
		return m_MaxTouch;
	}

	public int[] m_MouseCursor = new int[5*2];


	Vibrator m_Vibration;
	float[] m_AnalogPos = new float[3];
	float[] m_AnalogRot = new float[3];
	float[] m_AnalogTrg = new float[2];
	float[] m_AnalogHat = new float[2];
	int mPointerID1;
	int mPointerID2;

	public TouchStat[] m_Touch = new TouchStat[5];

	gxInput()
	{
		mPointerID1 = -1;
		mPointerID2 = -1;
		for(int ii=0; ii<m_MaxTouch; ii++ )
		{
			m_Touch[ii] = new TouchStat();
		}
	}

	void Init( Context view )
	{
		m_View = view;
		m_Vibration = (Vibrator) view.getSystemService(Context.VIBRATOR_SERVICE);
	}

	public void Rumble( int msec )
	{
		if( msec <= 0 )
		{
			m_Vibration.cancel();
		}
		else {
			m_Vibration.vibrate(msec);  // 指定時間ONする
		}
	}

	private int getBlankIndex()
	{
		for(int jj=0;jj<5;jj++)
		{
			if( m_Touch[jj].id == -1 )
			{
				return jj;
			}
		}
		return -1;
	}

	private int getTouchIndex(int pointerId )
	{
		for(int jj=0;jj<5;jj++)
		{
			if( pointerId == m_Touch[jj].id )
			{
				return jj;
			}
		}
		return -1;
	}

	@Override
	public boolean onTouch(View v , MotionEvent event)
	{
		int max = event.getPointerCount();

		for( int ii=0; ii<1; ii++ )
		{
			int pointerIndex = event.getActionIndex();				//今回のイベントで対象となる配列番号（※）
			int eventAction  = event.getActionMasked();				//イベント番号の割り出し

			int pointerId	 = event.getPointerId(pointerIndex);	//配列番号からポインタの割り出し
			int index = event.findPointerIndex( pointerId );		//ポインタから格納されているデータの配列番号を逆引き（※と同じ？）

			//int deviceId = event.getDeviceId();
			//int actId = event.getActionIndex();
			//actId = actId%max;

            if( index < 0 ) continue;

			int tID = -1;

			//if( tID == -1 ) continue;

			switch (eventAction) {
				case MotionEvent.ACTION_DOWN:
					for( int jj=0; jj<m_MaxTouch; jj++ )
					{
						//他のすべてをなかったコトにする
						m_Touch[jj].Reset();
					}
					//1本目のタッチを開始
					tID = getBlankIndex();
					m_Touch[tID].id = pointerId;
					m_Touch[tID].status = 1;
					m_Touch[tID].pos_x = event.getX( index );
					m_Touch[tID].pos_y = event.getY( index );
					m_Touch[1].id = -1;
					m_Touch[2].id = -1;
					m_Touch[3].id = -1;
					m_Touch[4].id = -1;
					//Log.d("gxJava","mouse_down:" + pointerId + " / (" + event.getX(n) + " . " + event.getX(n) + ")");
					break;

				case MotionEvent.ACTION_CANCEL:
				case MotionEvent.ACTION_UP:
					//一本目を離した
                    tID = getTouchIndex(pointerId);
					m_Touch[tID].status = 2;
					m_Touch[tID].pos_x = event.getX( index );
					m_Touch[tID].pos_y = event.getY( index );
					//Log.d("gxJava","mouse_down:" + pointerId + " / (" + event.getX( n ) + " . " + event.getY( n ) + ")");
					break;

				case MotionEvent.ACTION_POINTER_DOWN:
					//２本目以降
					tID = getTouchIndex(pointerId);
					if( tID == -1 ) tID = getBlankIndex();
					m_Touch[tID].id = pointerId;
					m_Touch[tID].pos_x = event.getX( index );
					m_Touch[tID].pos_y = event.getY( index );
					m_Touch[tID].status = 1;

					//Log.d("gxJava","touch_down:" + pointerId + " / (" + event.getX( n ) + " . " + event.getY( n ) + ")");
					break;

				case MotionEvent.ACTION_POINTER_UP:
					tID = getTouchIndex(pointerId);
					if( tID >= 0 )
					{
						m_Touch[tID].pos_x = event.getX( index );
						m_Touch[tID].pos_y = event.getY( index );
						m_Touch[tID].status = 2;
					}
					//Log.d("gxJava","touch_up:" + pointerId + " / (" + event.getX( n ) + " . " + event.getY( n ) + ")");
					break;

				case MotionEvent.ACTION_MOVE:
					for( int jj=0; jj<max; jj++ )
					{
						pointerId	 = event.getPointerId(jj);	//配列番号からポインタの割り出し
						index = event.findPointerIndex( pointerId );		//ポインタから格納されているデータの配列番号を逆引き（※と同じ？）
						tID = getTouchIndex(pointerId);
						if( tID < 0 ) continue;
						m_Touch[tID].pos_x = event.getX(index);
						m_Touch[tID].pos_y = event.getY(index);
					}
					//Log.d("gxJava","touch_move:" + pointerId + " / (" + event.getX( n ) + " . " + event.getY( n ) + ")");
					break;

				default:
					break;
			}

		}
		return true;
	}


	public boolean onTouch_temp(View v , MotionEvent event)
	{
		int eventAction  = event.getActionMasked();
		int pointerIndex = event.getActionIndex();
		int pointerId	= event.getPointerId(pointerIndex);
		int max = event.getPointerCount();
		max = 1;

		int px=0,py=0;

		for( int ii=0;ii<max;ii++)
		{
			int id = 0;
			px = (int)(event.getX(ii));
			py = (int)(event.getY(ii));
			id = event.getPointerId(ii);
			//Log.d("test","ID=" + id + " / X=" + px + " , Y=" + py );

			int bPush = 0;

			//if( pointerId == id )
			{
				switch (eventAction) {
					case MotionEvent.ACTION_DOWN:
						//single push down
						//GameGirl.GetInstance().Toast("test");
						//Rumble(1000);
						bPush = 1;
						mPointerID1 = pointerId;
						mPointerID2 = -1;
						m_MouseCursor[0*2+0] = (int)(event.getX(mPointerID1));
						m_MouseCursor[0*2+1] = (int)(event.getY(mPointerID1));
						GameGirlJNI.cursorUpdate( 0 , m_MouseCursor[0*2+0] , m_MouseCursor[0*2+1] , 1 );
						break;

					case MotionEvent.ACTION_CANCEL:
					case MotionEvent.ACTION_UP:
						//指Bを離した時はACTION_POINTER_UPが呼ばれ、指Aを離した時はACTION_UPが呼ばれる。
						bPush = 2;
						mPointerID1 = -1;
						mPointerID2 = -1;
						GameGirlJNI.cursorUpdate( 0 , m_MouseCursor[0*2+0] , m_MouseCursor[0*2+1] , 2 );
						break;

					case MotionEvent.ACTION_POINTER_DOWN:
						bPush = 1;
						if (mPointerID2 == -1)
						{
							mPointerID2 = pointerId;
							m_MouseCursor[1*2+0] = (int)(event.getX(mPointerID2));
							m_MouseCursor[1*2+1] = (int)(event.getY(mPointerID2));
							GameGirlJNI.cursorUpdate( 1 , m_MouseCursor[1*2+0] , m_MouseCursor[1*2+1] , 1 );
						}
						else if (mPointerID1 == -1)
						{
							mPointerID1 = pointerId;
							m_MouseCursor[0*2+0] = (int)(event.getX(mPointerID1));
							m_MouseCursor[0*2+1] = (int)(event.getY(mPointerID1));
							GameGirlJNI.cursorUpdate( 0 , m_MouseCursor[0*2+0] , m_MouseCursor[0*2+1] , 1 );
						}
						break;

					case MotionEvent.ACTION_POINTER_UP:
						bPush = 2;
						if (mPointerID1 == pointerId)
						{
							mPointerID1 = -1;
							//mTextView1.setText("");
							m_MouseCursor[0*2+0] = (int)(event.getX(mPointerID1));
							m_MouseCursor[0*2+1] = (int)(event.getY(mPointerID1));
							GameGirlJNI.cursorUpdate( 0 , m_MouseCursor[0*2+0] , m_MouseCursor[0*2+1] , 2 );
						}
						else if (mPointerID2 == pointerId)
						{
							mPointerID2 = -1;
							//mTextView2.setText("");
							m_MouseCursor[1*2+0] = (int)(event.getX(mPointerID2));
							m_MouseCursor[1*2+1] = (int)(event.getY(mPointerID2));
							GameGirlJNI.cursorUpdate( 1 , m_MouseCursor[1*2+0] , m_MouseCursor[1*2+1] , 2 );
						}
						break;

//					case MotionEvent.ACTION_CANCEL:
//					case MotionEvent.ACTION_UP:
//						//指Bを離した時はACTION_POINTER_UPが呼ばれ、指Aを離した時はACTION_UPが呼ばれる。
//						bPush = 2;
//						mPointerID1 = -1;
//						mPointerID2 = -1;
//						break;

					case MotionEvent.ACTION_MOVE:
						//指を動かしている場合に呼ばれる。
						bPush = 1;
						float x1 = 0.0f;
						float y1 = 0.0f;
						float x2 = 0.0f;
						float y2 = 0.0f;

						if (mPointerID1 >= 0)
						{
							int ptrIndex = event.findPointerIndex(mPointerID1);
							x1 = event.getX(ptrIndex);
							y1 = event.getY(ptrIndex);
						}
						if (mPointerID2 >= 0)
						{
							int ptrIndex = event.findPointerIndex(mPointerID2);
							x2 = event.getX(ptrIndex);
							y2 = event.getY(ptrIndex);
						}
						// ジェスチャー処理
						if (mPointerID1 >= 0 && mPointerID2 == -1)
						{
							// 1本目の指だけが動いてる時の処理
							//mTextView1.setText(String.format("pointer1: %3.1f, %3.1f", x1, y1));
							px = (int)x1;
							py = (int)y1;
							m_MouseCursor[0*2+0] = (int)x1;
							m_MouseCursor[0*2+1] = (int)y1;
						}
						else if (mPointerID1 == -1 && mPointerID2 >= 0)
						{
							// 2本目の指だけが動いてる時の処理
							//mTextView2.setText(String.format("pointer2: %3.1f, %3.1f", x2, y2));
							px = (int)x2;
							py = (int)y2;
							m_MouseCursor[1*2+0] = (int)x2;
							m_MouseCursor[1*2+1] = (int)x2;
						}
						else if (mPointerID1 >= 0 && mPointerID2 >= 0)
						{
							// 1本目と2本目の指が動いてる時の処理
							//mTextView1.setText(String.format("pointer1/2: %3.1f, %3.1f", x1, y1));
							//mTextView2.setText(String.format("pointer2/2: %3.1f, %3.1f", x2, y2));
							m_MouseCursor[0*2+0] = (int)x1;
							m_MouseCursor[0*2+1] = (int)y1;
							m_MouseCursor[1*2+0] = (int)x2;
							m_MouseCursor[1*2+1] = (int)x2;
							GameGirlJNI.cursorUpdate( 0 , m_MouseCursor[0*2+0] , m_MouseCursor[0*2+1] , bPush );
							GameGirlJNI.cursorUpdate( 1 , m_MouseCursor[1*2+0] , m_MouseCursor[1*2+1] , bPush );
						}
						break;
				}
			}


//			GameGirlJNI.cursorUpdate( id , px , py , bPush );
		}

		return true;
	}


	public void onAnalogInfo( int id, float[] pos , float[] roll , float[] trigger , float[] hat )
	{
		m_AnalogPos = pos;
		m_AnalogRot = roll;
		m_AnalogRot = trigger;
		m_AnalogHat = hat;

		float[] analog = new float[32];

		analog[0] = pos[0]; //analogL-x
		analog[1] = pos[1]; //analogL-y
		analog[2] = pos[2]; //analogR-X 右スティックのX軸

		analog[3] = roll[0];
		analog[4] = roll[1];
		analog[5] = roll[2]; //analogR-X 右スティックのY軸

		analog[6] = trigger[0];
		analog[7] = trigger[1];
		analog[8] = 0;//trigger[2];

		analog[9]  = hat[0];
		analog[10] = hat[1];
		analog[11] = 0;//hat[2];

		GameGirlJNI.analogUpdate( id, analog );

	}

	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if( keyCode == KeyEvent.KEYCODE_VOLUME_DOWN )
		{
		}
		else if( keyCode == KeyEvent.KEYCODE_VOLUME_UP )
		{

		}
		else if( keyCode == KeyEvent.KEYCODE_BACK )
		{

		}
		int id = event.getDeviceId();

		GameGirlJNI.inputUpdate( id,0x01 , keyCode );

		return true;
	}

	public boolean onKeyUp(int keyCode, KeyEvent event)
	{
		if( keyCode == KeyEvent.KEYCODE_VOLUME_DOWN )
		{
		}
		else if( keyCode == KeyEvent.KEYCODE_VOLUME_UP )
		{

		}
		else if( keyCode == KeyEvent.KEYCODE_BACK )
		{

		}

		int id = event.getDeviceId();

		GameGirlJNI.inputUpdate( id,0x02 , keyCode );

		return true;
	}
}

